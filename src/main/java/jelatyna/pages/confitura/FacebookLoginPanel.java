package jelatyna.pages.confitura;

import jelatyna.security.SecurityUtils;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

public class FacebookLoginPanel extends Panel {

    public FacebookLoginPanel(String id){
        super(id);

        add(new FacebookLoginForm());

    }
    private class FacebookLoginForm extends Form{
        public FacebookLoginForm() {
            super("facebook_login_form");

            this.add(new HiddenField<TextField>("scope",new Model("email,publish_stream,offline_access")));
            SubmitLink loginFormSubmitLink = new SubmitLink("loginFormSubmitLink");

            this.add(loginFormSubmitLink);
        }

        @Override
        public boolean isVisible() {
            return SecurityUtils.isAnonymous();
        }
        @Override
        protected void onComponentTag(ComponentTag tag) {
            super.onComponentTag(tag);
            tag.put("method","post");
            tag.put("action","/signin/facebook");
        }
    }

}