package jelatyna.domain.dto;

import java.util.List;

public class SpeakerDto extends UserDto {
    private List<PresentationDto> presentations;

    public List<PresentationDto> getPresentations() {
        return presentations;
    }

    public SpeakerDto presentations(List<PresentationDto> presentations) {
        this.presentations = presentations;
        return this;
    }
}
