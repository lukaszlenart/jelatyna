package jelatyna.services;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import jelatyna.domain.Admin;
import jelatyna.repositories.AdminRepository;
import jelatyna.repositories.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class AdminServiceImplShould extends UserServiceImplShould<Admin> {
    @Mock
    private AdminRepository repository;
    @Mock
    private Admin admin;
    @InjectMocks
    private AdminServiceImpl service;

//    @Override
//    @Test
//    public void saveOnlyIfSavingExistingUser() {
//        when(admin.isNew()).thenReturn(false);
//
//        service.save(admin, fileUpload);
//
//        verify(repository).save(admin);
//        verify(repository, never()).findByMail(anyString());
//        verify(admin, never()).encryptPassword();
//    }

    @Test
    public void notVerifyUserIfNoUserDefinedYet() {
        // given
        when(repository.count()).thenReturn(0L);

        // when
        Admin loggedUser = service.loginWith("my@email.com", "password");

        // then
        assertThat(loggedUser.getId()).isNull();
        assertThat(loggedUser.getMail()).isEqualTo("my@email.com");
        verify(repository, never()).findByMail(anyString());
    }

//    @Test
//    public void notCheckWhetherOldPasswordIsCorrect() {
//        String newPassword = "newPassword";
//
//        service.changePassword(admin, "oldPassword", newPassword);
//
//        verify(admin, never()).isPasswordCorrect(anyString());
//        verify(admin).passwordWithEncryption(newPassword);
//        verify(repository).save(admin);
//    }

    @Override
    protected Admin createUser() {
        return new Admin("my@email.com").id(1);
    }

    @Override
    protected UserService<Admin> getService() {
        return service;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <REPO extends UserRepository<Admin>> REPO getRepository() {
        return (REPO) repository;
    }

    @Override
    public void saveOnlyIfSavingExistingUser() {

    }

    @Override
    void mockRepositoryToReturn(Admin user) {
        when(getRepository().count()).thenReturn(1L);
        when(repository.findByMail(anyString())).thenReturn(user);
    }

}
