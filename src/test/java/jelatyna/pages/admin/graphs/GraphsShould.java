package jelatyna.pages.admin.graphs;

import jelatyna.domain.Participant;
import org.joda.time.LocalDateTime;
import org.junit.Test;

import static com.google.common.collect.Lists.*;
import static org.assertj.core.api.Assertions.*;

public class GraphsShould {
    private Graphs graphs;

    @Test
    public void getRegistrationGraphWithTwoParticipantInSameHour() {
        graphsWith(
                participant(day(), hour(), 10),
                participant(day(), hour(), 20));

        Graph graph = graphs.registrationGraph();

        assertThat(graph.getXAxis()).containsOnly("10, 01.02.2000");
        assertThat(graph.getYAxis()).containsOnly(2);
    }

    @Test
    public void getRegistrationGraphWithThreeParticipantInDifferentDaysSortedByDate() {
        graphsWith(
                participant(2, 2, minutes()),
                participant(3, 1, minutes()),
                participant(1, 3, minutes()),
                participant(3, 1, minutes()),
                participant(1, 3, minutes()),
                participant(1, 3, minutes())
        );

        Graph graph = graphs.registrationGraph();

        assertThat(graph.getXAxis()).containsSequence("03, 01.02.2000", "02, 02.02.2000", "01, 03.02.2000");
        assertThat(graph.getYAxis()).containsSequence(3, 1, 2);
    }

    private int minutes() {
        return 10;
    }

    private int hour() {
        return 10;
    }

    private int day() {
        return 1;
    }

    private int month() {
        return 2;
    }

    private int year() {
        return 2000;
    }

    private Participant participant(int day, int hour, int minutes) {
        return new Participant().registrationTime(new LocalDateTime(year(), month(), day, hour, minutes));
    }

    private void graphsWith(Participant... participants) {
        graphs = new Graphs(newArrayList(participants));
    }

}
