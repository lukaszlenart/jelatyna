package jelatyna.repositories;

import org.springframework.data.repository.Repository;

import jelatyna.domain.Volunteer;

public interface VolunteerRepository extends Repository<Volunteer, Integer>, UserRepository<Volunteer> {
}
