package jelatyna.pages.confitura.news;

import jelatyna.components.RedirectLink;
import jelatyna.domain.News;
import jelatyna.pages.confitura.ViewAdminPage;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.panel.Panel;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.PageParametersBuilder.*;

@SuppressWarnings("serial")
public class NewsPanel extends Panel {

    public NewsPanel(String id, News news, boolean withDetails) {
        super(id);
        add(titleLink(news));
        add(label("creationDate", news.formatCreatioDate()));
        add(label("creationDay", news.getDayOfWeek()));
        add(richLabel("shortDescription", news.getShortDescription()));
        add(richLabel("description", news.getDescription()).setVisible(withDetails));
        add(authorLink(news));
        add(moreLink(news, withDetails));
    }

    private Component moreLink(News news, boolean withDetails) {
        return detailsLink("more", news).setVisible(news.hasDescription() && !withDetails);
    }

    private RedirectLink authorLink(News news) {
        return new RedirectLink("author", ViewAdminPage.class)
                .withAnchor(news.getAutor().getFullName())
                .withLabel(news.getAutor().getFullName());
    }

    private RedirectLink titleLink(News news) {
        return detailsLink("title", news).withLabel(news.getTitle());
    }

    private RedirectLink detailsLink(String id, News news) {
        return new RedirectLink(id,
                NewsDetailsPage.class, params().id(news.getId()).title(news.getSimpleTitle()).build());
    }
}
