package jelatyna.components.menu;

import jelatyna.domain.MenuLinkItem;
import jelatyna.domain.WithTitle;
import jelatyna.repositories.MenuLinkItemRepository;
import org.apache.wicket.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import static com.google.common.collect.Lists.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DynamicMenuLinkShould {
    @Mock
    JpaRepository<EntityWithTitle, Integer> repository;

    @Mock
    MenuLinkItemRepository menuLinkItemRepository;

    @Test
    public void findAllItemsFromRepository() {
        when(repository.findAll()).thenReturn(newArrayList(new EntityWithTitle(1, "a"), new EntityWithTitle(2, "b")));
        DynamicMenuLink<EntityWithTitle> menuLink = new DynamicMenuLink<EntityWithTitle>(menuLinkItemRepository, repository,
                Page.class);

        List<MenuLinkItem> items = menuLink.getAllItems();

        assertThat(items).containsOnly(new MenuLinkItem(1, "a"), new MenuLinkItem(2, "b"));
    }

    @SuppressWarnings("serial")
    private class EntityWithTitle implements WithTitle {

        private final Integer id;
        private String title;

        public EntityWithTitle(Integer id, String title) {
            this.id = id;
            this.title = title;
        }

        @Override
        public String getTitle() {
            return title;
        }

        @Override
        public Integer getId() {
            return id;
        }

    }
}
