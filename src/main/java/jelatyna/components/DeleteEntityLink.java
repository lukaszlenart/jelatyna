package jelatyna.components;

import jelatyna.domain.AbstractEntity;
import org.apache.wicket.Page;

@SuppressWarnings("serial")
public abstract class DeleteEntityLink extends DeleteLink {
    private final AbstractEntity<?> entity;
    private final Class<? extends Page> page;

    public DeleteEntityLink(AbstractEntity<?> entity, Class<? extends Page> page) {
        super("delete");
        this.entity = entity;
        this.page = page;
    }

    @Override
    public void onClick() {
        deleteById(entity.getId());
        setResponsePage(page);
    }

    protected abstract void deleteById(Integer id);

}