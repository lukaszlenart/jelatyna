package jelatyna.domain;

@SuppressWarnings("serial")
public class EmptySponsor extends Sponsor {
    private SponsorType sponsorType;

    public EmptySponsor(SponsorType sponsorType) {
        this.sponsorType = sponsorType;
    }

    @Override
    public String getWebPage() {
        return sponsorType.getUrlPlaceholder();
    }
}