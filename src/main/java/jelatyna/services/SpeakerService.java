package jelatyna.services;

import java.util.List;

import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.domain.Vote;

public interface SpeakerService extends UserService<Speaker> {

    void delete(Presentation presentation);

    List<Presentation> allPresentations();

    List<Presentation> allShuffledPresentations();

    List<Speaker> allAcceptedSpeakers();

    void toggleAcceptance(Presentation presentation);

    Speaker findByToken(String string);

    void resetPasswordFor(Speaker speaker, String newPassword);

    void requestPasswordResetBy(String mail);

    boolean isCall4PapersActive();

    boolean isVotingActive();

    void save(Vote vote);

    double averageRateFor(Presentation presentation);

    int countVotesFor(Presentation presentation);

    Speaker randomSpeaker();

    Speaker findByMail(String mail);
}
