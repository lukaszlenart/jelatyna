package jelatyna.utils;

import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.util.string.Strings;

public class WicketUtils {

    public static String getRootUrlString(RequestCycle requestCycle) {
        Url baseUrl = requestCycle.getUrlRenderer().getBaseUrl();

        StringBuilder result = new StringBuilder();

        String protocol = baseUrl.getProtocol();
        if (Strings.isEmpty(protocol)) {
            protocol = "http";
        }

        result.append(protocol);
        result.append("://");
        result.append(baseUrl.getHost());

        if (baseUrl.getPort() != null && baseUrl.getPort().equals(getDefaultPortForProtocol(protocol)) == false) {
            result.append(':');
            result.append(baseUrl.getPort());
        }

        ServletWebRequest request = (ServletWebRequest) requestCycle.getRequest();
        result.append(request.getFilterPath());

        return result.toString();
    }

    private static Integer getDefaultPortForProtocol(String protocol) {
        if ("http".equals(protocol)) {
            return 80;
        } else if ("https".equals(protocol)) {
            return 443;
        } else if ("ftp".equals(protocol)) {
            return 21;
        } else {
            return null;
        }
    }
}
