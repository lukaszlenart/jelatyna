package jelatyna.pages.admin.sponsor;

import jelatyna.components.DeleteEntityLink;
import jelatyna.components.RedirectLink;
import jelatyna.domain.Sponsor;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.SponsorService;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ListSponsorPage extends AdminBasePage {

    @SpringBean
    private SponsorService service;

    public ListSponsorPage() {
        add(new SponsorGrid(service.findAllSponsors()));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.SPONSORS;
    }

    @Override
    public String getPageTitle() {
        return AdminMenuItemEnum.SPONSORS.getLabel();
    }

    private final class SponsorGrid extends DataView<Sponsor> {
        private SponsorGrid(List<Sponsor> list) {
            super("rows", new ListDataProvider<Sponsor>(list));
        }

        @Override
        protected void populateItem(Item<Sponsor> item) {
            Sponsor sponsor = item.getModelObject();
            item.add(label("name", sponsor.getName()));
            item.add(label("type", sponsor.getSponsorType().getName()));
            item.add(label("money", String.valueOf(sponsor.getMoney())));
            item.add(new RedirectLink("edit", AddSponsorPage.class, sponsor));
            item.add(new DeleteEntityLink(sponsor, ListSponsorPage.class) {

                @Override
                protected void deleteById(Integer id) {
                    service.deleteSponsorBy(id);
                }
            });
        }
    }
}
