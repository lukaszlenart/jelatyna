package jelatyna.utils;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import com.google.common.collect.Sets;

public class HtmlUtils {

    public static String clearHtml(String html) {
        if (StringUtils.isBlank(html)) {
            return html;
        }
        return Jsoup.clean(html, Whitelist.basic());
    }

    public static Collection<String> extractImageLinksFromHtml(String htmlContent) {
        Collection<String> images = Sets.newLinkedHashSet();

        if (StringUtils.isEmpty(htmlContent)) {
            return images;
        }

        Document document = Jsoup.parse(htmlContent);
        Elements imageElements = document.getElementsByTag("img");
        for (Element image : imageElements) {
            images.add(image.attr("src"));
        }
        return images;
    }

}
