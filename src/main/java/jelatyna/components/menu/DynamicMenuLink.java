package jelatyna.components.menu;

import jelatyna.domain.MenuLinkItem;
import jelatyna.domain.WithTitle;
import jelatyna.repositories.MenuLinkItemRepository;
import org.apache.wicket.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import static com.google.common.collect.Lists.*;

public class DynamicMenuLink<ENTITY extends WithTitle> extends AbstractMenuLink {
    private final MenuLinkItemRepository menuLinkItemRepository;
    private JpaRepository<ENTITY, ?> repository;

    public DynamicMenuLink(MenuLinkItemRepository menuLinkItemRepository, JpaRepository<ENTITY, ?> repository, Class<? extends Page> pageClazz) {
        super(pageClazz);
        this.menuLinkItemRepository = menuLinkItemRepository;
        this.repository = repository;
    }

    @Override
    public List<MenuLinkItem> getAllItems() {
        List<MenuLinkItem> result = newArrayList();
        for (ENTITY entity : repository.findAll()) {
            MenuLinkItem menuLinkItem = menuLinkItemRepository.readByItemId(entity.getId());
            if (menuLinkItem == null) {
                menuLinkItem = new MenuLinkItem(entity.getId(), entity.getTitle());
            }
            result.add(menuLinkItem);
        }
        return result;
    }

}
