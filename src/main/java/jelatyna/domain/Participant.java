package jelatyna.domain;

import static com.google.common.collect.Sets.*;
import static jelatyna.domain.RegistrationStatus.*;

import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import jelatyna.utils.DateUtils;

@SuppressWarnings("serial")
@Entity
public class Participant extends AbstractEntity<Participant> implements WithMail {
    public static final String WOMAN = "Kobieta";

    public static final String MAN = "Mężczyzna";

    public static final Participant EMPTY = new Participant("Imię", "Nazwisko").mail("");

    static final String ILLEGAL_STATUS = "Status Twojej rejestracji nie pozwala na tę operację.";

    RegistrationStatus registrationType = RegistrationStatus.NEW;

    private String firstName;

    private String lastName;

    private String mail;

    private String sex;

    private String city;

    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate = new Date();

    private String token;

    private boolean participated = false;

    @Temporal(TemporalType.TIMESTAMP)
    private Date participationTime;

    @ManyToMany
    private Set<Presentation> chosenPresentations = newHashSet();

        private String position;

        private String experience;

        private String info;

        private String size;

    private boolean mailing = false;

    public Participant() {
    }

    public Participant(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String getMail() {
        return mail;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSex() {
        return sex;
    }

    public LocalDate getRegistrationDate() {
        return LocalDate.fromDateFields(registrationDate);
    }

    public LocalDateTime getRegistrationTime() {
        return LocalDateTime.fromDateFields(registrationDate);
    }

    public String getRegistrationHour() {
        return getRegistrationTime().toString(DateUtils.HOUR_FORMAT);
    }

    public Participant token(String token) {
        this.token = token;
        return this;
    }

    public String getToken() {
        return token;
    }

    public RegistrationStatus getStatus() {
        return registrationType;
    }

    public boolean isConfirmed() {
        return this.registrationType == CONFIRMED;
    }

    public boolean isFinalConfirmed() {
        return this.registrationType == FINAL_CONFIRMED;
    }

    public String getCity() {
        return city == null ? "" : city;
    }

    public Participant city(String city) {
        this.city = city;
        return this;
    }

    public String getLowerCaseCity() {
        return getCity().toLowerCase();
    }

    public void finalConfirmation() {
        this.registrationType = FINAL_CONFIRMED;
    }

    public boolean isCancel() {
        return registrationType == CANCELED;
    }

    public void cancel() {
        this.registrationType = CANCELED;
    }

    public boolean participated() {
        return isParticipated();
    }

    public Set<Presentation> getChosenPresentations() {
        return chosenPresentations;
    }

    public boolean isParticipated() {
        return participated;
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public String getInfo() {
        return info;
    }

    public String getPosition() {
        return position;
    }

    public String getExperience() {
        return experience;
    }

    public String getSize() {
        return size;
    }

    public Participant mail(String mail) {
        this.mail = mail;
        return this;
    }

    public Participant participated(boolean participated) {
        this.participated = participated;
        this.participationTime = participated ? new Date() : null;
        return this;
    }

    public Date getParticipationTime() {
        return participationTime;
    }

    public void myPresentations(List<Presentation> presentations) {
        chosenPresentations.clear();
        chosenPresentations.addAll(presentations);
    }

    public int countPresentations() {
        return chosenPresentations.size();
    }

    public boolean isMailing() {
        return mailing;
    }

    public Participant registrationTime(LocalDateTime registrationTime) {
        this.registrationDate = registrationTime.toDate();
        return this;
    }

    public Participant status(RegistrationStatus status) {
        if (status.canBeAfter(registrationType)) {
            this.registrationType = status;
            return this;
        }
        throw new RuntimeException(ILLEGAL_STATUS);
    }

    public Participant mailing(boolean mailing) {
        this.mailing = mailing;
        return this;
    }

    public boolean shouldGetMaterials() {
        return isFinalConfirmed();
    }
}
