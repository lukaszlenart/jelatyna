package jelatyna.domain;

import jelatyna.utils.DateUtils;
import jelatyna.utils.TextInUrlUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.Locale;

@SuppressWarnings("serial")
@Entity
public class News extends AbstractEntity<News> {
    private boolean published = false;
    private Date creationDate = new Date();
    private String title;
    @Lob
    private String shortDescription;
    @Lob
    private String description;
    @ManyToOne
    private Admin autor;

    public String getTitle() {
        return title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public News autor(Admin autor) {
        this.autor = autor;
        return this;
    }

    public Admin getAutor() {
        return autor;
    }

    public News title(String title) {
        this.title = title;
        return this;
    }

    public News published(boolean published) {
        this.published = published;
        return this;
    }

    public News creationDate(Date creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public boolean isPublished() {
        return published;
    }

    public String formatCreatioDate() {
        return creationDateAsLocalDateTime().toString(DateUtils.DATE_FORMAT);
    }

    public String formatCreatioDateWithTime() {
        return creationDateAsLocalDateTime().toString(DateUtils.TIME_FORMAT);
    }

    public String getDayOfWeek() {
        return creationDateAsLocalDateTime().dayOfWeek().getAsText(new Locale("pl"));
    }

    public LocalDateTime creationDateAsLocalDateTime() {
        return LocalDateTime.fromDateFields(creationDate);
    }

    public boolean hasDescription() {
        return StringUtils.isNotBlank(description);
    }

    public News description(String description) {
        this.description = description;
        return this;
    }

    public News shortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    public String getSimpleTitle() {
        return TextInUrlUtils.prettify(title);
    }
}
