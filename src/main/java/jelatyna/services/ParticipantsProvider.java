package jelatyna.services;

import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.registration.ParticipantGroup;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Collections.*;

@SuppressWarnings("serial")
public class ParticipantsProvider implements Serializable {
    private final List<Participant> allParticipants;

    public ParticipantsProvider(List<Participant> allParticipants) {
        this.allParticipants = Collections.unmodifiableList(allParticipants);
    }

    public List<Participant> getAllParticipants() {
        return allParticipants;
    }

    public List<ParticipantGroup> groupParticipantsAndSort(Function<? super Participant, ?> classifier) {
        Map<Object, List<Participant>> grouped = allParticipants.stream().collect(Collectors.groupingBy(classifier));
        return convertGroupedMapToSortedParticipantGroupList(grouped);
    }

    private List<ParticipantGroup> convertGroupedMapToSortedParticipantGroupList(Map<Object, List<Participant>> grouped) {
        List<ParticipantGroup> result = new ArrayList<>(grouped.size());
        grouped.entrySet().forEach(entry -> result.add(new ParticipantGroup(entry.getKey(), grouped.get(entry.getValue()))));
        return result;
    }

    public Function<? super Participant, ?> getCityGroupCondition() {
        return Participant::getLowerCaseCity;
    }

    public Function<? super Participant, ?> getFirstNameGroupCondition() {
        return Participant::getFirstName;
    }

    public Function<? super Participant, ?> getRegistrationDateGroupCondition() {
        return Participant::getRegistrationDate;
    }

    public List<Participant> getParticipantsForGivenSex(String sex, RegistrationStatus status) {
        List<Participant> participantsForStatus = getAllParticipantsForStatus(status);
        return unmodifiableList(participantsForStatus.stream()
                .filter(e -> (sex == null) ? e.getSex() == null : sex.equals(e.getSex()))
                .collect(Collectors.toList()));
    }

    public List<Participant> getAllParticipantsForStatus(RegistrationStatus status) {
        if (status == null) {
            return allParticipants;
        }
        return unmodifiableList(allParticipants.stream()
                .filter(e -> status.equals(e.getStatus()))
                .collect(Collectors.toList()));
    }

    public Map<Interval, Integer> getParticipantsInTimeIntervals() {
        return getParticipantsInTimeIntervals(5);
    }

    public Map<Interval, Integer> getParticipantsInTimeIntervals(int minutesInInterval) {
        if (allParticipants.isEmpty()) {
            return Collections.emptyMap();
        }
        List<Participant> sortedParticipants = getParticipantsSortedByParticipationTime();

        DateTime initialBorderTime = new DateTime(sortedParticipants.get(0).getParticipationTime()).withMinuteOfHour(0);
        Period period = new Period().withMinutes(minutesInInterval);
        return splitParticipantsToIntervals(sortedParticipants, initialBorderTime, period);
    }

    private List<Participant> getParticipantsSortedByParticipationTime() {
        List<Participant> result = new ArrayList<>(allParticipants);
        result.sort((p1, p2) -> p1.getParticipationTime().compareTo(p2.getParticipationTime()));
        return result;
    }

    private Map<Interval, Integer> splitParticipantsToIntervals(List<Participant> participants,
                                                                DateTime currentBorderTime,
                                                                final Period periodOfSingleInterval) {
        Map<Interval, Integer> participantsInTimeIntervals = new LinkedHashMap<Interval, Integer>();

        Interval currentInterval = new Interval(currentBorderTime,
                currentBorderTime = currentBorderTime.plus(periodOfSingleInterval));
        int totalPresentParticipants = 0;
        for (Participant participant : participants) {
            if (participant.getParticipationTime() != null) {
                totalPresentParticipants++;
                while (participant.getParticipationTime().after(currentInterval.getEnd().toDate())) {
                    participantsInTimeIntervals.put(currentInterval, totalPresentParticipants - 1);
                    currentInterval = new Interval(currentBorderTime,
                            currentBorderTime = currentBorderTime.plus(periodOfSingleInterval));
                }
            }
        }
        participantsInTimeIntervals.put(currentInterval, totalPresentParticipants);

        return participantsInTimeIntervals;
    }
}
