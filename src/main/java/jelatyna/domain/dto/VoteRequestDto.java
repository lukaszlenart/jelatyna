package jelatyna.domain.dto;

public class VoteRequestDto implements AbstractDto {
    private String key;
    private Integer rate;
    private Integer presentationId;

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }


    public Integer getPresentationId() {
        return presentationId;
    }

    public void setPresentationId(Integer presentationId) {
        this.presentationId = presentationId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "VoteRequestDto{" +
                "vote=" + rate +
                ", presentationId=" + presentationId +
                '}';
    }
}
