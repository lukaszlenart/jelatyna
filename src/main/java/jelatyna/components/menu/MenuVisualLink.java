package jelatyna.components.menu;

import jelatyna.components.RedirectLink;
import jelatyna.domain.MenuItem;
import jelatyna.domain.MenuLinkItem;
import jelatyna.pages.confitura.ViewSimpleContentPage;
import jelatyna.utils.PageParametersBuilder;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.PageParametersBuilder.*;

@SuppressWarnings("serial")
public class MenuVisualLink extends Panel {

    private static final String LINK_ID = "link";

    public MenuVisualLink(String id, MenuItem menuItem, MenuLink menuLink) {
        super(id);
        setRenderBodyOnly(true);
        MarkupContainer link = createLink(menuItem, menuLink).add(label("label", menuItem.getName()));
        add(link);
    }

    private AbstractLink createLink(MenuItem menuItem, MenuLink menuLink) {
        if (menuLink.isLink()) {
            return createLinkToPage(menuItem, menuLink);
        } else {
            return new ExternalLink(LINK_ID, "#");
        }
    }

    private AbstractLink createLinkToPage(MenuItem menuItem, MenuLink menuLink) {
        Class<? extends Page> pageClass = menuLink.getPageClass();
        return new RedirectLink(LINK_ID, pageClass, createParams(menuItem, pageClass));
    }

    private PageParameters createParams(MenuItem menuItem, Class<? extends Page> pageClass) {
        MenuLinkItem linkItem = menuItem.getLinkItem();
        PageParametersBuilder builder = params().id(linkItem.getItemId());
        if (pageClass.isAssignableFrom(ViewSimpleContentPage.class)) {
            builder.title(linkItem.getSimpleTitle());
        }
        return builder.build();
    }
}
