package jelatyna.services;

import jelatyna.utils.ImageIO;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidationError;
import org.apache.wicket.validation.ValidationError;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.awt.image.BufferedImage;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static jelatyna.services.PhotoValidator.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PhotoValidatorShould {
    @Mock
    private FileUpload upload;
    @Mock
    private ImageIO imageIO;
    @Mock
    private BufferedImage image;
    @Mock
    private IValidatable<List<FileUpload>> validatable;
    @InjectMocks
    private PhotoValidator validator;

    @Test
    public void photo_be_valid_if_is_square() {
        mockImageWithSize(400, 400);

        validate();

        isValid();
    }

    @Test
    public void not_be_valid_if_photo_is_rectangle() {
        mockImageWithSize(100, 112);

        validate();

        ArgumentCaptor<ValidationError> captor = ArgumentCaptor.forClass(ValidationError.class);
        verify(validatable).error(captor.capture());
        assertThat(captor.getValue().getKeys()).containsOnly(WRONG_RATION);
    }

    @Test
    public void be_valid_if_widther_by_10_percent() throws Exception {
        mockImageWithSize(110, 100);

        validate();

        isValid();
    }

    @Test
    public void be_valid_if_higher_by_10_percent() {
        mockImageWithSize(100, 110);

        validate();

        isValid();
    }

    @Test
    public void be_valid_if_value_is_null() {
        when(validatable.getValue()).thenReturn(null);

        validate();

        isValid();
    }

    private void isValid() {
        verify(validatable, never()).error(any(IValidationError.class));
    }

    private void validate() {
        validator.validate(validatable);
    }

    private void mockImageWithSize(int width, int height) {
        when(validatable.getValue()).thenReturn(newArrayList(upload));
        when(imageIO.read(upload)).thenReturn(image);
        when(image.getWidth()).thenReturn(width);
        when(image.getHeight()).thenReturn(height);
    }
}
