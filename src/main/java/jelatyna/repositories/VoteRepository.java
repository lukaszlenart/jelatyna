package jelatyna.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import jelatyna.domain.Presentation;
import jelatyna.domain.Vote;

public interface VoteRepository extends JpaRepository<Vote, Integer> {

    List<Vote> findByIp(String ip);

    Vote findByIpAndPresentation(String ip, Presentation presentation);

    @Query("FROM Vote where rate IS NOT NULL and presentation = ?1")
    List<Vote> findByPresentation(Presentation presentation);

}
