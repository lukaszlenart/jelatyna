package jelatyna.domain;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.*;
import static java.util.Arrays.*;
import static org.springframework.util.Assert.*;

public class Agenda implements Serializable {
    private static final long serialVersionUID = -2466912335116267598L;
    /*
     * Please note, that this matrix is NEVER allowed to consist of null values.
     * Methods that change it's size, make sure of it by calling
     * ensureNoNullCells
     */
    private AgendaCell[][] agendaTable =
            {
                    {new AgendaCell(0, 0, "Godzina/Sala"), new AgendaCell(0, 1, "Sala 1"), new AgendaCell(0, 2, "Sala 2")},
                    {new AgendaCell(1, 0, "10-11"), new AgendaCell(1, 1, "Jacek - Klozure"),
                            new AgendaCell(1, 2, "Sławek - DDD")},
                    {new AgendaCell(2, 0, "11-12"), new AgendaCell(2, 1, "Jacek - Agile"),
                            new AgendaCell(2, 2, "Sławek - BDD")},
                    {new AgendaCell(3, 0, "12-13"), new AgendaCell(3, 1, "Jacek - Zkala"),
                            new AgendaCell(3, 2, "Sławek - TDD")},
                    {new AgendaCell(4, 0, "13-14"), new AgendaCell(4, 1, "Jacek - Dart"),
                            new AgendaCell(4, 2, "Sławek - Java")}
            };

    public int getNumberOfTimeSlots() {
        return agendaTable.length;
    }

    public void setNumberOfTimeSlots(int numberOfTimeSlots) {
        isTrue(numberOfTimeSlots > 0);
        agendaTable = copyOfRange(agendaTable, 0, numberOfTimeSlots);
        ensureNoNullCells();
    }

    private void ensureNoNullCells() {
        int numberOfRooms = getNumberOfRooms();
        for (int rowNumber = 0; rowNumber < agendaTable.length; rowNumber++) {
            fillRowIfEmpty(numberOfRooms, rowNumber);
            for (int columnNumber = 0; columnNumber < numberOfRooms; columnNumber++) {
                fillCellIfEmpty(rowNumber, columnNumber);
            }
        }
    }

    private void fillCellIfEmpty(int rowNumber, int columnNumber) {
        if (agendaTable[rowNumber][columnNumber] == null) {
            agendaTable[rowNumber][columnNumber] = new AgendaCell(rowNumber, columnNumber, "TBD");
        }
    }

    private void fillRowIfEmpty(int numberOfRooms, int rowNumber) {
        if (agendaTable[rowNumber] == null) {
            agendaTable[rowNumber] = new AgendaCell[numberOfRooms];
        }
    }

    public int getNumberOfRooms() {
        if (agendaTable[0] != null) {
            return agendaTable[0].length;
        }
        return 0;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        isTrue(numberOfRooms > 0);
        AgendaCell[][] newAgenda = new AgendaCell[getNumberOfTimeSlots()][numberOfRooms];
        for (int rowNumber = 0; rowNumber < agendaTable.length; rowNumber++) {
            newAgenda[rowNumber] = copyOfRange(agendaTable[rowNumber], 0, numberOfRooms);
        }
        agendaTable = newAgenda;
        ensureNoNullCells();
    }

    /*
     * All you get is a copy of the matrix, but Cells are for real. You can
     * modify them, but there is setCell method to that for you convenience
     * already.
     */
    public AgendaCell[][] getMatrix() {
        AgendaCell[][] deepCopyOfTable = new AgendaCell[getNumberOfTimeSlots()][getNumberOfRooms()];
        for (int rowNumber = 0; rowNumber < agendaTable.length; rowNumber++) {
            deepCopyOfTable[rowNumber] = agendaTable[rowNumber].clone();
        }
        return deepCopyOfTable;
    }

    public void setCell(int colNum, int rowNum, Presentation presentation) {
        agendaTable[rowNum][colNum] = new AgendaCell(rowNum, colNum, presentation.getId());
    }

    public void setCell(int colNum, int rowNum, String value) {
        agendaTable[rowNum][colNum] = new AgendaCell(rowNum, colNum, value);
    }

    public void clearCell(int colNum, int rowNum) {
        agendaTable[rowNum][colNum] = new AgendaCell(rowNum, colNum, "Przerwa");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return deepEquals(agendaTable, ((Agenda) o).getMatrix());
    }

    @Override
    public int hashCode() {
        return 0;
    }

    public List<String> getRooms() {
        return stream(agendaTable[0]).map(cell -> cell.getValue()).collect(Collectors.toList());
    }

    public List<TimeSlot> getSlots() {
        List<TimeSlot> slots = newArrayList();
        for (int rowIdx = 1; rowIdx < agendaTable.length; rowIdx++) {
            slots.add(createTimeSlotFor(rowIdx));
        }
        return slots;
    }

    private TimeSlot createTimeSlotFor(int rowIdx) {
        TimeSlot slot = new TimeSlot(agendaTable[rowIdx][0].value);
        for (int columnIdx = 1; columnIdx < agendaTable[rowIdx].length; columnIdx++) {
            slot.addItem(agendaTable[rowIdx][columnIdx].toAgendaItem());
        }
        return slot;
    }
}
