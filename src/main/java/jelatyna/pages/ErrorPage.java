package jelatyna.pages;

import jelatyna.pages.confitura.BaseWebPage;

@SuppressWarnings("serial")
public class ErrorPage extends BaseWebPage {

    public ErrorPage() {
        setPageTitlePostfix("Błąd");
    }

    @Override
    public boolean isErrorPage() {
        return true;
    }

    @Override
    public boolean isVersioned() {
        return false;
    }

}
