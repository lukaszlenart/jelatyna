package jelatyna.pages.confitura.sponsor;

import jelatyna.components.StaticImage;
import jelatyna.domain.Sponsor;
import jelatyna.domain.SponsorType;
import jelatyna.services.FileService;
import jelatyna.services.SponsorService;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class SponsorWidget extends Panel {

    @SpringBean
    private SponsorService service;
    @SpringBean
    private FileService fileService;

    public SponsorWidget(String id, List<SponsorType> sponsorTypes) {
        super(id);
        add(new SponsorsByTypeList(sponsorTypes));
    }

    private Component createSponsorLogo(Sponsor sponsor) {
        ExternalLink link = new ExternalLink("webPage", sponsor.getWebPage());
        link.add(new StaticImage("logo", fileService.getUrlTo(sponsor), sponsor.getName()));
        return link;
    }

    private final class SponsorsByTypeList extends ListView<SponsorType> {
        private SponsorsByTypeList(List<SponsorType> sponsorTypes) {
            super("sponsors", sponsorTypes);
        }

        @Override
        protected void populateItem(ListItem<SponsorType> item) {
            SponsorType sponsorType = item.getModelObject();
            item.setRenderBodyOnly(true);
            item.add(label("type", sponsorType.getName()));
            item.add(new SponsorList(getSponsors(sponsorType)));
        }

        private List<Sponsor> getSponsors(SponsorType sponsorType) {
            return service.findDisplayableSponsorsBy(sponsorType);
        }
    }

    private final class SponsorList extends ListView<Sponsor> {

        private SponsorList(List<Sponsor> sponsors) {
            super("sponsor", sponsors);
        }

        @Override
        protected void populateItem(ListItem<Sponsor> item) {
            item.add(createSponsorLogo(item.getModelObject()));
        }
    }

}
