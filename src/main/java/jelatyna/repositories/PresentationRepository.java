package jelatyna.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import jelatyna.domain.Presentation;

public interface PresentationRepository extends JpaRepository<Presentation, Integer> {
    @Query("FROM Presentation WHERE accepted = true ORDER BY title")
    List<Presentation> findAllAccepted();

}
