package jelatyna.components.user;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.markup.html.panel.Panel;

import jelatyna.domain.User;

public class PasswordPanel extends Panel {
    public PasswordPanel(String id, User user) {
        super(id);
        setVisible(user.isNew());
        PasswordTextField password = passwordField("password", propertyModel(user, "plainPassword"), true);
        PasswordTextField repassword = passwordField("repassword", model(), true);
        add(i18nLabel("password.label"), withLabelKey("password.label", password));
        add(i18nLabel("password.repeat.label"), withLabelKey("password.repeat.label", repassword));
        add(new EqualPasswordInputValidator(password, repassword));
    }

}
