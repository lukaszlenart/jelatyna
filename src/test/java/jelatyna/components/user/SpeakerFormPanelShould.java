package jelatyna.components.user;

import jelatyna.WicketTest;
import jelatyna.components.user.speaker.SpeakerFormPanel;
import jelatyna.domain.BioLocation;
import jelatyna.domain.Speaker;
import jelatyna.pages.admin.AdminHomePage;
import jelatyna.services.AdminService;
import jelatyna.services.UserService;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SpeakerFormPanelShould extends WicketTest{

    private Speaker speaker = new Speaker();

    @Mock
    private UserService<Speaker> userService;

    @Mock
    private AdminService adminService;

    private static final String VALID_BIO =
            "min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków " +
            "min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków " +
            "min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków min 300 znaków ";


    @Override
    public void setupServices() {
        super.setupServices();
        put(userService);
        put(adminService);
    }

    @Test
    public void change_bio_location_value()
    {
        tester.startComponentInPage(new SpeakerFormPanel(userService, speaker, AdminHomePage.class));

        FormTester formTester = tester.newFormTester("userForm:form");
        formTester.setValue("firstName", "John");
        formTester.setValue("lastName", "Smith");
        formTester.setValue("mail", "my@mail.com");
        formTester.setValue("passwordPanel:password", "aaaaa");
        formTester.setValue("passwordPanel:repassword", "aaaaa");
        formTester.setValue("bio", VALID_BIO);

        formTester.setValue("bioLocation", "RIGHT");
        formTester.submit();

        assertThat(speaker.getBioLocation()).isEqualTo(BioLocation.RIGHT);
    }

}
