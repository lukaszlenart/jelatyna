package jelatyna.pages.confitura.feed;

import com.sun.syndication.feed.atom.Content;
import com.sun.syndication.feed.atom.Entry;
import com.sun.syndication.feed.atom.Feed;
import com.sun.syndication.feed.atom.Link;
import jelatyna.Confitura;
import jelatyna.domain.News;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.feed.AbstractAtomFeedView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.*;

//This class will not be automatically tested, until spring-test-mvc projects releases anything. I don't feel confident enought to add SNAPSHOT dependency
@Component
public class AtomFeedView extends AbstractAtomFeedView {

    private String feedId = "tag:confitura.pl";
    private String title = "Confitura: news";

    @Override
    protected void buildFeedMetadata(Map<String, Object> model, Feed feed, HttpServletRequest request) {
        feed.setId(feedId);
        feed.setTitle(title);
        setUpdatedIfNeeded(model, feed);
    }

    private void setUpdatedIfNeeded(Map<String, Object> model, Feed feed) {
        Date lastUpdate = (Date) model.get(FeedController.LAST_UPDATE_VIEW_KEY);
        if (thisIsTheFirstUpdate(feed, lastUpdate) || freshNewsHaveArrived(feed, lastUpdate)) {
            feed.setUpdated(lastUpdate);
        }
    }

    private boolean freshNewsHaveArrived(Feed feed, Date lastUpdate) {
        return feed.getUpdated() != null && lastUpdate != null && lastUpdate.compareTo(feed.getUpdated()) > 0;
    }

    private boolean thisIsTheFirstUpdate(Feed feed, Date lastUpdate) {
        return feed.getUpdated() == null && lastUpdate != null;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected List<Entry> buildFeedEntries(Map<String, Object> model, HttpServletRequest request,
                                           HttpServletResponse response) throws Exception {
        List<News> newsList = (List<News>) model.get(FeedController.NEWS_VIEW_KEY);
        List<Entry> entries = new ArrayList<Entry>();
        for (News news : newsList) {
            addEntry(entries, news);
        }
        return entries;
    }

    private void addEntry(List<Entry> entries, News news) {
        Entry entry = new Entry();
        entry.setId(feedId + ", " + news.getId());
        entry.setTitle(news.getTitle());
        entry.setUpdated(news.getCreationDate());
        entry = setSummary(news, entry);
        entry = setLink(news, entry);
        entries.add(entry);
    }

    private Entry setSummary(News news, Entry entry) {
        Content summary = new Content();
        summary.setType("html");
        summary.setValue(news.getShortDescription());
        entry.setSummary(summary);
        return entry;
    }

    private Entry setLink(News news, Entry entry) {
        Link link = new Link();
        link.setType("text/html");
        link.setHref(createAbsoluteNewsUrl(news));
        entry.setAlternateLinks(newArrayList(link));
        return entry;
    }

    private String createAbsoluteNewsUrl(News news) {
        return Confitura.NEWS_BASE_URL + "/" + news.getTitle();
    }

}
