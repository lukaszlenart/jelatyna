package jelatyna.repositories;

import org.springframework.data.repository.Repository;

import jelatyna.domain.Admin;

public interface AdminRepository extends Repository<Admin, Integer>, UserRepository<Admin> {

}
