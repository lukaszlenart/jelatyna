package jelatyna.pages.admin.news;

import jelatyna.WicketTest;
import jelatyna.domain.News;
import jelatyna.services.NewsService;
import org.apache.wicket.util.tester.FormTester;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;


public class AddNewsPageShould extends WicketTest {

    @Before
    public void setupServices() {
        withAdmin();
        put(mock(NewsService.class));
    }

    @Test
    public void fillNewsWithProvidedData(){
        //given
        News expected = defaultNews();
        tester.startPage(AddNewsPage.class);
        FormTester formTester = tester.newFormTester("form");

        //when
        formTester.setValue("title", expected.getTitle());
        formTester.setValue("published", expected.isPublished());
        formTester.setValue("description", expected.getDescription());
        formTester.setValue("shortDescription", expected.getShortDescription());
//        formTester.setValue("creationDate:date", new DateTime(expected.getCreationDate()).toString("yy-MM-dd"));
//        formTester.setValue("creationDate:hours", "00");
//        formTester.setValue("creationDate:minutes", "00");
        formTester.submit();

        AddNewsPage.AddNewsForm form = (AddNewsPage.AddNewsForm) formTester.getForm();

        //then
        assertFalse(form.hasError());
        assertThat(form.news.getTitle()).isEqualTo(expected.getTitle());
        assertThat(form.news.isPublished()).isEqualTo(expected.isPublished());
        assertThat(form.news.getDescription()).isEqualTo(expected.getDescription());
        assertThat(form.news.getShortDescription()).isEqualTo(expected.getShortDescription());
//        assertThat(form.news.getCreationDate()).isEqualTo(expected.getCreationDate());


    }

    private News defaultNews() {
        return new News()
                .title("news")
                .description("description")
                .shortDescription("short description")
                .published(true)
                .creationDate(new DateTime().withDate(2012, 1, 1).withTime(0, 0, 0, 0).toDate());
    }
}
