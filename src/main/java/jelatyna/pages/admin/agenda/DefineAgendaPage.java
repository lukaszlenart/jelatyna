package jelatyna.pages.admin.agenda;

import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.components.nogeneric.Link;
import jelatyna.components.validatotion.MoreThanZeroIntegerValidator;
import jelatyna.domain.Agenda;
import jelatyna.domain.AgendaCell;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.agenda.choosePresentation.ChoosePresentationPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.repositories.AgendaRepository;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.AjaxEditableLabel;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.DataGridView;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.collect.Lists.*;

@SuppressWarnings({"rawtypes", "unchecked", "serial"})
public class DefineAgendaPage extends AdminBasePage {
    static final String AGENDA_ROWS_ID = "agendaRows";
    static final String NUMBER_OF_ROOMS_ID = "numberOfRooms";
    static final String NUMBER_OF_TIME_SLOTS_ID = "numberOfTimeSlots";
    static final String FEEDBACK_ID = "feedback";
    static final String SAVE_AGENDA_IN_DB = "saveAgendaInDb";

    private IModel<Agenda> agendaModel = new AgendaInSessionModel();
    private FeedbackPanel feedbackPanel;

    @SpringBean
    private AgendaRepository agendaRepository;

    public DefineAgendaPage() {
        System.out.println("aaa");
        Agenda agenda = agendaRepository.getAgenda();
        // agendaModel = new Model<Agenda>(agenda);
        getSession().setAgenda(agenda);
        addFeedbackPanel();
        add(createNumberOfRoomsLabel(agenda));
        add(createNumberOfTimeSlotsLabel(agenda));
        add(createDataGridView());
        add(createSaveAgendaLink());
        setOutputMarkupId(true);
    }

    private Link createSaveAgendaLink() {
        return new Link(SAVE_AGENDA_IN_DB) {
            @Override
            public void onClick() {
                agendaRepository.saveAgenda(agendaModel.getObject());
                info("Agenda zapisana");
            }
        };
    }

    private AjaxEditableLabel createNumberOfTimeSlotsLabel(Agenda agenda) {
        AjaxEditableLabel numberOfTimeSlotsLabel = new AjaxEditableLabelWithFeedback(NUMBER_OF_TIME_SLOTS_ID,
                "numberOfTimeSlots", agenda);
        numberOfTimeSlotsLabel.add(new MoreThanZeroIntegerValidator());
        return numberOfTimeSlotsLabel;
    }

    private AjaxEditableLabel createNumberOfRoomsLabel(Agenda agenda) {
        AjaxEditableLabel numberOfRoomsLabel = new AjaxEditableLabelWithFeedback(NUMBER_OF_ROOMS_ID, "numberOfRooms",
                agenda);
        numberOfRoomsLabel.add(new MoreThanZeroIntegerValidator());
        return numberOfRoomsLabel;
    }

    private void addFeedbackPanel() {
        feedbackPanel = new ConfituraFeedbackPanel(FEEDBACK_ID);
        feedbackPanel.setOutputMarkupId(true);
        add(feedbackPanel);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.AGENDA;
    }

    @Override
    public String getPageTitle() {
        return "Definiowanie agendy";
    }

    public DataGridView createDataGridView() {
        List<ICellPopulator> populators = createColumnPopulators(agendaModel.getObject().getNumberOfRooms());
        IDataProvider dataProvider = new ListDataProvider<ArrayList<AgendaCell>>() {
            @Override
            protected List<ArrayList<AgendaCell>> getData() {
                return createListBasedCellMatrix(agendaModel.getObject());
            }
        };
        return new DataGridView(AGENDA_ROWS_ID, populators, dataProvider);
    }

    private List<ICellPopulator> createColumnPopulators(int numberOfRooms) {
        List<ICellPopulator> populators = newArrayList();
        populators.add(new TimespanPopulator(0, agendaModel, feedbackPanel));
        for (int columnIdx = 1; columnIdx < numberOfRooms; columnIdx++) {
            populators.add(createPresentationPopulator(columnIdx));
        }
        return populators;
    }

    private ColumnWithPossibleHeaderPopulator createPresentationPopulator(final int columnIdx) {
        return new ModalWindowCellPopulator(columnIdx, agendaModel, this, new PageInModalWindowCreator() {
            @Override
            public Page createPageInModalWindow(IModel<Agenda> agendaModel, int columnIdx,
                                                int rowIdx,
                                                ModalWindow modalWindow) {
                return new ChoosePresentationPage(modalWindow, agendaModel, columnIdx, rowIdx);
            }
        });
    }

    private List<ArrayList<AgendaCell>> createListBasedCellMatrix(Agenda agenda) {
        List<ArrayList<AgendaCell>> cells = newArrayList();
        for (AgendaCell[] cellRow : agenda.getMatrix()) {
            cells.add(new ArrayList<AgendaCell>(Arrays.asList(cellRow)));
        }
        return cells;
    }

    private class AjaxEditableLabelWithFeedback extends AjaxEditableLabel {
        public AjaxEditableLabelWithFeedback(String wicketId, String agendaPropertyName, Agenda agenda) {
            super(wicketId, new PropertyModel(agenda, agendaPropertyName));
        }

        @Override
        protected void onSubmit(AjaxRequestTarget target) {
            DefineAgendaPage.this.replace(createDataGridView());
            target.add(DefineAgendaPage.this);
        }

        @Override
        protected void onError(AjaxRequestTarget target) {
            target.add(feedbackPanel);
        }
    }
}