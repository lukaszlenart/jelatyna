package jelatyna.pages.confitura.agenda;

import jelatyna.components.RedirectLink;
import jelatyna.domain.Agenda;
import jelatyna.domain.AgendaItem;
import jelatyna.domain.TimeSlot;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.repositories.AgendaRepository;
import jelatyna.utils.Components;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class AgendaPresentationPage extends BaseWebPage {

    @SpringBean
    private AgendaRepository agendaRepository;

    public AgendaPresentationPage() {
        Agenda agenda = agendaRepository.getAgenda();
        add(createRoomsRow(agenda));
        add(createEmptyRow("span", agenda.getNumberOfTimeSlots()));
        add(createEmptyRow("header", agenda.getNumberOfTimeSlots()));
        add(createSlots(agenda));
        add(createEmptyRow("footer", agenda.getNumberOfTimeSlots()));
        add(new RedirectLink("ical", AgendaICalendarPage.class));
        setPageTitlePostfix("Agenda");
    }

    private Component createEmptyRow(String id, int colspan) {
        return new WebMarkupContainer(id).add(new AttributeModifier("colspan", colspan));
    }

    private ListView<TimeSlot> createSlots(Agenda agenda) {
        return new ListView<TimeSlot>("slots", agenda.getSlots()) {
            @Override
            protected void populateItem(ListItem<TimeSlot> item) {
                TimeSlot timeSlot = item.getModelObject();
                setClassIfOneItem(item);
                item.add(label("time", timeSlot.getTime()));
                item.add(creatItems(timeSlot));
            }

            private void setClassIfOneItem(ListItem<TimeSlot> item) {
                if (item.getModelObject().getSpan() > 1) {
                    item.add(new AttributeModifier("class", "light"));
                }
            }

        };
    }

    private ListView<AgendaItem> creatItems(final TimeSlot timeSlot) {
        return new ListView<AgendaItem>("items", timeSlot.getItems()) {
            @Override
            protected void populateItem(ListItem<AgendaItem> item) {
                WebMarkupContainer container = new WebMarkupContainer("item");
                container.add(new AttributeModifier("colspan", timeSlot.getSpan()));
                container.add(new AgendaCellComponent("cell", item.getModelObject()));
                item.add(container);
            }
        };
    }

    private ListView<String> createRoomsRow(Agenda agenda) {
        return new ListView<String>("rooms", agenda.getRooms()) {

            @Override
            protected void populateItem(ListItem<String> item) {
                item.add(Components.label("name", item.getModelObject()));
            }
        };
    }
}
