package jelatyna.domain;

public enum RegistrationStatus {
    NEW("Nowa"),
    CONFIRMED("Potwierdzona") {
        @Override
        public boolean canBeAfter(RegistrationStatus status) {
            return status == NEW || status == this;
        }
    },
    FINAL_CONFIRMED("Finalna") {
        @Override
        public boolean canBeAfter(RegistrationStatus status) {
            return status == CONFIRMED || status == this;
        }
    },
    CANCELED("Anulowana");

    private final String name;

    private RegistrationStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean canBeAfter(RegistrationStatus status) {
        return true;
    }
}
