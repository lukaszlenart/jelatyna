package jelatyna.services;

import jelatyna.domain.Admin;

public interface AdminService extends UserService<Admin> {

    @Override
    Admin loginWith(String name, String password);

}
