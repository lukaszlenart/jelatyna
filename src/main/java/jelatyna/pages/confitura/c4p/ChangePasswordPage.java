package jelatyna.pages.confitura.c4p;

import jelatyna.components.user.ChangePasswordPanel;
import jelatyna.domain.Speaker;
import jelatyna.services.SpeakerService;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class ChangePasswordPage extends SpeakerBasePage {
    @SpringBean
    private SpeakerService service;

    public ChangePasswordPage() {
        add(new ChangePasswordPanel<Speaker>(getSession().getSpeaker(), service, ViewSpeakerPage.class));
        setPageTitlePostfix(new ResourceModel("password.change.label"));
    }
}
