package jelatyna.pages.admin.agenda.choosePresentation;

import jelatyna.ConfituraBasePage;
import jelatyna.components.nogeneric.AjaxLink;
import jelatyna.domain.Agenda;
import jelatyna.domain.Presentation;
import jelatyna.repositories.PresentationRepository;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.AjaxEditableLabel;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings({"rawtypes", "unchecked", "serial"})
public class ChoosePresentationPage extends ConfituraBasePage {
    static final String PRESENTATIONS_ID = "presentations";
    static final String PRESENTATION_ID = "item";
    static final String PRESENTATION_TITLE_ID = "presentationTitle";
    static final String SPEAKER_ID = "speaker";
    static final String CLEAR_CELL_ID = "clearCell";
    static final String ENTER_CUSTOM_VALUE_ID = "enterCustomValue";

    @SpringBean
    private PresentationRepository presentationRepository;

    public ChoosePresentationPage(final ModalWindow modalWindow, final IModel<Agenda> agendaModel,
                                  final int colNum, final int rowNum) {
        add(createListView(modalWindow, agendaModel, colNum, rowNum));
        add(createClearCellLink(modalWindow, agendaModel, colNum, rowNum));
        add(createEnterCustomValueLabel(modalWindow, agendaModel, colNum, rowNum));
    }

    private AjaxEditableLabel createEnterCustomValueLabel(final ModalWindow modalWindow,
                                                          final IModel<Agenda> agendaModel, final int colNum, final int rowNum) {
        return new AjaxEditableLabel(ENTER_CUSTOM_VALUE_ID, new Model("Dowolny tekst")) {
            @Override
            protected void onSubmit(AjaxRequestTarget target) {
                agendaModel.getObject().setCell(colNum, rowNum, this.getEditor().getValue());
                modalWindow.close(target);
                target.add(modalWindow.getParent());
            }
        };
    }

    private AjaxLink createClearCellLink(final ModalWindow modalWindow,
                                         final IModel<Agenda> agendaModel, final int colNum, final int rowNum) {
        return new AjaxLink(CLEAR_CELL_ID) {
            @Override
            public void onClick(AjaxRequestTarget target) {
                agendaModel.getObject().clearCell(colNum, rowNum);
                modalWindow.close(target);
                target.add(modalWindow.getParent());
            }
        };
    }

    private ListView<Presentation> createListView(final ModalWindow modalWindow, final IModel<Agenda> agendaModel,
                                                  final int colNum, final int rowNum) {
        ListView<Presentation> listView = new ListView<Presentation>(PRESENTATIONS_ID,
                presentationRepository.findAllAccepted()) {
            @Override
            protected void populateItem(final ListItem<Presentation> item) {
                Presentation presentation = item.getModelObject();
                item.add(createPresentationLink(modalWindow, agendaModel, colNum, rowNum, item, presentation));
                item.add(new Label(SPEAKER_ID, presentation.getOwner().getFullName()));
            }

        };
        listView.setOutputMarkupId(true);
        return listView;
    }

    private AjaxLink createPresentationLink(final ModalWindow modalWindow, final IModel<Agenda> agendaModel,
                                            final int colNum, final int rowNum, final ListItem<Presentation> item, Presentation presentation) {
        AjaxLink ajaxLink = new AjaxLink(PRESENTATION_ID) {
            @Override
            public void onClick(AjaxRequestTarget target) {
                agendaModel.getObject().setCell(colNum, rowNum, item.getModelObject());
                modalWindow.close(target);
                target.add(modalWindow.getParent());
            }
        };
        ajaxLink.add(new Label(PRESENTATION_TITLE_ID, presentation.getTitle()));
        return ajaxLink;
    }
}