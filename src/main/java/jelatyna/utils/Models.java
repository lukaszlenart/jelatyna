package jelatyna.utils;

import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import java.io.Serializable;

public class Models {
    public static Model<String> model() {
        return new Model<String>();
    }

    public static Model<String> model(String value) {
        return Model.of(value);
    }

    public static <T extends Serializable> PropertyModel<T> propertyModel(Object value, String propertyName) {
        return PropertyModel.of(value, propertyName);
    }
}
