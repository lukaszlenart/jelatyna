package jelatyna.pages.admin.registration;

import static com.google.common.collect.Lists.*;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.util.CollectionModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.components.mail.DefineMailPanel;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.ParticipantService;

@SuppressWarnings("serial")
public class GroupMailPage extends AdminBasePage {

    @SpringBean
    private ParticipantService service;

    public GroupMailPage() {
        Form<Void> form = new SendMailForm("form");
        form.add(AttributeModifier.replace("onsubmit",
                "return confirm('Wysyłać maila do uczestników o wybranym statusie?');"));
        add(form);
        add(new DefineMailPanel("mail", "group_mail", "$firstName, $lastName, $confirmLink, $cancelLink, $statusLink"));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "Mail grupowy";
    }

    private final class SendMailForm extends Form<Void> {
        private CheckBoxMultipleChoice<RegistrationStatus> statusChoice = new CheckBoxMultipleChoice<RegistrationStatus>(
                "statuses", new CollectionModel<RegistrationStatus>(), newArrayList(RegistrationStatus.values()));
        private CheckBox participated = new CheckBox("participated", new Model<Boolean>());
        private CheckBox mailing = new CheckBox("mailing", new Model<Boolean>());

        private SendMailForm(String id) {
            super(id);
            add(statusChoice);
            add(participated);
            add(mailing);
        }

        @Override
        protected void onSubmit() {
            service.sendGroupMessage(statusChoice.getModelObject(), mailing.getModelObject(),
                    participated.getModelObject());
        }
    }
}
