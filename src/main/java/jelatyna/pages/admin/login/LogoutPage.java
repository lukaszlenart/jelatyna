package jelatyna.pages.admin.login;

import jelatyna.ConfituraSession;
import org.apache.wicket.markup.html.WebPage;


@SuppressWarnings("serial")
public class LogoutPage extends WebPage {

    public LogoutPage() {
        ConfituraSession.get().invalidate();
    }

}
