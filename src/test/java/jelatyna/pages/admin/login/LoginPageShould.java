package jelatyna.pages.admin.login;

import jelatyna.ConfituraSession;
import jelatyna.WicketTest;
import jelatyna.domain.Admin;
import jelatyna.pages.admin.AdminHomePage;
import jelatyna.services.AdminService;
import jelatyna.services.NewsService;
import jelatyna.services.exceptions.LoginException;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LoginPageShould extends WicketTest {

    @Mock
    private AdminService service;

    @Mock
    private NewsService newsService;

    @Before
    public void setupLocale() {
        Locale.setDefault(new Locale("pl"));
    }

    @Override
    public void setupServices() {
        put(service);
        put(newsService);
        tester.startPage(LoginPage.class);
    }

    @Test
    public void loginCorrectUser() {
        // given
        Admin admin = new Admin();
        when(service.loginWith(anyString(), anyString())).thenReturn(admin);

        // when
        submitLoginFormWith("my@email.com", "correct-password");

        // then
        tester.assertNoErrorMessage();
        tester.assertRenderedPage(AdminHomePage.class);
        assertThat(adminFromSession()).isSameAs(admin);
        verify(service).loginWith("my@email.com", "correct-password");
    }

    @Test
    public void showErrorIfUserNameIsEmpty() {

        // when
        submitLoginFormWith("", "password");

        // then
        // Doesn't take locale settings on CI server
        // tester.assertErrorMessages("Pole 'e-mail' jest wymagane.");
        assertThat(adminFromSession()).isNull();
        verifyNoInteractionWithService();
    }

    @Test
    public void showErrorIfPasswordIsEmpty() {

        // when
        submitLoginFormWith("name", "");

        // then
        // Doesn't take locale settings on CI server
        // tester.assertErrorMessages("Pole 'Hasło' jest wymagane.");
        assertThat(adminFromSession()).isNull();
        verifyNoInteractionWithService();
    }

    @Test
    public void showErrorsOnLoginException() {
        // given
        String errorMessage = "my message";
        doThrow(new LoginException(errorMessage)).when(service).loginWith(anyString(), anyString());

        // when
        submitLoginFormWith("my@email.com", "invalidPassword");

        // then
        tester.assertErrorMessages(errorMessage);
        assertThat(adminFromSession()).isNull();
        verify(service).loginWith("my@email.com", "invalidPassword");
    }

    private Admin adminFromSession() {
        return getSession().getAdmin();
    }

    private ConfituraSession getSession() {
        return (ConfituraSession) tester.getSession();
    }

    private void verifyNoInteractionWithService() {
        verifyZeroInteractions(service);
    }

    private void submitLoginFormWith(String username, String password) {
        FormTester form = tester.newFormTester(LoginPage.FORM_ID);
        form.setValue(LoginPage.EMAIL_FIELD_ID, username);
        form.setValue(LoginPage.PASSWORD_FIELD_ID, password);
        form.submit();
    }

}
