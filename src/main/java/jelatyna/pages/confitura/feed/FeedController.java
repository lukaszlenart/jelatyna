package jelatyna.pages.confitura.feed;

import jelatyna.Confitura;
import jelatyna.domain.News;
import jelatyna.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.feed.AbstractAtomFeedView;

import java.util.Date;
import java.util.List;

@Controller
public class FeedController {
    static final String LAST_UPDATE_VIEW_KEY = "lastUpdate";
    static final String NEWS_VIEW_KEY = "news";
    @Autowired
    private NewsService service;
    @Autowired
    private AbstractAtomFeedView view;

    @RequestMapping(value = Confitura.FEED_URL, method = RequestMethod.GET)
    @Transactional
    public ModelAndView feed() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(view);
        List<News> news = service.fetchPublished();
        modelAndView.addObject(NEWS_VIEW_KEY, news);
        modelAndView.addObject(LAST_UPDATE_VIEW_KEY, getCreationDateOfTheLast(news));
        return modelAndView;
    }

    private Date getCreationDateOfTheLast(List<News> news) {
        if (news.size() > 0) {
            return news.get(0).getCreationDate();
        }
        return new Date(0);
    }
}
