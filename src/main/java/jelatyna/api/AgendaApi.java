package jelatyna.api;

import static java.util.Arrays.*;
import static java.util.stream.Collectors.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jelatyna.domain.AgendaCell;
import jelatyna.domain.dto.AgendaDto;
import jelatyna.domain.dto.AgendaScheduleDto;
import jelatyna.domain.dto.AgendaSlotDto;
import jelatyna.domain.dto.SimpleAgendaItemDto;
import jelatyna.domain.dto.TimeSlotDto;
import jelatyna.domain.dto.WithTitle;
import jelatyna.domain.dto.converters.PresentationConverter;
import jelatyna.repositories.AgendaRepository;
import jelatyna.services.PresentationService;

@Controller
public class AgendaApi {

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private PresentationService service;

    @Autowired
    private PresentationConverter converter;

    @RequestMapping(value = "agenda", method = RequestMethod.GET)
    @ResponseBody
    public AgendaDto agenda() {
        AgendaCell[][] matrix = agendaRepository.getAgenda().getMatrix();

        return new AgendaDto()
                .rooms(collectRooms(matrix))
                .slots(collectSlots(matrix))
                .schedule(collectSchedule(matrix));
    }

    private List<String> collectRooms(AgendaCell[][] a) {
        return asList(a[0])
                .stream().skip(1)
                .map(AgendaCell::getValue)
                .collect(toList());
    }

    private List<TimeSlotDto> collectSlots(AgendaCell[][] matrix) {
        return asList(matrix)
                .stream().skip(1)
                .map(s -> slot(s[0]))
                .collect(toList());
    }

    private List<AgendaScheduleDto> collectSchedule(AgendaCell[][] matrix) {
        return stream(matrix).skip(1)
                .map((AgendaCell[] items) -> new AgendaScheduleDto()
                        .slotId(items[0].rowNum)
                        .presentations(collectPresentationsFrom(items)))
                .collect(toList());
    }

    private List<AgendaSlotDto> collectPresentationsFrom(AgendaCell[] items) {
        List<AgendaSlotDto> dtos = stream(items)
                .skip(1)
                .map(this::presentations).collect(toList());
        if (dtos.stream().map((AgendaSlotDto item) -> item.getPresentation().getTitle()).distinct().count() == 1) {
            dtos = dtos.subList(0, 1);
            dtos.get(0).room("ALL");
        }
        return dtos;
    }

    private TimeSlotDto slot(AgendaCell s) {
        String[] time = s.value.split("-");
        return new TimeSlotDto()
                .id(s.rowNum)
                .start(time(time[0]))
                .end(time(time[1]));
    }

    private String time(String s) {
        return s.length() == 5 ? s : s + ":00";
    }

    private AgendaSlotDto presentations(AgendaCell item) {
        return new AgendaSlotDto()
                .room(agendaRepository.getAgenda().getRooms().get(item.colNum))
                .item(getSlotItemFor(item));
    }

    private WithTitle getSlotItemFor(AgendaCell agendaCell) {
        if (agendaCell.hasPresentation()) {
            return converter.map(service.findBy(agendaCell.presentationId));
        } else {
            return new SimpleAgendaItemDto(agendaCell.getValue());
        }

    }
}
