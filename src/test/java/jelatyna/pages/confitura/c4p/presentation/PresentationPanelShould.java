package jelatyna.pages.confitura.c4p.presentation;

import jelatyna.WicketTest;
import jelatyna.domain.Presentation;
import jelatyna.services.PresentationServiceImpl;
import jelatyna.services.SpeakerService;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class PresentationPanelShould extends WicketTest {
    private static final String ID = "id";
    private Presentation presentation = new Presentation();

    @Override
    @Before
    public void setupServices() {
        put(mock(PresentationServiceImpl.class));
        put(mock(SpeakerService.class));
    }

    @Test
    public void notShowParleysAndYouTubeFieldsForPresenter() {

        tester.startComponentInPage(PresentationPanel.forPresenter(ID, presentation));

        tester.assertInvisible(ID + ":form:movies");
    }

    @Test
    public void notShowParleysAndYouTubeFieldsForAdmin() {

        tester.startComponentInPage(PresentationPanel.forAdmin(ID, presentation));

        tester.assertVisible(ID + ":form:movies");
    }

}
