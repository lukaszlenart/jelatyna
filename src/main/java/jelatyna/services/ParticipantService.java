package jelatyna.services;

import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.registration.GroupMailSender;
import jelatyna.pages.admin.registration.Participants;
import jelatyna.repositories.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

import static java.util.Collections.*;
import static org.apache.commons.lang.StringUtils.*;

@Service
public class ParticipantService {
    @Autowired
    private ParticipantRepository repository;
    @Autowired
    private GroupMailSender sender;

    public Participant findByMail(String eMail) {
        return repository.findByMail(eMail);
    }

    public Participant findByToken(String token) {
        return repository.findByToken(token);
    }

    public List<Participant> findByLastName(String value) {
        if (isBlank(value)) {
            return emptyList();
        }
        return repository.findWithLastNameLike(value + "%");
    }

    public void toogleParticipation(Participant item) {
        item.participated(!item.isParticipated());
        repository.save(item);
    }

    public Participants findByStatus(Collection<RegistrationStatus> statuses) {
        if (statuses.isEmpty()) {
            return new Participants(repository.findAll());
        }
        return new Participants(repository.findByRegistrationTypeIn(statuses));
    }

    public void sendGroupMessage(Collection<RegistrationStatus> statuses, boolean mailing, boolean participated) {
        Participants participants = findByStatus(statuses);
        sender.sendMessages(participants.limitToWithMailing(mailing).limitToParticipated(participated).asList());
    }

    public List<Participant> allParticipated() {
        return repository.findRealParticipants();
    }
}
