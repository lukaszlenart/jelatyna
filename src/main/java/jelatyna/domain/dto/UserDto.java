package jelatyna.domain.dto;

public class UserDto<T extends UserDto> {
    private String firstName;

    private String lastName;

    private String bio;

    private String photo;

    private String twitter;

    private String url;

    private Integer id;

    private String fullName;

    public T firstName(String firstName) {
        this.firstName = firstName;
        return (T) this;
    }

    public T lastName(String lastName) {
        this.lastName = lastName;
        return (T) this;
    }

    public T bio(String bio) {
        this.bio = bio;
        return (T) this;
    }

    public T photo(String photo) {
        this.photo = photo;
        return (T) this;
    }

    public T twitter(String twitter) {
        this.twitter = twitter;
        return (T) this;
    }

    public T url(String url) {
        this.url = url;
        return (T) this;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBio() {
        return bio;
    }

    public String getPhoto() {
        return photo;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getUrl() {
        return url;
    }

    public Integer getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public T id(Integer id) {
        this.id = id;
        return (T) this;
    }

    public T fullName(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = firstName + " " + lastName;
        return (T) this;
    }


}
