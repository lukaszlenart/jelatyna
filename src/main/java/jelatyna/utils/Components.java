package jelatyna.utils;

import jelatyna.components.RedirectLink;
import jelatyna.components.RichEditor;
import jelatyna.components.StaticImage;
import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.validation.validator.RfcCompliantEmailAddressValidator;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.extensions.yui.calendar.DateTimeField;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static org.apache.commons.lang.BooleanUtils.*;

public class Components {

    public static <T extends Serializable> TextField<T> textField(String id) {
        return new TextField<T>(id);
    }

    public static <T extends Serializable> TextArea<T> textArea(String id, IModel<T> model) {
        return new TextArea<T>(id, model);
    }

    public static <T extends Serializable> TextField<T> textField(String id, IModel<T> model) {
        return new TextField<T>(id, model);
    }

    public static TextField<String> mailField(String id, IModel<String> model) {
        TextField<String> textField = new TextField<String>(id, model);
        textField.add(RfcCompliantEmailAddressValidator.getInstance());
        textField.setRequired(true);
        return textField;
    }

    public static <T extends Serializable> TextField<T> textField(String id, IModel<T> model, boolean required) {
        TextField<T> textField = new TextField<T>(id, model);
        textField.setRequired(required);
        return textField;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> PasswordTextField passwordField(String id, IModel<T> model, boolean required) {
        return (PasswordTextField) new PasswordTextField(id, (IModel<String>) model).setRequired(required);
    }

    public static DateTextField dateTextField(String creationDate, IModel<Date> model) {
        DateTextField textField = new DateTextField(creationDate, model, "dd-MM-yyyy");
        textField.add(datePicker());
        return textField;
    }

    public static DateTimeField dateTimeField(String creationDate, IModel<Date> model) {
        return new DateTimeField(creationDate, model);
    }

    public static DatePicker datePicker() {
        DatePicker datePicker = new DatePicker();
        datePicker.setShowOnFieldClick(true);
        datePicker.setAutoHide(true);
        return datePicker;
    }

    public static <T extends Serializable> RichEditor<T> richEditor(String id, IModel<T> model) {
        return new RichEditor<T>(id, model);
    }

    public static Label label(String id) {
        return new Label(id);
    }

    public static Label label(String id, IModel<?> model) {
        return new Label(id, model);
    }

    public static Label label(String id, Object content) {
        return new Label(id, content.toString());
    }

    public static Label label(String id, String content) {
        return new Label(id, content);
    }

    public static <T extends Serializable> DropDownChoice<T> dropDown(String id, Model<T> model,
                                                                      List<? extends T> values) {
        return new DropDownChoice<T>(id, model, values);
    }

    public static <T extends Serializable> DropDownChoice<T> dropDown(String id, PropertyModel<T> model,
                                                                      List<? extends T> values) {
        return new DropDownChoice<T>(id, model, values);
    }

    public static <T extends Serializable> DropDownChoice<T> dropDown(String id, PropertyModel<T> model,
                                                                      List<? extends T> values, IChoiceRenderer<T> renderer) {
        return new DropDownChoice<T>(id, model, values, renderer);
    }

    public static Label richLabel(String id, String content) {
        return (Label) new Label(id, content).setEscapeModelStrings(false);
    }

    public static RedirectLink redirectLink(String id, Class<? extends Page> page) {
        return new RedirectLink(id, page);
    }

    public static RedirectLink cancelLink(Class<? extends Page> page) {
        return new RedirectLink("cancel", page);
    }

    public static RedirectLink cancelLinkWithLabel(Class<? extends Page> page) {
        RedirectLink cancelLink = cancelLink(page);
        cancelLink.add(i18nLabel("cancel.label"));
        return cancelLink;
    }

    public static <T extends FormComponent<?>> T withLabel(String label, T comp) {
        comp.setLabel(new Model<String>(label));
        return comp;
    }

    public static <T extends FormComponent<?>> T withLabelKey(String key, T comp) {
        comp.setLabel(new ResourceModel(key));
        return comp;
    }

    public static <T extends FormComponent<?>> Component inLabel(String id, String key, T comp) {
        comp.setLabel(new ResourceModel(key));
        WebMarkupContainer container = new WebMarkupContainer(id);
        container.add(i18nLabel(key), comp);
        return container;
    }

    public static <T extends FormComponent<?>> T required(T comp) {
        comp.setRequired(true);
        return comp;
    }

    public static StaticImage yesNoIcon(String id, boolean is) {
        String yesNo = toStringYesNo(is);
        return new StaticImage(id, yesNoIconPath(yesNo), yesNo);
    }

    private static String yesNoIconPath(String yesNo) {
        return String.format("/img/admin/%s.png", yesNo);
    }

    public static WebComponent i18nLabel(String key) {
        return i18nLabel(key, key);
    }

    public static WebComponent i18nLabel(String id, String key) {
        return new Label(id, new ResourceModel(key));
    }

    public static WebComponent i18nLabel(String id, StringResourceModel model) {
        return new Label(id, model);
    }

}
