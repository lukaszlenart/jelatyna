package jelatyna.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import jelatyna.domain.User;

public interface UserRepository<USER extends User<?>> extends JpaRepository<USER, Integer> {
    USER findByMail(String eMail);


}
