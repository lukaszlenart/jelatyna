package jelatyna.pages.confitura.registration.form;


import jelatyna.domain.Participant;
import jelatyna.pages.confitura.registration.Registration;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.DefaultCssAutoCompleteTextField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.EqualInputValidator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Iterator;

import static com.google.common.collect.Lists.*;
import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

public class ParticipantDetailsEditFormPanel extends Panel {

    public static final String MAILING_ERROR = "Aby się zarejestrować musisz zaakceptować mailing od naszych partnerów";
    public static final String FIRST_NAME_FIELD_ID = "firstName";
    public static final String LAST_NAME_FIELD_ID = "lastName";
    public static final String MAIL_FIELD_ID = "mail";
    public static final String MAIL_REPEATED_FIELD_ID = "mailRepeated";
    public static final String CITY_FIELD_ID = "city";
    public static final String SEX_FIELD_ID = "sex";
    public static final String INFO_FIELD_ID = "info";
    public static final String POSITION_FIELD_ID = "position";
    public static final String EXPERIENCE_FIELD_ID = "experience";
    public static final String SIZE_FIELD_ID = "size";

    @SpringBean
    private Registration registration;

    @SpringBean
    private RegistrationOptions options;

    public ParticipantDetailsEditFormPanel(String id, Form parentForm, Participant participant) {
        super(id);

        add(withLabel("Imię", textField(FIRST_NAME_FIELD_ID, propertyModel(participant, "firstName"), true)));
        add(withLabel("Nazwisko", textField(LAST_NAME_FIELD_ID, propertyModel(participant, "lastName"), true)));
        TextField<String> emailField = required(mailField(MAIL_FIELD_ID, PropertyModel.<String>of(participant, "mail")));
        add(withLabel("e-mail", emailField));
        TextField<String> emailRepeatedField = required(mailField(MAIL_REPEATED_FIELD_ID, model()));
        add(withLabel("Powtórz e-mail", emailRepeatedField));
        parentForm.add(new EqualInputValidator(emailField, emailRepeatedField));

        add(withLabel("Miasto", required(new CityTextField(CITY_FIELD_ID, participant))));
        add(required(withLabel(
                "Płeć",
                dropDown(SEX_FIELD_ID, propertyModel(participant, "sex"),
                        newArrayList(Participant.WOMAN, Participant.MAN))
        )));
        add(required(withLabel("Rozmiar koszulki",
                dropDown(SIZE_FIELD_ID, propertyModel(participant, "size"), options.getShirtSize()))));
        add(required(withLabel("O konferencji dowiedziałem się z",
                dropDown(INFO_FIELD_ID, propertyModel(participant, "info"), options.getInfo()))));
        add(required(withLabel("Pozycja",
                dropDown(POSITION_FIELD_ID, propertyModel(participant, "position"), options.getPosition()))));
        add(required(withLabel("Doświadczenie",
                dropDown(EXPERIENCE_FIELD_ID, propertyModel(participant, "experience"), options.getExperiance()))));
    }

    private final class CityTextField extends DefaultCssAutoCompleteTextField<String> {
        private CityTextField(String string, Participant participant) {
            super(string, new PropertyModel<String>(participant, "city"));
        }

        @Override
        protected Iterator<String> getChoices(String input) {
            return registration.fetchCitiesStartingWith(input).iterator();
        }
    }
}
