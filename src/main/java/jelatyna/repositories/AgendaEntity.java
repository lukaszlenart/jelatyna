package jelatyna.repositories;

import jelatyna.domain.Agenda;
import org.apache.xerces.impl.dv.util.Base64;
import org.springframework.data.domain.Persistable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

@SuppressWarnings("serial")
@Entity
public class AgendaEntity implements Persistable<String> {
    public static final String AGENDA_KEY = "agenda";

    @Id
    private String id = AGENDA_KEY;

    @Lob
    private String value;

    protected AgendaEntity() {
    }

    public AgendaEntity(Agenda agenda) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(agenda);
            objectOutputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        value = Base64.encode(byteArrayOutputStream.toByteArray());
    }

    public Agenda getAgenda() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(Base64.decode(value)));
            return (Agenda) objectInputStream.readObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return false;
    }
}
