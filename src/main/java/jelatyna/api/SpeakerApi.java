package jelatyna.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.emory.mathcs.backport.java.util.Collections;
import jelatyna.domain.dto.SpeakerDto;
import jelatyna.domain.dto.converters.SpeakerConverter;
import jelatyna.repositories.SpeakerRepository;

@Controller
public class SpeakerApi {

    @Autowired
    private SpeakerRepository speakerRepository;

    @Autowired
    private SpeakerConverter converter;

    @RequestMapping(value = "/speakers", method = RequestMethod.GET)
    @ResponseBody
    public List<SpeakerDto> getSpeakers() {
        List<SpeakerDto> speakers = speakerRepository.allAccepted()
                .stream()
                .map((s) -> converter.map(s))
                .collect(Collectors.toList());
        Collections.shuffle(speakers);
        return speakers;

    }


    @RequestMapping(value="/speaker/{id}", method = RequestMethod.GET)
    @ResponseBody
    public SpeakerDto getSpeaker(@PathVariable("id")int id) {
        return converter.map(speakerRepository.findOne(id));
    }


}
