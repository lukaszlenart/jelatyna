package jelatyna.security;

import jelatyna.domain.Participant;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SecurityUtilsShould {

    @Test
    public void returnCorrectUserNameIfWasSet() throws Exception {
        setUser();

        assertEquals("Michal", SecurityUtils.getUsername());

    }

    private void setUser() {
        Participant participant = new Participant("Michal", "Michalowski");
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(participant, "pass");
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
    }

    @Test
    public void returnEmptyStringInCaseOfAnonymous() throws Exception {
        assertEquals("", SecurityUtils.getUsername());
    }

    @Test
    public void returnThatUserIsAnonymousBecauseNoneWasSetInContext() throws Exception {
         assertTrue(SecurityUtils.isAnonymous());
    }
    @Test
    public void returnThatUserIsNotAnonymousBecauseWasSetInContext() throws Exception {
        setUser();

        assertFalse(SecurityUtils.isAnonymous());
    }

    @After
    public void  cleanUp(){
        SecurityContextHolder.getContext().setAuthentication(null);
    }
}
