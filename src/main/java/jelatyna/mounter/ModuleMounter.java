package jelatyna.mounter;

import jelatyna.Confitura;
import org.apache.wicket.Page;

public abstract class ModuleMounter {

    private Confitura confitura;
    private String prefix;

    public ModuleMounter(String prefix, Confitura confitura) {
        this.prefix = prefix;
        this.confitura = confitura;
    }

    protected void mountPage(String url, Class<? extends Page> pageClass) {
        confitura.mountPage(prefix + url, pageClass);
    }

    public abstract void mount();

}
