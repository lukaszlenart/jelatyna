package jelatyna.services;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class VotingAspect {
    @Value("${call4papers.voting}")
    private boolean isActive;

    @Before("execution(* jelatyna.services.VotingService.*(..))")
    private void isVotingActive() {
        if(isActive  == false) {
            throw new RuntimeException("V4P process is not active");
        }
    }

}
