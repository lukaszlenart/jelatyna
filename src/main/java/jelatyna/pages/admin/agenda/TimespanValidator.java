package jelatyna.pages.admin.agenda;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import org.apache.wicket.validation.INullAcceptingValidator;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

import java.io.Serializable;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static org.springframework.util.Assert.*;

@SuppressWarnings("serial")
class TimespanValidator implements IValidator<String>, INullAcceptingValidator<String>, Serializable {
    @Override
    public void validate(IValidatable<String> toBeValidated) {
        if (!isTimespan(toBeValidated)) {
            toBeValidated.error(new ValidationError().setMessage("Akceptowalny format HH:mm-HH:mm"));
        }
    }

    private boolean isTimespan(IValidatable<String> toBeValidated) {
        try {
            hasText(toBeValidated.getValue());
            String cleanInput = clearInput(toBeValidated);
            List<String> fromAndTo = splitToFromAndTo(cleanInput);
            for (String timeAsString : fromAndTo) {
                List<String> hourAndMinutes = splitHoursAndMinutes(timeAsString);
                verifyHour(hourAndMinutes);
                if (minutesArePresent(hourAndMinutes)) {
                    verifyMinutes(hourAndMinutes);
                }
            }
        } catch (RuntimeException e) {
            return false;
        }
        return true;
    }

    private String clearInput(IValidatable<String> toBeValidated) {
        CharMatcher charMatcher = CharMatcher.DIGIT.or(CharMatcher.is(':').or(CharMatcher.is('-')));
        String cleanInput = charMatcher.retainFrom(toBeValidated.getValue().toString());
        if (cleanInput.isEmpty()) {
            throw new RuntimeException("Clean input is empty");
        }
        return cleanInput;
    }

    private List<String> splitToFromAndTo(String cleanInput) {
        Iterable<String> fromToAsStrings = Splitter.on("-").split(cleanInput);
        List<String> fromAndTo = newArrayList(fromToAsStrings.iterator());
        if (fromAndTo.size() != 2) {
            throw new RuntimeException("Could not split into two times");
        }
        return fromAndTo;
    }

    private List<String> splitHoursAndMinutes(String timeAsString) {
        Iterable<String> timeAsStringsIterable = Splitter.on(":").split(timeAsString);
        List<String> hourAndMinutes = newArrayList(timeAsStringsIterable.iterator());
        if (hourAndMinutes.size() < 1 || hourAndMinutes.size() > 2) {
            throw new RuntimeException("Could not split into hours and minutes (or just hours)");
        }
        return hourAndMinutes;
    }

    private void verifyHour(List<String> hourAndMinutes) {
        String hourAsString = hourAndMinutes.get(0);
        Integer hour = Integer.parseInt(hourAsString);
        if (hour == null || hour < 0 || hour > 24) {
            throw new RuntimeException("Hour is not in range 0-24");
        }
    }

    private boolean minutesArePresent(List<String> hourAndMinutes) {
        return hourAndMinutes.size() == 2;
    }

    private void verifyMinutes(List<String> hourAndMinutes) {
        String minutesAsString = hourAndMinutes.get(1);
        Integer minutes = Integer.parseInt(minutesAsString);
        if (minutes == null || minutes < 0 || minutes > 60) {
            throw new RuntimeException("Minutes not in range 0-60");
        }
    }
}
