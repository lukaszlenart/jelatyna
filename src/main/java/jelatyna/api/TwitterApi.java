package jelatyna.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jelatyna.services.Twitter;

@Controller
public class TwitterApi {
    @Autowired
    private Twitter twitter;

    @RequestMapping(value = "twitter", method = RequestMethod.GET)
    @ResponseBody
    public String getTweets() throws Exception {
        return twitter.doGetTweetsFor("confiturapl");
    }

}
