package jelatyna;

import jelatyna.domain.Admin;
import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.domain.Sponsor;

import java.io.File;
import java.util.List;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.*;

public class TestUtils {
    public static int ID = 0;

    public static Participant participant(int id) {
        return new Participant().id(id);
    }

    public static Participant participant(String firstName, String lastName) {
        return new Participant(firstName, lastName);
    }

    public static List<RegistrationStatus> anyStatuses() {
        return anyListOf(RegistrationStatus.class);
    }

    public static Admin admin() {
        return new Admin().firstName("Jan").lastName("Kowalski");
    }

    public static Sponsor sponsor() {
        return sponsor(nextId());
    }

    public static Sponsor sponsor(int id) {
        String name = "sponsor_" + id;
        return new Sponsor(name, "www." + name + ".com");
    }

    private static int nextId() {
        return ID += 1;
    }

    public static File folder(String name) {
        File file = withName(name);
        when(file.isDirectory()).thenReturn(true);
        when(file.isFile()).thenReturn(false);
        return file;
    }

    public static File file(String name) {
        File file = withName(name);
        when(file.isDirectory()).thenReturn(false);
        when(file.isFile()).thenReturn(true);
        return file;
    }

    public static File withName(String name) {
        File file = mock(File.class);
        when(file.getName()).thenReturn(name);
        return file;
    }

    public static <T extends Object> T[] $(T... objects) {
        return objects;
    }

}
