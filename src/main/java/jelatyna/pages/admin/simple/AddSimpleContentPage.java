package jelatyna.pages.admin.simple;

import jelatyna.components.RedirectLink;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.SimpleContent;
import jelatyna.pages.admin.AbstractAddPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.repositories.SimpleContentRepository;
import jelatyna.utils.Models;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.PageParametersBuilder.*;

@SuppressWarnings("serial")
public class AddSimpleContentPage extends AbstractAddPage<SimpleContent> {
    @SpringBean
    private SimpleContentRepository repository;

    public AddSimpleContentPage(PageParameters params) {
        super(params);
    }

    @Override
    protected void initPage(final SimpleContent simpleContent) {
        Form<SimpleContent> form = new Form<SimpleContent>("form") {
            @Override
            protected void onSubmit() {
                repository.save(simpleContent);
                getSession().info("Strona statyczna została zapisana");
                setResponsePage(AddSimpleContentPage.class, paramsFor("id", simpleContent.getId()));
            }
        };
        form.add(new RedirectLink("simpleContentListLink", ListSimpleContentPage.class));
        form.add(new ConfituraFeedbackPanel("feedback"));
        form.add(textField("title", new PropertyModel<String>(simpleContent, "title")));
        form.add(richEditor("content", Models.propertyModel(simpleContent, "content")));
        form.add(cancelLink(ListSimpleContentPage.class));
        form.add(new SimpleContentPreviewLink("previewLink", simpleContent));
        add(form);
    }

    @Override
    protected SimpleContent createNew() {
        return new SimpleContent();
    }

    @Override
    protected SimpleContent findById(int id) {
        return repository.findOne(id);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CONFERENCE_SITE;
    }


    @Override
    public String getPageTitle() {
        return "Dodaj/edytuj stronę statyczną";
    }

}
