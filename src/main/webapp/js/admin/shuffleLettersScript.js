//popup based on http://robertmatuszewski.pl/blog/programowanie/okienko-popup-oparte-o-css-i-jquery/
$(document).ready(function () {
    $('#luckyGuyContainer').hide();

//When you click on a link with class of poplight and the href starts with a #    
    $('a.poplight[href^=#]').click(function () {
        var popupContent = $('#popup_name');
        //$('#container1').css('display' , 'none');
        //Fade in the Popup and add close button
        popupContent.fadeIn();
        var container2 = $("#lotteryHeader");
        container2.shuffleLetters();
        //Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
        var popMargTop = (popupContent.height() + 80) / 2;
        var popMargLeft = (popupContent.width() + 80) / 2;

        //Apply Margin to Popup
        popupContent.css({
            'margin-top': -popMargTop,
            'margin-left': -popMargLeft
        });

        //Fade in Background
        $('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
        $('#fade').css({'filter': 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies

        //Close Popups and Fade Layer
        $('a.close, #fade').live('click', function () { //When clicking on the close or fade layer...
            $('#fade , .popup_block').fadeOut(function () {
                $('#fade').remove();  //fade them both out
            });

            return false;
        });
    });
});

function shuffle(name) {
    $(name).shuffleLetters();
    $(name).show();

}
