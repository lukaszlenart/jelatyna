package jelatyna.domain.dto;

public class TimeSlotDto implements AbstractDto{
    private int id;
    private String start;
    private String end;

    public TimeSlotDto() {
    }

    public TimeSlotDto(int id, String start, String end) {
        this.id = id;
        this.start = start;
        this.end = end;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public TimeSlotDto id(final int id) {
        this.id = id;
        return this;
    }

    public TimeSlotDto start(final String star) {
        this.start = star;
        return this;
    }

    public TimeSlotDto end(final String end) {
        this.end = end;
        return this;
    }


    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimeSlotDto that = (TimeSlotDto) o;

        if (id != that.id) return false;
        if (end != null ? !end.equals(that.end) : that.end != null) return false;
        if (start != null ? !start.equals(that.start) : that.start != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TimeSlotDto{" +
                "id=" + id +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
