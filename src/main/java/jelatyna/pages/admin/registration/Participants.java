package jelatyna.pages.admin.registration;

import jelatyna.domain.Participant;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Participants {

    private final List<Participant> participants;

    public Participants(List<Participant> participants) {
        this.participants = participants;
    }

    public Participants(Stream<Participant> stream) {
        this.participants = stream.collect(Collectors.toList());
    }

    public Participants limitToWithMailing(boolean limit) {
        if (limit) {
            return new Participants(
                    participants
                            .stream()
                            .filter(p -> p.isMailing())
            );
        }
        return this;
    }

    public Participants limitToParticipated(boolean limit) {
        if (limit) {
            return new Participants(
                    participants
                            .stream()
                            .filter(p -> p.isParticipated())
            );
        }
        return this;
    }

    public List<Participant> asList() {
        return participants;
    }
}
