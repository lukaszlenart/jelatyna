package jelatyna.domain.dto;

import jelatyna.api.DateApi;
import org.apache.commons.lang.StringUtils;

public class NewsDto {
    private String title;
    private String author;
    private DateApi date;
    private String shortText;
    private String description;
    private boolean hasMore;

    public NewsDto title(String title) {
        this.title = title;
        return this;
    }

    public NewsDto author(String author) {
        this.author = author;
        return this;
    }

    public NewsDto date(DateApi date) {
        this.date = date;
        return this;
    }

    public NewsDto shortText(String shortText) {
        this.shortText = shortText;
        return this;
    }

    public NewsDto text(String description) {
        this.description = description;
        this.hasMore = StringUtils.isNotBlank(description);
        return this;
    }


    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public DateApi getDate() {
        return date;
    }

    public String getShortText() {
        return shortText;
    }

    public String getDescription() {
        return description;
    }

    public boolean isHasMore() {
        return hasMore;
    }
}
