package jelatyna.pages.confitura.registration.process;

import static jelatyna.domain.RegistrationStatus.*;

import org.apache.wicket.request.mapper.parameter.PageParameters;

@SuppressWarnings("serial")
public class CancelRegistrationPage extends ChangeRegistrationStatePage {

    public CancelRegistrationPage(PageParameters params) {
        super(params, CANCELED);
        setPageTitlePostfix("Anulowanie rejestracji");
    }

    @Override
    String getMessage() {
        return "Twoja rejestracja została anulowana. Dziękujęmy za informację!";
    }

}
