package jelatyna.domain;

import com.google.common.base.Objects;
import jelatyna.utils.TextInUrlUtils;

import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class MenuLinkItem extends AbstractEntity<MenuLinkItem> {

    private String title;
    private Integer itemId;

    public MenuLinkItem() {
    }

    public MenuLinkItem(Integer itemId, String title) {
        this.itemId = itemId;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getSimpleTitle() {
        return TextInUrlUtils.prettify(title);
    }

    public Integer getItemId() {
        return itemId;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), title, itemId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MenuLinkItem other = (MenuLinkItem) obj;
        return Objects.equal(getId(), other.getId())
                && Objects.equal(title, other.title)
                && Objects.equal(itemId, other.itemId);
    }

}
