package jelatyna.api;

import static org.fest.assertions.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import jelatyna.domain.Agenda;
import jelatyna.domain.Presentation;
import jelatyna.domain.dto.AgendaDto;
import jelatyna.domain.dto.AgendaSlotDto;
import jelatyna.domain.dto.PresentationDto;
import jelatyna.domain.dto.TimeSlotDto;
import jelatyna.repositories.AgendaRepository;
import jelatyna.services.PresentationService;
import jelatyna.services.PresentationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public class AgendaApiShould {

    @Mock
    AgendaRepository agendaRepository = mock(AgendaRepository.class);

    @Mock
    PresentationService service = mock(PresentationServiceImpl.class);

    @InjectMocks
    private AgendaApi api = new AgendaApi();

    private Agenda agenda = new Agenda();

    private Presentation presentation = new Presentation().id(20);

    @Before
    public void setUp() {

        when(agendaRepository.getAgenda()).thenReturn(agenda);
        when(service.findBy(presentation.getId())).thenReturn(presentation);

    }

    @Test
    public void returnProperRoomsWith2Rooms2Slots() {
        //given
        rows(3);
        columns(3);
        setRow("godzina", "room 1", "room 2");
        setRow("10:00-11:00", "title 1", "title 2");
        setRow("11:00-12:00", "title 3", "title 4");

        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getRooms())
                .containsExactly("room 1", "room 2");

    }
    @Test
    public void returnProperRoomsWith2Rooms1Slot() {
        //given
        rows(2);
        columns(3);
        setRow("godzina", "room 1", "room 2");
        setRow("10:00-11:00", "title 1", "title 2");

        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getRooms())
                .containsExactly("room 1", "room 2");

    }

    @Test
    public void returnProperRoomsWith1Room1Slot() {
        //given
        rows(2);
        columns(2);
        setRow("godzina", "room 1");
        setRow("10:00-11:00", "title 1");
        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getRooms())
                .containsExactly("room 1");

    }


    @Test
    public void returnProperTimeSlotsWith2Rooms2Slots() {
        //given
        rows(3);
        columns(3);
        setRow("godzina", "room 1", "room 2");
        setRow("10:00-11:00", "title 1", "title 2");
        setRow("11-12", "title 3", "title 4");

        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getSlots())
                .containsExactly(
                        new TimeSlotDto(1, "10:00", "11:00"),
                        new TimeSlotDto(2, "11:00", "12:00")
                );

    }

    @Test
    public void returnProperTimeSlotsWith2Rooms1Slot() {
        //given
        rows(2);
        columns(3);
        setRow("godzina", "room 1", "room 2");
        setRow("10:00-11:00", "title 1", "title 2");

        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getSlots())
                .containsExactly(
                        new TimeSlotDto(1, "10:00", "11:00")
                );

    }

    @Test
    public void returnProperTimeSlotsWith1Room1Slot() {
        //given
        rows(2);
        columns(2);
        setRow("godzina", "room 1");
        setRow("10:00-11:00", "title 1");

        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getSlots())
                .containsExactly(
                        new TimeSlotDto(1, "10:00", "11:00")
                );

    }


    @Test
    public void returnProperScheduleWith2Rooms2Slots() {
        //given
        rows(3);
        columns(3);
        setRow("godzina", "room 1", "room 2");
        setRow("10:00-11:00", "title 1", "title 2");
        setRow("11:00-12:00", "title 3", "title 4");


        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getSchedule()).hasSize(2);
        assertThat(agenda.getSchedule().get(0).getSlotId()).isEqualTo(1);
        assertThat(agenda.getSchedule().get(0).getPresentations())
                .contains(slot("room 1", "title 1"))
                .contains(slot("room 2", "title 2"));


        assertThat(agenda.getSchedule().get(1).getSlotId()).isEqualTo(2);
        assertThat(agenda.getSchedule().get(1).getPresentations())
                .contains(slot("room 1", "title 3"))
                .contains(slot("room 2", "title 4"));


    }

    @Test
    public void returnProperScheduleWith2Rooms1Slot() {
        //given
        rows(2);
        columns(3);
        setRow("godzina", "room 1", "room 2");
        setRow("10:00-11:00", "title 1", "title 2");


        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getSchedule()).hasSize(1);
        assertThat(agenda.getSchedule().get(0).getSlotId()).isEqualTo(1);
        assertThat(agenda.getSchedule().get(0).getPresentations())
                .contains(slot("room 1", "title 1"))
                .contains(slot("room 2", "title 2"));


    }

    @Test
    public void returnProperScheduleWith1Room1Slot() {
        //given
        rows(2);
        columns(2);
        setRow("godzina", "room 1");
        setRow("10:00-11:00", "title 1");

        presentation.title("other title");

        agenda.setCell(1,1,presentation);


        //when
        AgendaDto agenda = api.agenda();

        //then
        assertThat(agenda.getSchedule()).hasSize(1);
        assertThat(agenda.getSchedule().get(0).getSlotId()).isEqualTo(1);
        assertThat(agenda.getSchedule().get(0).getPresentations())
                .contains(slot("room 1", presentation.getTitle()));



    }

    private AgendaSlotDto slot(String room, String title) {
        AgendaSlotDto slot = new AgendaSlotDto();
        slot.setRoom(room);
        slot.setItem(new PresentationDto());
        return slot;

    }


    private void rows(int rows) {
        agenda.setNumberOfTimeSlots(rows);
    }

    private void columns(int columns) {
        agenda.setNumberOfRooms(columns);
    }

    private int rowIdx = 0;
    private void setRow(String... cellContent) {
        for (int columnIdx = 0; columnIdx < cellContent.length; columnIdx++) {
            agenda.setCell(columnIdx, rowIdx, cellContent[columnIdx]);
        }
        rowIdx++;
    }

}
