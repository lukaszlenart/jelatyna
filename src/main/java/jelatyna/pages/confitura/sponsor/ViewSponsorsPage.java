package jelatyna.pages.confitura.sponsor;

import jelatyna.components.StaticImage;
import jelatyna.domain.Sponsor;
import jelatyna.domain.SponsorType;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.services.FileService;
import jelatyna.services.SponsorService;
import jelatyna.utils.Components;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ViewSponsorsPage extends BaseWebPage {
    @SpringBean
    private SponsorService service;
    @SpringBean
    private FileService fileService;

    public ViewSponsorsPage(PageParameters params) {
        List<SponsorType> types = service.findTypesWithSponsors();
        add(createTypesList(types));
        add(createSponsorTypeList(types));
        setPageTitlePostfix("Partnerzy konferencji");

    }

    private ListView<SponsorType> createTypesList(List<SponsorType> types) {
        return new ListView<SponsorType>("types", types) {
            @Override
            protected void populateItem(ListItem<SponsorType> item) {
                activateFirstType(item);
                SponsorType type = item.getModelObject();
                ExternalLink link = new ExternalLink("link", "#" + createId(type));
                link.add(Components.label("label", type.getName()));
                item.add(link);
            }

        };
    }

    private ListView<SponsorType> createSponsorTypeList(List<SponsorType> types) {
        return new ListView<SponsorType>("sponsors_by_types", types) {

            @Override
            protected void populateItem(ListItem<SponsorType> item) {
                activateFirstType(item);
                SponsorType type = item.getModelObject();
                item.add(new AttributeModifier("id", createId(type)));
                List<Sponsor> sponsors = service.findSponsorsBy(type);
                item.add(createSponsorsListFor(sponsors)).setVisible(sponsors.size() > 0);
            }

        };
    }

    private ListView<Sponsor> createSponsorsListFor(List<Sponsor> sponsors) {
        return new ListView<Sponsor>("sponsor", sponsors) {

            @Override
            protected void populateItem(ListItem<Sponsor> item) {
                Sponsor sponsor = item.getModelObject();
                item.add(createLogo(sponsor));
                item.add(richLabel("name", sponsor.getName()));
                item.add(richLabel("desc", sponsor.getDescription()));
                addImageForFacebook(fileService.getUrlTo(sponsor));
            }

            private ExternalLink createLogo(Sponsor sponsor) {
                ExternalLink link = new ExternalLink("link", sponsor.getWebPage());
                link.add(new StaticImage("logo", fileService.getUrlTo(sponsor)));
                return link;
            }

        };
    }

    private void activateFirstType(ListItem<SponsorType> item) {
        if (item.getIndex() == 0) {
            item.add(new AttributeAppender("class", " active"));
        }
    }

    private String createId(SponsorType type) {
        return "type_" + type.getId();
    }

}
