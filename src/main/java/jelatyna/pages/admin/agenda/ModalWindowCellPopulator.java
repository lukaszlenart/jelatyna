package jelatyna.pages.admin.agenda;

import java.util.List;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;

import jelatyna.domain.Agenda;
import jelatyna.domain.AgendaCell;

@SuppressWarnings("serial")
class ModalWindowCellPopulator extends ColumnWithPossibleHeaderPopulator {
    private Page pageToBeRefreshed;
    private PageInModalWindowCreator pageInModalWindowCreator;

    protected ModalWindowCellPopulator(int colNum, IModel<Agenda> agendaInSessionModel, Page pageToBeRefreshed,
                                       PageInModalWindowCreator pageInModalWindowCreator) {
        super(colNum, agendaInSessionModel);
        this.pageToBeRefreshed = pageToBeRefreshed;
        this.pageInModalWindowCreator = pageInModalWindowCreator;
    }

    @Override
    protected void populateCellItem(Item<ICellPopulator<List<AgendaCell>>> cellItem, String componentId,
                                    IModel<List<AgendaCell>> rowModel) {
        final ChooseOptionsModalWindow modalWindow = createModalWindow(colNum, getRowNumber(rowModel), agendaModel);
        final Label label = createClickablePresentationLabel(rowModel, modalWindow);
        addComponentsToCellItem(cellItem, componentId, label, modalWindow);
    }

    protected ChooseOptionsModalWindow createModalWindow(int colNum, int rowNum,
                                                         IModel<Agenda> agendaModel) {
        final ChooseOptionsModalWindow modalWindow = new ChoosePresentationModalWindow(agendaModel, colNum, rowNum);
        modalWindow.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {
            @Override
            public void onClose(AjaxRequestTarget target) {
                onModalWindowClose(target);
            }
        });
        return modalWindow;
    }

    protected void onModalWindowClose(AjaxRequestTarget target) {
        target.add(pageToBeRefreshed);
    }

    class ChoosePresentationModalWindow extends ChooseOptionsModalWindow {
        public ChoosePresentationModalWindow(IModel<Agenda> agendaModel, int colNum, int rowNum) {
            super(ColumnWithPossibleHeaderPopulator.MODAL_WINDOW_ID, agendaModel, colNum, rowNum);
        }

        @Override
        protected Page createPageInModalWindow(IModel<Agenda> agendaModel, int colNum, int rowNum,
                                               ModalWindow modalWindow) {
            return pageInModalWindowCreator.createPageInModalWindow(agendaModel, colNum, rowNum, modalWindow);
        }
    }
}
