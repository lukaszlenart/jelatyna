package jelatyna.pages.confitura.c4p;

import jelatyna.ConfituraSession;
import jelatyna.domain.Speaker;
import jelatyna.pages.PageNotFound;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.security.AuthenticationUserDetails;
import jelatyna.security.NormalizedOpenIdAttributes;
import jelatyna.services.SpeakerService;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;

import static jelatyna.components.user.UserFormPanel.*;

@SuppressWarnings("serial")
public class RegisterSpeakerPage extends BaseWebPage {
    @SpringBean
    private SpeakerService service;

    public RegisterSpeakerPage() {
        add(newUserFormPanel(service, getSpeaker(), ViewSpeakerPage.class, new UserEditedCallback<Speaker>() {
            @Override
            public void apply(Speaker user) {
                ConfituraSession.get().setUser(user);
            }
        }));
        setPageTitlePostfix("Rejestracja prelegenta");
        if (service.isCall4PapersActive() == false) {
            setResponsePage(PageNotFound.class);
        }
    }

    private Speaker getSpeaker() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication != null ? authentication.getPrincipal() : null;
        Speaker speaker = principal != null && principal instanceof AuthenticationUserDetails ? ((AuthenticationUserDetails) principal).getSpeaker() : createNewSpeaker();
        return getSession().hasSpeaker() ? getSession().getSpeaker() : speaker;
    }


    private Speaker createNewSpeaker() {
        return copyValuesFromOpenIdLogin(new Speaker());
    }

    private Speaker copyValuesFromOpenIdLogin(Speaker speaker) {
        NormalizedOpenIdAttributes attributes = getOpenIdAttributes();
        if (attributes != null) {
            speaker.
                    openIdLogin(attributes.getUserLocalIdentifier())
                    .mail(attributes.getEmailAddress())
                    .firstName(attributes.getFirstName())
                    .lastName(attributes.getLastName());
        }
        return speaker;
    }

    private NormalizedOpenIdAttributes getOpenIdAttributes() {
        return (NormalizedOpenIdAttributes) ((HttpServletRequest) getRequest()
                .getContainerRequest())
                .getSession().getAttribute("USER_OPENID_CREDENTIAL");
    }

}
