package jelatyna.pages.confitura.registration;

import static com.google.common.collect.Lists.*;
import static jelatyna.domain.RegistrationStatus.*;
import static jelatyna.pages.confitura.registration.Registration.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.googlecode.catchexception.CatchException;
import com.googlecode.catchexception.apis.CatchExceptionBdd;

import jelatyna.domain.Participant;
import jelatyna.domain.Presentation;
import jelatyna.domain.RegistrationConfiguration;
import jelatyna.pages.admin.registration.ParticipantMailSender;
import jelatyna.repositories.ParticipantRepository;
import jelatyna.repositories.PresentationRepository;
import jelatyna.repositories.RegistrationConfigurationRepository;
import jelatyna.services.RandomGenerator;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationShould {
    private Participant participant = new Participant().id(1);

    @Captor
    private ArgumentCaptor<Participant> captor;

    @Mock
    private RandomGenerator randomGenerator;

    @Mock
    private ParticipantMailSender mailSender;

    @Mock
    private ParticipantRepository repository;

    @Mock
    private PresentationRepository presentationRepository;

    @Mock
    private RegistrationConfigurationRepository configurationRepository;

    @InjectMocks
    private Registration registration = new Registration();

    @Test
    public void registerParticipantAndSendMail() {
        withActiveRegistration();
        String token = "123";
        when(randomGenerator.getRandomId()).thenReturn(token);

        registration.register(participant);

        InOrder inOrder = inOrder(repository, mailSender);
        inOrder.verify(repository).save(captor.capture());
        inOrder.verify(mailSender).sendMessage(participant);
        assertThat(captor.getValue().getToken()).isEqualTo(token);
    }



    @Test
    public void throwExceptionIfMailAlreadyExists() {
        when(repository.findByMail(anyString())).thenReturn(new Participant());

        CatchExceptionBdd.when(registration).register(participant);

        CatchExceptionBdd.then(CatchException.caughtException())
                .hasMessage(MAIL_EXISTS_ERROR);
        verifyZeroInteractions(mailSender, randomGenerator);
    }

    @Test
    public void registerParticipantIfHas7Presentations() {
        participant = spy(participant);
        when(participant.countPresentations()).thenReturn(7);
        when(randomGenerator.getRandomId()).thenReturn("123");
        withActiveRegistration();

        registration.register(participant);

        verify(repository).save(anyParticipant());
    }

    @Test
    public void throwExceptionIfParticipantHasMoreThen7Presentations() {
        participant = mock(Participant.class);
        when(participant.countPresentations()).thenReturn(8);

        CatchExceptionBdd.when(registration).register(participant);

        CatchExceptionBdd.then(CatchException.caughtException())
                .hasMessage(PRESENTATION_LIMIT_ERROR);
        verifyZeroInteractions(mailSender, randomGenerator);
    }

    @Test
    public void fetchAllAcceptedPresentations() {
        List<Presentation> acceptedPresentations = newArrayList(new Presentation());
        when(presentationRepository.findAllAccepted()).thenReturn(acceptedPresentations);

        List<Presentation> presentations = registration.allPresentations();

        assertThat(presentations).isSameAs(acceptedPresentations);
    }

    @Test
    public void addWildcardAtTheEndOnFetchingCities() {
        List<String> cities = newArrayList("city 1");
        when(repository.fetchCitiesStartingWith(anyString())).thenReturn(cities);

        List<String> fetchedCities = registration.fetchCitiesStartingWith("c");

        verify(repository).fetchCitiesStartingWith("c%");
        assertThat(fetchedCities).isSameAs(cities);
    }

    @Test
    public void changeRegistrationStatusForParticipant() {
        when(repository.findByToken(anyString())).thenReturn(participant);

        registration.changeStatusFor("123", CONFIRMED);

        verify(repository).save(captor.capture());
        Participant savedParticipant = captor.getValue();
        assertThat(savedParticipant.getId()).isEqualTo(participant.getId());
        assertThat(savedParticipant.getStatus()).isEqualTo(CONFIRMED);
    }

    @Test
    public void throwExceptionIfParticipantNotFoundOnConfirming() {
        when(repository.findByToken(anyString())).thenReturn(null);

        CatchExceptionBdd.when(registration).changeStatusFor("123", CONFIRMED);

        CatchExceptionBdd.then(CatchException.caughtException())
                .hasMessage(NOT_FOUND_ERROR);
        verify(repository, never()).save(anyParticipant());

    }

    private Participant anyParticipant() {
        return any(Participant.class);
    }

    private void withActiveRegistration() {
        when(configurationRepository.findAll()).thenReturn(newArrayList(new RegistrationConfiguration().active(true)));
    }
}
