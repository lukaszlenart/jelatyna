package jelatyna.pages.confitura.registration.form;

import static com.google.common.collect.Lists.*;
import static jelatyna.utils.Components.*;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.components.nogeneric.AjaxLink;
import jelatyna.domain.Participant;
import jelatyna.domain.Presentation;
import jelatyna.pages.confitura.registration.Registration;
import jelatyna.pages.confitura.registration.RegistrationInfoPage;

@SuppressWarnings("serial")
public final class RegistrationForm extends Form<Void> {

    public static final String DETAILS_FORM_PANEL = "detailsFormPanel";
    public static final String PERSONAL_AGENDA_FIELD_ID = "personalAgenda";
    public static final String TITLE_FIELD_ID = "title";
    public static final String DESCRIPTION_FIELD_ID = "description";
    public static final String SPEAKER_FIELD_ID = "speaker";
    public static final String CHOSEN_FIELD_ID = "chosen";
    public static final String DESCRIPTION_ROW_ID = "descriptionRow";
    public static final String SPEAKER_TITLE_LINK_ID = "speakerTitleLink";
    private Participant participant = new Participant();
    private List<PresentationChoice> chosenPresentations;

    @SpringBean
    private Registration registration;

    public RegistrationForm(String id) {
        super(id);

        add(new ParticipantDetailsEditFormPanel(DETAILS_FORM_PANEL, this, participant));
        addPersonalAgenda();
    }

    private void addPersonalAgenda() {
        chosenPresentations = preparePresentationChoices();
        ListView<PresentationChoice> listView = new PersonalAgenda(chosenPresentations);

        add(listView);
    }

    private List<PresentationChoice> preparePresentationChoices() {
        List<PresentationChoice> presentationChoices = newArrayList();
        for (Presentation presentation : registration.allPresentations()) {
            presentationChoices.add(new PresentationChoice(presentation));
        }
        return presentationChoices;
    }

    @Override
    protected void onSubmit() {
        registerParticipant();
    }

    private void registerParticipant() {
        try {
            doRegisterParticipant();
        } catch (Exception ex) {
            error(ex.getMessage());
        }
    }

    private void doRegisterParticipant() {
        participant.myPresentations(getChosenPresentations());
        registration.register(participant);
        setResponsePage(new RegistrationInfoPage(
                "Dziękujemy za rejestrację! Na adres e-mail podany w formularzu przesłaliśmy dalsze informacje. " +
                        "Upewnij się, że go dostałeś i koniecznie kliknij w link potwierdzający."
        ));
    }

    private List<Presentation> getChosenPresentations() {
        return chosenPresentations.stream()
                .filter(PresentationChoice::isChosen)
                .map(PresentationChoice::getPresentation)
                .collect(Collectors.toList());
    }


    private final class PersonalAgenda extends ListView<PresentationChoice> {
        private PersonalAgenda(List<? extends PresentationChoice> list) {
            super(PERSONAL_AGENDA_FIELD_ID, list);
            setOutputMarkupId(true);
        }

        @Override
        protected void populateItem(final ListItem<PresentationChoice> item) {
            item.setOutputMarkupId(true);
            final PresentationChoice presentationChoice = item.getModelObject();
            item.add(new CheckBox(CHOSEN_FIELD_ID, new PropertyModel<>(presentationChoice, "chosen")));
            WebMarkupContainer descriptionRow = createDescriptionRow(presentationChoice);
            item.add(createPresentationCell(presentationChoice, descriptionRow));
            item.add(descriptionRow);
        }

        private AjaxLink createPresentationCell(PresentationChoice presentationChoice, WebMarkupContainer descriptionRow) {
            AjaxLink speakerTitleLink = createDescriptionAjaxLink(descriptionRow);
            speakerTitleLink.add(label(SPEAKER_FIELD_ID, presentationChoice.getSpeakerName()));
            speakerTitleLink.add(label(TITLE_FIELD_ID, presentationChoice.getTitle()));
            return speakerTitleLink;
        }

        private AjaxLink createDescriptionAjaxLink(final WebMarkupContainer descriptionRow) {
            return new AjaxLink(SPEAKER_TITLE_LINK_ID) {
                @Override
                public void onClick(AjaxRequestTarget target) {
                    descriptionRow.setVisible(!descriptionRow.isVisible());
                    target.add(descriptionRow);
                }
            };
        }

        private WebMarkupContainer createDescriptionRow(PresentationChoice presentationChoice) {
            WebMarkupContainer descriptionRow = new WebMarkupContainer(DESCRIPTION_ROW_ID);
            descriptionRow.setOutputMarkupId(true);
            descriptionRow.setOutputMarkupPlaceholderTag(true);
            descriptionRow.setVisible(false);
            descriptionRow.add(richLabel(DESCRIPTION_FIELD_ID, presentationChoice.getPresentation().getDescription()));
            return descriptionRow;
        }
    }


}
