package jelatyna.domain.dto;

public class VoteDto implements AbstractDto {
    private Integer vote;
    private PresentationDto presentation;

    public VoteDto() {
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public PresentationDto getPresentation() {
        return presentation;
    }

    public void setPresentation(PresentationDto presentation) {
        this.presentation = presentation;
    }
}