package jelatyna.services;

import jelatyna.domain.RegistrationConfiguration;
import jelatyna.repositories.ParticipantRepository;
import jelatyna.repositories.RegistrationConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {
    @Autowired
    private RegistrationConfigurationRepository repository;
    @Autowired
    private ParticipantRepository participantRepository;

    public String getWidgetInfo() {
        if (hasRegistration()) {
            return getWidgetInfoOrEmpty().replace("$counter", getNumberOfParticipants());
        } else {
            return "";
        }
    }

    private String getNumberOfParticipants() {
        return participantRepository.count() + "";
    }

    private boolean hasRegistration() {
        return repository.count() != 0;
    }

    private String getWidgetInfoOrEmpty() {
        String widgetInfo = getRegistration().getWidgetInfo();
        return widgetInfo != null ? widgetInfo : "";
    }

    public RegistrationConfiguration getRegistration() {
        return repository.findAll().get(0);
    }

}
