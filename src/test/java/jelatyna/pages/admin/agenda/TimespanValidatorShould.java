package jelatyna.pages.admin.agenda;

import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidationError;
import org.junit.Test;

import static org.mockito.BDDMockito.*;

public class TimespanValidatorShould {
    private TimespanValidator timespanValidator = new TimespanValidator();

    @Test
    public void validateCorrectly() {
        is("10:20-15:30", true);
        is("10-15", true);
        is("25-15", false);
        is("10:61-11:00", false);
        is("10:00:44-8:20", false);
        is("16", false);
        is("", false);
        is(null, false);

    }

    @SuppressWarnings("unchecked")
    private void is(String value, boolean expectedResult) {
        // given
        IValidatable<String> toBeValidated = mock(IValidatable.class);
        given(toBeValidated.getValue()).willReturn(value);

        // when
        timespanValidator.validate(toBeValidated);

        // then
        if (expectedResult == true) {
            verify(toBeValidated, never()).error((IValidationError) any());
        } else {
            verify(toBeValidated).error((IValidationError) any());
        }
    }
}
