/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 * 
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 * 
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
;
(function (b) {
    var m, t, u, f, D, j, E, n, z, A, q = 0, e = {}, o = [], p = 0, d = {}, l = [], G = null, v = new Image, J = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, W = /[^\.]\.(swf)\s*$/i, K, L = 1, y = 0, s = "", r, i, h = false, B = b.extend(b("<div/>")[0], {prop: 0}), M = b.browser.msie && b.browser.version < 7 && !window.XMLHttpRequest, N = function () {
        t.hide();
        v.onerror = v.onload = null;
        G && G.abort();
        m.empty()
    }, O = function () {
        if (false === e.onError(o, q, e)) {
            t.hide();
            h = false
        } else {
            e.titleShow = false;
            e.width = "auto";
            e.height = "auto";
            m.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>');
            F()
        }
    }, I = function () {
        var a = o[q], c, g, k, C, P, w;
        N();
        e = b.extend({}, b.fn.fancybox.defaults, typeof b(a).data("fancybox") == "undefined" ? e : b(a).data("fancybox"));
        w = e.onStart(o, q, e);
        if (w === false)h = false; else {
            if (typeof w == "object")e = b.extend(e, w);
            k = e.title || (a.nodeName ? b(a).attr("title") : a.title) || "";
            if (a.nodeName && !e.orig)e.orig = b(a).children("img:first").length ? b(a).children("img:first") : b(a);
            if (k === "" && e.orig && e.titleFromAlt)k = e.orig.attr("alt");
            c = e.href || (a.nodeName ? b(a).attr("href") : a.href) || null;
            if (/^(?:javascript)/i.test(c) ||
                c == "#")c = null;
            if (e.type) {
                g = e.type;
                if (!c)c = e.content
            } else if (e.content)g = "html"; else if (c)g = c.match(J) ? "image" : c.match(W) ? "swf" : b(a).hasClass("iframe") ? "iframe" : c.indexOf("#") === 0 ? "inline" : "ajax";
            if (g) {
                if (g == "inline") {
                    a = c.substr(c.indexOf("#"));
                    g = b(a).length > 0 ? "inline" : "ajax"
                }
                e.type = g;
                e.href = c;
                e.title = k;
                if (e.autoDimensions)if (e.type == "html" || e.type == "inline" || e.type == "ajax") {
                    e.width = "auto";
                    e.height = "auto"
                } else e.autoDimensions = false;
                if (e.modal) {
                    e.overlayShow = true;
                    e.hideOnOverlayClick = false;
                    e.hideOnContentClick =
                        false;
                    e.enableEscapeButton = false;
                    e.showCloseButton = false
                }
                e.padding = parseInt(e.padding, 10);
                e.margin = parseInt(e.margin, 10);
                m.css("padding", e.padding + e.margin);
                b(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change", function () {
                    b(this).replaceWith(j.children())
                });
                switch (g) {
                    case "html":
                        m.html(e.content);
                        F();
                        break;
                    case "inline":
                        if (b(a).parent().is("#fancybox-content") === true) {
                            h = false;
                            break
                        }
                        b('<div class="fancybox-inline-tmp" />').hide().insertBefore(b(a)).bind("fancybox-cleanup", function () {
                            b(this).replaceWith(j.children())
                        }).bind("fancybox-cancel",
                            function () {
                                b(this).replaceWith(m.children())
                            });
                        b(a).appendTo(m);
                        F();
                        break;
                    case "image":
                        h = false;
                        b.fancybox.showActivity();
                        v = new Image;
                        v.onerror = function () {
                            O()
                        };
                        v.onload = function () {
                            h = true;
                            v.onerror = v.onload = null;
                            e.width = v.width;
                            e.height = v.height;
                            b("<img />").attr({id: "fancybox-img", src: v.src, alt: e.title}).appendTo(m);
                            Q()
                        };
                        v.src = c;
                        break;
                    case "swf":
                        e.scrolling = "no";
                        C = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + e.width + '" height="' + e.height + '"><param name="movie" value="' + c +
                            '"></param>';
                        P = "";
                        b.each(e.swf, function (x, H) {
                            C += '<param name="' + x + '" value="' + H + '"></param>';
                            P += " " + x + '="' + H + '"'
                        });
                        C += '<embed src="' + c + '" type="application/x-shockwave-flash" width="' + e.width + '" height="' + e.height + '"' + P + "></embed></object>";
                        m.html(C);
                        F();
                        break;
                    case "ajax":
                        h = false;
                        b.fancybox.showActivity();
                        e.ajax.win = e.ajax.success;
                        G = b.ajax(b.extend({}, e.ajax, {url: c, data: e.ajax.data || {}, error: function (x) {
                            x.status > 0 && O()
                        }, success: function (x, H, R) {
                            if ((typeof R == "object" ? R : G).status == 200) {
                                if (typeof e.ajax.win ==
                                    "function") {
                                    w = e.ajax.win(c, x, H, R);
                                    if (w === false) {
                                        t.hide();
                                        return
                                    } else if (typeof w == "string" || typeof w == "object")x = w
                                }
                                m.html(x);
                                F()
                            }
                        }}));
                        break;
                    case "iframe":
                        Q()
                }
            } else O()
        }
    }, F = function () {
        var a = e.width, c = e.height;
        a = a.toString().indexOf("%") > -1 ? parseInt((b(window).width() - e.margin * 2) * parseFloat(a) / 100, 10) + "px" : a == "auto" ? "auto" : a + "px";
        c = c.toString().indexOf("%") > -1 ? parseInt((b(window).height() - e.margin * 2) * parseFloat(c) / 100, 10) + "px" : c == "auto" ? "auto" : c + "px";
        m.wrapInner('<div style="width:' + a + ";height:" + c +
            ";overflow: " + (e.scrolling == "auto" ? "auto" : e.scrolling == "yes" ? "scroll" : "hidden") + ';position:relative;"></div>');
        e.width = m.width();
        e.height = m.height();
        Q()
    }, Q = function () {
        var a, c;
        t.hide();
        if (f.is(":visible") && false === d.onCleanup(l, p, d)) {
            b.event.trigger("fancybox-cancel");
            h = false
        } else {
            h = true;
            b(j.add(u)).unbind();
            b(window).unbind("resize.fb scroll.fb");
            b(document).unbind("keydown.fb");
            f.is(":visible") && d.titlePosition !== "outside" && f.css("height", f.height());
            l = o;
            p = q;
            d = e;
            if (d.overlayShow) {
                u.css({"background-color": d.overlayColor,
                    opacity: d.overlayOpacity, cursor: d.hideOnOverlayClick ? "pointer" : "auto", height: b(document).height()});
                if (!u.is(":visible")) {
                    M && b("select:not(#fancybox-tmp select)").filter(function () {
                        return this.style.visibility !== "hidden"
                    }).css({visibility: "hidden"}).one("fancybox-cleanup", function () {
                        this.style.visibility = "inherit"
                    });
                    u.show()
                }
            } else u.hide();
            i = X();
            s = d.title || "";
            y = 0;
            n.empty().removeAttr("style").removeClass();
            if (d.titleShow !== false) {
                if (b.isFunction(d.titleFormat))a = d.titleFormat(s, l, p, d); else a = s && s.length ?
                        d.titlePosition == "float" ? '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + s + '</td><td id="fancybox-title-float-right"></td></tr></table>' : '<div id="fancybox-title-' + d.titlePosition + '">' + s + "</div>" : false;
                s = a;
                if (!(!s || s === "")) {
                    n.addClass("fancybox-title-" + d.titlePosition).html(s).appendTo("body").show();
                    switch (d.titlePosition) {
                        case "inside":
                            n.css({width: i.width - d.padding * 2, marginLeft: d.padding, marginRight: d.padding});
                            y = n.outerHeight(true);
                            n.appendTo(D);
                            i.height += y;
                            break;
                        case "over":
                            n.css({marginLeft: d.padding, width: i.width - d.padding * 2, bottom: d.padding}).appendTo(D);
                            break;
                        case "float":
                            n.css("left", parseInt((n.width() - i.width - 40) / 2, 10) * -1).appendTo(f);
                            break;
                        default:
                            n.css({width: i.width - d.padding * 2, paddingLeft: d.padding, paddingRight: d.padding}).appendTo(f)
                    }
                }
            }
            n.hide();
            if (f.is(":visible")) {
                b(E.add(z).add(A)).hide();
                a = f.position();
                r = {top: a.top, left: a.left, width: f.width(), height: f.height()};
                c = r.width == i.width && r.height ==
                    i.height;
                j.fadeTo(d.changeFade, 0.3, function () {
                    var g = function () {
                        j.html(m.contents()).fadeTo(d.changeFade, 1, S)
                    };
                    b.event.trigger("fancybox-change");
                    j.empty().removeAttr("filter").css({"border-width": d.padding, width: i.width - d.padding * 2, height: e.autoDimensions ? "auto" : i.height - y - d.padding * 2});
                    if (c)g(); else {
                        B.prop = 0;
                        b(B).animate({prop: 1}, {duration: d.changeSpeed, easing: d.easingChange, step: T, complete: g})
                    }
                })
            } else {
                f.removeAttr("style");
                j.css("border-width", d.padding);
                if (d.transitionIn == "elastic") {
                    r = V();
                    j.html(m.contents());
                    f.show();
                    if (d.opacity)i.opacity = 0;
                    B.prop = 0;
                    b(B).animate({prop: 1}, {duration: d.speedIn, easing: d.easingIn, step: T, complete: S})
                } else {
                    d.titlePosition == "inside" && y > 0 && n.show();
                    j.css({width: i.width - d.padding * 2, height: e.autoDimensions ? "auto" : i.height - y - d.padding * 2}).html(m.contents());
                    f.css(i).fadeIn(d.transitionIn == "none" ? 0 : d.speedIn, S)
                }
            }
        }
    }, Y = function () {
        if (d.enableEscapeButton || d.enableKeyboardNav)b(document).bind("keydown.fb", function (a) {
            if (a.keyCode == 27 && d.enableEscapeButton) {
                a.preventDefault();
                b.fancybox.close()
            } else if ((a.keyCode ==
                37 || a.keyCode == 39) && d.enableKeyboardNav && a.target.tagName !== "INPUT" && a.target.tagName !== "TEXTAREA" && a.target.tagName !== "SELECT") {
                a.preventDefault();
                b.fancybox[a.keyCode == 37 ? "prev" : "next"]()
            }
        });
        if (d.showNavArrows) {
            if (d.cyclic && l.length > 1 || p !== 0)z.show();
            if (d.cyclic && l.length > 1 || p != l.length - 1)A.show()
        } else {
            z.hide();
            A.hide()
        }
    }, S = function () {
        if (!b.support.opacity) {
            j.get(0).style.removeAttribute("filter");
            f.get(0).style.removeAttribute("filter")
        }
        e.autoDimensions && j.css("height", "auto");
        f.css("height", "auto");
        s && s.length && n.show();
        d.showCloseButton && E.show();
        Y();
        d.hideOnContentClick && j.bind("click", b.fancybox.close);
        d.hideOnOverlayClick && u.bind("click", b.fancybox.close);
        b(window).bind("resize.fb", b.fancybox.resize);
        d.centerOnScroll && b(window).bind("scroll.fb", b.fancybox.center);
        if (d.type == "iframe")b('<iframe id="fancybox-frame" name="fancybox-frame' + (new Date).getTime() + '" frameborder="0" hspace="0" ' + (b.browser.msie ? 'allowtransparency="true""' : "") + ' scrolling="' + e.scrolling + '" src="' + d.href + '"></iframe>').appendTo(j);
        f.show();
        h = false;
        b.fancybox.center();
        d.onComplete(l, p, d);
        var a, c;
        if (l.length - 1 > p) {
            a = l[p + 1].href;
            if (typeof a !== "undefined" && a.match(J)) {
                c = new Image;
                c.src = a
            }
        }
        if (p > 0) {
            a = l[p - 1].href;
            if (typeof a !== "undefined" && a.match(J)) {
                c = new Image;
                c.src = a
            }
        }
    }, T = function (a) {
        var c = {width: parseInt(r.width + (i.width - r.width) * a, 10), height: parseInt(r.height + (i.height - r.height) * a, 10), top: parseInt(r.top + (i.top - r.top) * a, 10), left: parseInt(r.left + (i.left - r.left) * a, 10)};
        if (typeof i.opacity !== "undefined")c.opacity = a < 0.5 ? 0.5 : a;
        f.css(c);
        j.css({width: c.width - d.padding * 2, height: c.height - y * a - d.padding * 2})
    }, U = function () {
        return[b(window).width() - d.margin * 2, b(window).height() - d.margin * 2, b(document).scrollLeft() + d.margin, b(document).scrollTop() + d.margin]
    }, X = function () {
        var a = U(), c = {}, g = d.autoScale, k = d.padding * 2;
        c.width = d.width.toString().indexOf("%") > -1 ? parseInt(a[0] * parseFloat(d.width) / 100, 10) : d.width + k;
        c.height = d.height.toString().indexOf("%") > -1 ? parseInt(a[1] * parseFloat(d.height) / 100, 10) : d.height + k;
        if (g && (c.width > a[0] || c.height > a[1]))if (e.type ==
            "image" || e.type == "swf") {
            g = d.width / d.height;
            if (c.width > a[0]) {
                c.width = a[0];
                c.height = parseInt((c.width - k) / g + k, 10)
            }
            if (c.height > a[1]) {
                c.height = a[1];
                c.width = parseInt((c.height - k) * g + k, 10)
            }
        } else {
            c.width = Math.min(c.width, a[0]);
            c.height = Math.min(c.height, a[1])
        }
        c.top = parseInt(Math.max(a[3] - 20, a[3] + (a[1] - c.height - 40) * 0.5), 10);
        c.left = parseInt(Math.max(a[2] - 20, a[2] + (a[0] - c.width - 40) * 0.5), 10);
        return c
    }, V = function () {
        var a = e.orig ? b(e.orig) : false, c = {};
        if (a && a.length) {
            c = a.offset();
            c.top += parseInt(a.css("paddingTop"),
                10) || 0;
            c.left += parseInt(a.css("paddingLeft"), 10) || 0;
            c.top += parseInt(a.css("border-top-width"), 10) || 0;
            c.left += parseInt(a.css("border-left-width"), 10) || 0;
            c.width = a.width();
            c.height = a.height();
            c = {width: c.width + d.padding * 2, height: c.height + d.padding * 2, top: c.top - d.padding - 20, left: c.left - d.padding - 20}
        } else {
            a = U();
            c = {width: d.padding * 2, height: d.padding * 2, top: parseInt(a[3] + a[1] * 0.5, 10), left: parseInt(a[2] + a[0] * 0.5, 10)}
        }
        return c
    }, Z = function () {
        if (t.is(":visible")) {
            b("div", t).css("top", L * -40 + "px");
            L = (L + 1) % 12
        } else clearInterval(K)
    };
    b.fn.fancybox = function (a) {
        if (!b(this).length)return this;
        b(this).data("fancybox", b.extend({}, a, b.metadata ? b(this).metadata() : {})).unbind("click.fb").bind("click.fb", function (c) {
            c.preventDefault();
            if (!h) {
                h = true;
                b(this).blur();
                o = [];
                q = 0;
                c = b(this).attr("rel") || "";
                if (!c || c == "" || c === "nofollow")o.push(this); else {
                    o = b("a[rel=" + c + "], area[rel=" + c + "]");
                    q = o.index(this)
                }
                I()
            }
        });
        return this
    };
    b.fancybox = function (a, c) {
        var g;
        if (!h) {
            h = true;
            g = typeof c !== "undefined" ? c : {};
            o = [];
            q = parseInt(g.index, 10) || 0;
            if (b.isArray(a)) {
                for (var k =
                    0, C = a.length; k < C; k++)if (typeof a[k] == "object")b(a[k]).data("fancybox", b.extend({}, g, a[k])); else a[k] = b({}).data("fancybox", b.extend({content: a[k]}, g));
                o = jQuery.merge(o, a)
            } else {
                if (typeof a == "object")b(a).data("fancybox", b.extend({}, g, a)); else a = b({}).data("fancybox", b.extend({content: a}, g));
                o.push(a)
            }
            if (q > o.length || q < 0)q = 0;
            I()
        }
    };
    b.fancybox.showActivity = function () {
        clearInterval(K);
        t.show();
        K = setInterval(Z, 66)
    };
    b.fancybox.hideActivity = function () {
        t.hide()
    };
    b.fancybox.next = function () {
        return b.fancybox.pos(p +
            1)
    };
    b.fancybox.prev = function () {
        return b.fancybox.pos(p - 1)
    };
    b.fancybox.pos = function (a) {
        if (!h) {
            a = parseInt(a);
            o = l;
            if (a > -1 && a < l.length) {
                q = a;
                I()
            } else if (d.cyclic && l.length > 1) {
                q = a >= l.length ? 0 : l.length - 1;
                I()
            }
        }
    };
    b.fancybox.cancel = function () {
        if (!h) {
            h = true;
            b.event.trigger("fancybox-cancel");
            N();
            e.onCancel(o, q, e);
            h = false
        }
    };
    b.fancybox.close = function () {
        function a() {
            u.fadeOut("fast");
            n.empty().hide();
            f.hide();
            b.event.trigger("fancybox-cleanup");
            j.empty();
            d.onClosed(l, p, d);
            l = e = [];
            p = q = 0;
            d = e = {};
            h = false
        }

        if (!(h || f.is(":hidden"))) {
            h =
                true;
            if (d && false === d.onCleanup(l, p, d))h = false; else {
                N();
                b(E.add(z).add(A)).hide();
                b(j.add(u)).unbind();
                b(window).unbind("resize.fb scroll.fb");
                b(document).unbind("keydown.fb");
                j.find("iframe").attr("src", M && /^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank");
                d.titlePosition !== "inside" && n.empty();
                f.stop();
                if (d.transitionOut == "elastic") {
                    r = V();
                    var c = f.position();
                    i = {top: c.top, left: c.left, width: f.width(), height: f.height()};
                    if (d.opacity)i.opacity = 1;
                    n.empty().hide();
                    B.prop = 1;
                    b(B).animate({prop: 0}, {duration: d.speedOut, easing: d.easingOut, step: T, complete: a})
                } else f.fadeOut(d.transitionOut == "none" ? 0 : d.speedOut, a)
            }
        }
    };
    b.fancybox.resize = function () {
        u.is(":visible") && u.css("height", b(document).height());
        b.fancybox.center(true)
    };
    b.fancybox.center = function (a) {
        var c, g;
        if (!h) {
            g = a === true ? 1 : 0;
            c = U();
            !g && (f.width() > c[0] || f.height() > c[1]) || f.stop().animate({top: parseInt(Math.max(c[3] - 20, c[3] + (c[1] - j.height() - 40) * 0.5 - d.padding)), left: parseInt(Math.max(c[2] - 20, c[2] + (c[0] - j.width() - 40) * 0.5 -
                d.padding))}, typeof a == "number" ? a : 200)
        }
    };
    b.fancybox.init = function () {
        if (!b("#fancybox-wrap").length) {
            b("body").append(m = b('<div id="fancybox-tmp"></div>'), t = b('<div id="fancybox-loading"><div></div></div>'), u = b('<div id="fancybox-overlay"></div>'), f = b('<div id="fancybox-wrap"></div>'));
            D = b('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(f);
            D.append(j = b('<div id="fancybox-content"></div>'), E = b('<a id="fancybox-close"></a>'), n = b('<div id="fancybox-title"></div>'), z = b('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'), A = b('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>'));
            E.click(b.fancybox.close);
            t.click(b.fancybox.cancel);
            z.click(function (a) {
                a.preventDefault();
                b.fancybox.prev()
            });
            A.click(function (a) {
                a.preventDefault();
                b.fancybox.next()
            });
            b.fn.mousewheel && f.bind("mousewheel.fb", function (a, c) {
                if (h)a.preventDefault(); else if (b(a.target).get(0).clientHeight == 0 || b(a.target).get(0).scrollHeight === b(a.target).get(0).clientHeight) {
                    a.preventDefault();
                    b.fancybox[c > 0 ? "prev" : "next"]()
                }
            });
            b.support.opacity || f.addClass("fancybox-ie");
            if (M) {
                t.addClass("fancybox-ie6");
                f.addClass("fancybox-ie6");
                b('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank") + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(D)
            }
        }
    };
    b.fn.fancybox.defaults = {padding: 10, margin: 40, opacity: false, modal: false, cyclic: false, scrolling: "auto", width: 560, height: 340, autoScale: true, autoDimensions: true, centerOnScroll: false, ajax: {}, swf: {wmode: "transparent"}, hideOnOverlayClick: true, hideOnContentClick: false, overlayShow: true, overlayOpacity: 0.7, overlayColor: "#777", titleShow: true, titlePosition: "float", titleFormat: null, titleFromAlt: false, transitionIn: "fade", transitionOut: "fade", speedIn: 300, speedOut: 300, changeSpeed: 300, changeFade: "fast", easingIn: "swing",
        easingOut: "swing", showCloseButton: true, showNavArrows: true, enableEscapeButton: true, enableKeyboardNav: true, onStart: function () {
        }, onCancel: function () {
        }, onComplete: function () {
        }, onCleanup: function () {
        }, onClosed: function () {
        }, onError: function () {
        }};
    b(document).ready(function () {
        b.fancybox.init()
    })
})(jQuery);

/* jquery.tweet.js - See http://tweet.seaofclouds.com/ or https://github.com/seaofclouds/tweet for more info
 * Copyright (c) 2008-2011 Todd Matthews & Steve Purcell
 */
(function (d) {
    "function" === typeof define && define.amd ? define(["jquery"], d) : d(jQuery)
})(function (d) {
    d.fn.tweet = function (j) {
        function e(b, a) {
            if ("string" === typeof b) {
                var f = b, d;
                for (d in a)var e = a[d], f = f.replace(RegExp("{" + d + "}", "g"), null === e ? "" : e);
                return f
            }
            return b(a)
        }

        function g(b, a) {
            return function () {
                var f = [];
                this.each(function () {
                    f.push(this.replace(b, a))
                });
                return d(f)
            }
        }

        function k(b, a) {
            return b.replace(h, function (b) {
                for (var c = /^[a-z]+:/i.test(b) ? b : "http://" + b, d = 0; d < a.length; ++d) {
                    var e = a[d];
                    if (e.url == c &&
                        e.expanded_url) {
                        c = e.expanded_url;
                        b = e.display_url;
                        break
                    }
                }
                return'<a href="' + c.replace(/</g, "&lt;").replace(/>/g, "^&gt;") + '">' + b.replace(/</g, "&lt;").replace(/>/g, "^&gt;") + "</a>"
            })
        }

        function l(b) {
            var a = parseInt(((1 < arguments.length ? arguments[1] : new Date).getTime() - b) / 1E3, 10), d = "";
            return d = 1 > a ? "teraz" : 60 > a ? a + " sekund temu" : 120 > a ? "minutę temu" : 2700 > a ? parseInt(a / 60, 10).toString() + " minut temu" : 7200 > a ? "godzinę temu" : 86400 > a ? parseInt(a / 3600, 10).toString() + " godzin temu" : 172800 >
                a ? "wczoraj" : parseInt(a / 86400, 10).toString() + " dni temu"
        }

        function m() {
            var c = "https:" == document.location.protocol ? "https:" : "http:", a = null === b.fetch ? b.count : b.fetch;
            if (b.list)return c + "//" + b.twitter_api_url + "/1/" + b.username[0] + "/lists/" + b.list + "/statuses.json?page=" + b.page + "&per_page=" + a + "&include_entities=1&callback=?";
            if (b.favorites)return c + "//" + b.twitter_api_url + "/favorites/" + b.username[0] + ".json?page=" + b.page + "&count=" + a + "&include_entities=1&callback=?";
            if (null === b.query && 1 == b.username.length)return c +
                "//" + b.twitter_api_url + "/1/statuses/user_timeline.json?screen_name=" + b.username[0] + "&count=" + a + (b.retweets ? "&include_rts=1" : "") + "&page=" + b.page + "&include_entities=1&callback=?";
            var d = b.query || "from:" + b.username.join(" OR from:");
            return c + "//" + b.twitter_search_url + "/search.json?&q=" + encodeURIComponent(d) + "&rpp=" + a + "&page=" + b.page + "&include_entities=1&callback=?"
        }

        function i(b, a) {
            return a ? "user"in b ? b.user.profile_image_url_https : i(b, !1).replace(/^http:\/\/[a-z0-9]{1,3}\.twimg\.com\//, "https://s3.amazonaws.com/twitter_production/") :
                b.profile_image_url || b.user.profile_image_url
        }

        function n(c) {
            var a = {};
            a.item = c;
            a.source = c.source;
            a.screen_name = c.from_user || c.user.screen_name;
            a.avatar_size = b.avatar_size;
            a.avatar_url = i(c, "https:" === document.location.protocol);
            a.retweet = "undefined" != typeof c.retweeted_status;
            a.tweet_time = Date.parse(c.created_at.replace(/^([a-z]{3})( [a-z]{3} \d\d?)(.*)( \d{4})$/i, "$1,$2$4$3"));
            a.join_text = "auto" == b.join_text ? c.text.match(/^(@([A-Za-z0-9-_]+)) .*/i) ? b.auto_join_text_reply : c.text.match(h) ? b.auto_join_text_url :
                c.text.match(/^((\w+ed)|just) .*/im) ? b.auto_join_text_ed : c.text.match(/^(\w*ing) .*/i) ? b.auto_join_text_ing : b.auto_join_text_default : b.join_text;
            a.tweet_id = c.id_str;
            a.twitter_base = "http://" + b.twitter_url + "/";
            a.user_url = a.twitter_base + a.screen_name;
            a.tweet_url = a.user_url + "/status/" + a.tweet_id;
            a.reply_url = a.twitter_base + "intent/tweet?in_reply_to=" + a.tweet_id;
            a.retweet_url = a.twitter_base + "intent/retweet?tweet_id=" + a.tweet_id;
            a.favorite_url = a.twitter_base + "intent/favorite?tweet_id=" + a.tweet_id;
            a.retweeted_screen_name =
                a.retweet && c.retweeted_status.user.screen_name;
            a.tweet_relative_time = l(a.tweet_time);
            a.entities = c.entities ? (c.entities.urls || []).concat(c.entities.media || []) : [];
            a.tweet_raw_text = a.retweet ? "RT @" + a.retweeted_screen_name + " " + c.retweeted_status.text : c.text;
            a.tweet_text = d([k(a.tweet_raw_text, a.entities)]).linkUser().linkHash()[0];
            a.tweet_text_fancy = d([a.tweet_text]).makeHeart().capAwesome().capEpic()[0];
            a.user = e('<a class="tweet_user" href="{user_url}">{screen_name}</a>', a);
            a.join = b.join_text ? e(' <span class="tweet_join">{join_text}</span> ',
                a) : " ";
            a.avatar = a.avatar_size ? e('<a class="tweet_avatar" href="{user_url}"><img src="{avatar_url}" height="{avatar_size}" width="{avatar_size}" alt="{screen_name}\'s avatar" title="{screen_name}\'s avatar" border="0"/></a>', a) : "";
            a.time = e('<span class="tweet_time"><a href="{tweet_url}" title="view tweet on twitter">{tweet_relative_time}</a></span>', a);
            a.text = e('<span class="tweet_text">{tweet_text_fancy}</span>', a);
            a.reply_action = e('<a class="tweet_action tweet_reply" href="{reply_url}">reply</a>',
                a);
            a.retweet_action = e('<a class="tweet_action tweet_retweet" href="{retweet_url}">retweet</a>', a);
            a.favorite_action = e('<a class="tweet_action tweet_favorite" href="{favorite_url}">favorite</a>', a);
            return a
        }

        var b = d.extend({username: null, list: null, favorites: !1, query: null, avatar_size: null, count: 3, fetch: null, page: 1, retweets: !0, intro_text: null, outro_text: null, join_text: null, auto_join_text_default: "i said,", auto_join_text_ed: "i", auto_join_text_ing: "i am", auto_join_text_reply: "i replied to", auto_join_text_url: "i was looking at",
            loading_text: null, refresh_interval: null, twitter_url: "twitter.com", twitter_api_url: "api.twitter.com", twitter_search_url: "search.twitter.com", template: "{avatar}{time}{join}{text}", comparator: function (b, a) {
                return a.tweet_time - b.tweet_time
            }, filter: function () {
                return!0
            }}, j), h = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?\u00ab\u00bb\u201c\u201d\u2018\u2019]))/gi;
        d.extend({tweet: {t: e}});
        d.fn.extend({linkUser: g(/(^|[\W])@(\w+)/gi, '$1@<a href="http://' + b.twitter_url + '/$2">$2</a>'), linkHash: g(/(?:^| )[\#]+([\w\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u00ff\u0600-\u06ff]+)/gi, ' <a href="http://' + b.twitter_search_url + "/search?q=&tag=$1&lang=all" + (b.username && 1 == b.username.length && !b.list ? "&from=" + b.username.join("%2BOR%2B") : "") + '">#$1</a>'), capAwesome: g(/\b(awesome)\b/gi, '<span class="awesome">$1</span>'), capEpic: g(/\b(epic)\b/gi, '<span class="epic">$1</span>'), makeHeart: g(/(&lt;)+[3]/gi,
            "<tt class='heart'>&#x2665;</tt>")});
        return this.each(function (c, a) {
            var f = d('<ul class="tweet_list">'), g = '<p class="tweet_intro">' + b.intro_text + "</p>", h = '<p class="tweet_outro">' + b.outro_text + "</p>", i = d('<p class="loading">' + b.loading_text + "</p>");
            b.username && "string" == typeof b.username && (b.username = [b.username]);
            d(a).unbind("tweet:load").bind("tweet:load", function () {
                b.loading_text && d(a).empty().append(i);
                d.getJSON(m(), function (c) {
                    d(a).empty().append(f);
                    b.intro_text && f.before(g);
                    f.empty();
                    c = d.map(c.results ||
                        c, n);
                    c = d.grep(c, b.filter).sort(b.comparator).slice(0, b.count);
                    f.append(d.map(c, function (a) {
                        return"<li>" + e(b.template, a) + "</li>"
                    }).join("")).children("li:first").addClass("tweet_first").end().children("li:odd").addClass("tweet_even").end().children("li:even").addClass("tweet_odd");
                    b.outro_text && f.after(h);
                    d(a).trigger("loaded").trigger(c.length === 0 ? "empty" : "full");
                    b.refresh_interval && window.setTimeout(function () {
                        d(a).trigger("tweet:load")
                    }, 1E3 * b.refresh_interval)
                })
            }).trigger("tweet:load")
        })
    }
});

/*! 
 * jquery.event.drag - v 2.0.0 
 * Copyright (c) 2010 Three Dub Media - http://threedubmedia.com
 * Open Source MIT License - http://threedubmedia.com/code/license
 */
;
(function (f) {
    f.fn.drag = function (b, a, d) {
        var e = typeof b == "string" ? b : "", k = f.isFunction(b) ? b : f.isFunction(a) ? a : null;
        if (e.indexOf("drag") !== 0)e = "drag" + e;
        d = (b == k ? a : d) || {};
        return k ? this.bind(e, d, k) : this.trigger(e)
    };
    var i = f.event, h = i.special, c = h.drag = {defaults: {which: 1, distance: 0, not: ":input", handle: null, relative: false, drop: true, click: false}, datakey: "dragdata", livekey: "livedrag", add: function (b) {
        var a = f.data(this, c.datakey), d = b.data || {};
        a.related += 1;
        if (!a.live && b.selector) {
            a.live = true;
            i.add(this, "draginit." + c.livekey, c.delegate)
        }
        f.each(c.defaults, function (e) {
            if (d[e] !== undefined)a[e] = d[e]
        })
    }, remove: function () {
        f.data(this, c.datakey).related -= 1
    }, setup: function () {
        if (!f.data(this, c.datakey)) {
            var b = f.extend({related: 0}, c.defaults);
            f.data(this, c.datakey, b);
            i.add(this, "mousedown", c.init, b);
            this.attachEvent && this.attachEvent("ondragstart", c.dontstart)
        }
    }, teardown: function () {
        if (!f.data(this, c.datakey).related) {
            f.removeData(this, c.datakey);
            i.remove(this, "mousedown", c.init);
            i.remove(this, "draginit", c.delegate);
            c.textselect(true);
            this.detachEvent && this.detachEvent("ondragstart", c.dontstart)
        }
    }, init: function (b) {
        var a = b.data, d;
        if (!(a.which > 0 && b.which != a.which))if (!f(b.target).is(a.not))if (!(a.handle && !f(b.target).closest(a.handle, b.currentTarget).length)) {
            a.propagates = 1;
            a.interactions = [c.interaction(this, a)];
            a.target = b.target;
            a.pageX = b.pageX;
            a.pageY = b.pageY;
            a.dragging = null;
            d = c.hijack(b, "draginit", a);
            if (a.propagates) {
                if ((d = c.flatten(d)) && d.length) {
                    a.interactions = [];
                    f.each(d, function () {
                        a.interactions.push(c.interaction(this, a))
                    })
                }
                a.propagates = a.interactions.length;
                a.drop !== false && h.drop && h.drop.handler(b, a);
                c.textselect(false);
                i.add(document, "mousemove mouseup", c.handler, a);
                return false
            }
        }
    }, interaction: function (b, a) {
        return{drag: b, callback: new c.callback, droppable: [], offset: f(b)[a.relative ? "position" : "offset"]() || {top: 0, left: 0}}
    }, handler: function (b) {
        var a = b.data;
        switch (b.type) {
            case !a.dragging && "mousemove":
                if (Math.pow(b.pageX - a.pageX, 2) + Math.pow(b.pageY - a.pageY, 2) < Math.pow(a.distance, 2))break;
                b.target = a.target;
                c.hijack(b, "dragstart", a);
                if (a.propagates)a.dragging = true;
            case "mousemove":
                if (a.dragging) {
                    c.hijack(b, "drag", a);
                    if (a.propagates) {
                        a.drop !== false && h.drop && h.drop.handler(b, a);
                        break
                    }
                    b.type = "mouseup"
                }
            case "mouseup":
                i.remove(document, "mousemove mouseup", c.handler);
                if (a.dragging) {
                    a.drop !== false && h.drop && h.drop.handler(b, a);
                    c.hijack(b, "dragend", a)
                }
                c.textselect(true);
                if (a.click === false && a.dragging) {
                    jQuery.event.triggered = true;
                    setTimeout(function () {
                        jQuery.event.triggered = false
                    }, 20);
                    a.dragging = false
                }
                break
        }
    }, delegate: function (b) {
        var a = [], d, e = f.data(this, "events") || {};
        f.each(e.live || [], function (k, j) {
            if (j.preType.indexOf("drag") === 0)if (d = f(b.target).closest(j.selector, b.currentTarget)[0]) {
                i.add(d, j.origType + "." + c.livekey, j.origHandler, j.data);
                f.inArray(d, a) < 0 && a.push(d)
            }
        });
        if (!a.length)return false;
        return f(a).bind("dragend." + c.livekey, function () {
            i.remove(this, "." + c.livekey)
        })
    }, hijack: function (b, a, d, e, k) {
        if (d) {
            var j = {event: b.originalEvent, type: b.type}, n = a.indexOf("drop") ? "drag" : "drop", l, o = e || 0, g, m;
            e = !isNaN(e) ? e : d.interactions.length;
            b.type = a;
            b.originalEvent = null;
            d.results = [];
            do if (g = d.interactions[o])if (!(a !== "dragend" && g.cancelled)) {
                m = c.properties(b, d, g);
                g.results = [];
                f(k || g[n] || d.droppable).each(function (q, p) {
                    l = (m.target = p) ? i.handle.call(p, b, m) : null;
                    if (l === false) {
                        if (n == "drag") {
                            g.cancelled = true;
                            d.propagates -= 1
                        }
                        if (a == "drop")g[n][q] = null
                    } else if (a == "dropinit")g.droppable.push(c.element(l) || p);
                    if (a == "dragstart")g.proxy = f(c.element(l) || g.drag)[0];
                    g.results.push(l);
                    delete b.result;
                    if (a !== "dropinit")return l
                });
                d.results[o] = c.flatten(g.results);
                if (a == "dropinit")g.droppable = c.flatten(g.droppable);
                a == "dragstart" && !g.cancelled && m.update()
            } while (++o < e);
            b.type = j.type;
            b.originalEvent = j.event;
            return c.flatten(d.results)
        }
    }, properties: function (b, a, d) {
        var e = d.callback;
        e.drag = d.drag;
        e.proxy = d.proxy || d.drag;
        e.startX = a.pageX;
        e.startY = a.pageY;
        e.deltaX = b.pageX - a.pageX;
        e.deltaY = b.pageY - a.pageY;
        e.originalX = d.offset.left;
        e.originalY = d.offset.top;
        e.offsetX = b.pageX - (a.pageX - e.originalX);
        e.offsetY = b.pageY - (a.pageY - e.originalY);
        e.drop = c.flatten((d.drop || []).slice());
        e.available = c.flatten((d.droppable || []).slice());
        return e
    }, element: function (b) {
        if (b && (b.jquery || b.nodeType == 1))return b
    }, flatten: function (b) {
        return f.map(b, function (a) {
            return a && a.jquery ? f.makeArray(a) : a && a.length ? c.flatten(a) : a
        })
    }, textselect: function (b) {
        f(document)[b ? "unbind" : "bind"]("selectstart", c.dontstart).attr("unselectable", b ? "off" : "on").css("MozUserSelect", b ? "" : "none")
    }, dontstart: function () {
        return false
    }, callback: function () {
    }};
    c.callback.prototype = {update: function () {
        h.drop && this.available.length && f.each(this.available, function (b) {
            h.drop.locate(this, b)
        })
    }};
    h.draginit = h.dragstart = h.dragend = c
})(jQuery);

/*
 * jCarousel Lite 1.0.1
 */
(function ($) {
    $.fn.jCarouselLite = function (o) {
        o = $.extend({btnPrev: null, btnNext: null, btnGo: null, mouseWheel: false, auto: null, speed: 200, easing: null, vertical: false, circular: true, visible: 3, start: 0, scroll: 1, beforeStart: null, afterEnd: null}, o || {});
        return this.each(function () {
            var b = false, animCss = o.vertical ? "top" : "left", sizeCss = o.vertical ? "height" : "width";
            var c = $(this), ul = $("ul", c), tLi = $("li", ul), tl = tLi.size(), v = o.visible;
            if (o.circular) {
                ul.prepend(tLi.slice(tl - v - 1 + 1).clone()).append(tLi.slice(0, v).clone());
                o.start += v
            }
            var f = $("li", ul), itemLength = f.size(), curr = o.start;
            c.css("visibility", "visible");
            f.css({overflow: "hidden", float: o.vertical ? "none" : "left"});
            ul.css({margin: "0", padding: "0", position: "relative", "list-style-type": "none", "z-index": "1"});
            c.css({overflow: "hidden", position: "relative", "z-index": "2", left: "0px"});
            var g = o.vertical ? height(f) : width(f);
            var h = g * itemLength;
            var j = g * v;
            f.css({width: f.width(), height: f.height()});
            ul.css(sizeCss, h + "px").css(animCss, -(curr * g));
            c.css(sizeCss, j + "px");
            if (o.btnPrev)$(o.btnPrev).click(function () {
                return go(curr - o.scroll)
            });
            if (o.btnNext)$(o.btnNext).click(function () {
                return go(curr + o.scroll)
            });
            if (o.btnGo)$.each(o.btnGo, function (i, a) {
                $(a).click(function () {
                    return go(o.circular ? o.visible + i : i)
                })
            });
            if (o.mouseWheel && c.mousewheel)c.mousewheel(function (e, d) {
                return d > 0 ? go(curr - o.scroll) : go(curr + o.scroll)
            });
            if (o.auto)setInterval(function () {
                go(curr + o.scroll)
            }, o.auto + o.speed);
            function vis() {
                return f.slice(curr).slice(0, v)
            };
            function go(a) {
                if (!b) {
                    if (o.beforeStart)o.beforeStart.call(this, vis());
                    if (o.circular) {
                        if (a <= o.start - v - 1) {
                            ul.css(animCss, -((itemLength - (v * 2)) * g) + "px");
                            curr = a == o.start - v - 1 ? itemLength - (v * 2) - 1 : itemLength - (v * 2) - o.scroll
                        } else if (a >= itemLength - v + 1) {
                            ul.css(animCss, -((v) * g) + "px");
                            curr = a == itemLength - v + 1 ? v + 1 : v + o.scroll
                        } else curr = a
                    } else {
                        if (a < 0 || a > itemLength - v)return; else curr = a
                    }
                    b = true;
                    ul.animate(animCss == "left" ? {left: -(curr * g)} : {top: -(curr * g)}, o.speed, o.easing, function () {
                        if (o.afterEnd)o.afterEnd.call(this, vis());
                        b = false
                    });
                    if (!o.circular) {
                        $(o.btnPrev + "," + o.btnNext).removeClass("disabled");
                        $((curr - o.scroll < 0 && o.btnPrev) || (curr + o.scroll > itemLength - v && o.btnNext) || []).addClass("disabled")
                    }
                }
                return false
            }
        })
    };
    function css(a, b) {
        return parseInt($.css(a[0], b)) || 0
    };
    function width(a) {
        return a[0].offsetWidth + css(a, 'marginLeft') + css(a, 'marginRight')
    };
    function height(a) {
        return a[0].offsetHeight + css(a, 'marginTop') + css(a, 'marginBottom')
    }
})(jQuery);
function touchHandler(c) {
    var a = c.changedTouches[0], b = "";
    switch (c.type) {
        case "touchstart":
            b = "mousedown";
            break;
        case "touchmove":
            b = "mousemove";
            break;
        case "touchend":
            b = "mouseup";
            break;
        default:
            return
    }
    var d = document.createEvent("MouseEvent");
    d.initMouseEvent(b, !0, !0, window, 1, a.screenX, a.screenY, a.clientX, a.clientY, !1, !1, !1, !1, 0, null);
    a.target.dispatchEvent(d);
    c.preventDefault()
};

/*
 * Author: niu tech (http://niute.ch)
 */
Cufon.replace('#platynowi li,#carousel li,header h2,aside h2,aside h3,footer h2,.cufontitle h1,.cufontitle h2,.paski,#rss p,#pager,#skroty li,#twitter p,#copy,#hello,.link,#agenda th', {hover: {color: '#b31301'}});
Cufon.replace('#social,#menu a,.data,.czytajwiecej,.zarejestruj,.zobaczrozklad,.dogory', {textShadow: '1px 1px 1px #000'});
Cufon.now();

$(function () {
    //$('figure a').fancybox({overlayShow: false});
    $("#twitty").tweet({ username: "confiturapl", avatar_size: 48, count: 2, refresh_interval: 60, template: "{avatar}{user}{text}{time}", loading_text: "ładowanie tweetów..."});
    var stuknob = $('#stuknob').drag(function (ev, dd) {
        var x = dd.offsetX < 0 ? 0 : (dd.offsetX > 93 ? 93 : dd.offsetX);
        $(this).css({left: x});
        if (x == 93) {
            $(this).parent().addClass('unlocked');
            $('form .czytajwiecej').addClass('unlocked');
        }
    }, {relative: true}).drag("end", function (ev, dd) {
        if (dd.offsetX < 93)
            $(this).animate({left: dd.originalX});
    }).get(0);
    if (stuknob) {
        stuknob.addEventListener('touchstart', touchHandler, false);
        stuknob.addEventListener('touchmove', touchHandler, false);
        stuknob.addEventListener('touchend', touchHandler, false);
        stuknob.addEventListener('touchcancel', touchHandler, false);
    }
    $('form .czytajwiecej').click(function () {
        if ($(this).hasClass('unlocked'))
            $(this).parents('form').submit();
        return false;
    });

});
$(window).load(function () {
    $("#carousel div").jcarousel({
        auto: 2,
        wrap: 'circular',
        scroll: 1,
        animation: 1000,
        itemLastInCallback: {
            onBeforeAnimation: function (instance, li, idx, state) {
                Cufon.replace('#carousel li')
            }
        },
        initCallback: function (carousel) {
            $('#nextcar').bind('click', function () {
                carousel.next();
                return false;
            });

            $('#prevcar').bind('click', function () {
                carousel.prev();
                return false;
            });
        },
    });
});
