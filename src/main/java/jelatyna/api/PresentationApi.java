package jelatyna.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.emory.mathcs.backport.java.util.Collections;
import jelatyna.domain.Presentation;
import jelatyna.domain.dto.PresentationDto;
import jelatyna.domain.dto.converters.PresentationConverter;
import jelatyna.services.PresentationService;
import jelatyna.services.SpeakerService;

@Controller
public class PresentationApi {

    @Autowired
    private PresentationConverter presentationConverter;

    @Autowired
    private PresentationService presentationService;

    @Autowired
    private SpeakerService speakerService;


    @RequestMapping(value = "presentations", method = RequestMethod.GET)
    @ResponseBody
    List<PresentationDto> acceptedPresentations() {
        List<PresentationDto> presentations = presentationService.acceptedPresentations()
                .stream()
                .map(p -> presentationConverter.map(p))
                .collect(Collectors.toList());
        Collections.shuffle(presentations);
        return presentations;
    }

    @RequestMapping(value = "presentation/{id}", method = RequestMethod.GET)
    @ResponseBody
    public PresentationDto getPresentation(@PathVariable("id") int id) {
        Presentation presentation = presentationService.findBy(id);
        return (presentation == null) ? null : presentationConverter.map(presentation);
    }
}
