package jelatyna.services.exceptions;

@SuppressWarnings("serial")
public class LoginException extends RuntimeException {

    public LoginException(String message) {
        super(message);
    }

}
