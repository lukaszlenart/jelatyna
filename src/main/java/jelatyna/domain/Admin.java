package jelatyna.domain;

import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class Admin extends User<Admin> implements WithFile {

    public Admin() {
    }

    public Admin(String mail) {
        mail(mail);
    }

    @Override
    public String getSubfolder() {
        return "kapitula";
    }

    @Override
    public String getFileName() {
        return getId() + ".jpg";
    }

}
