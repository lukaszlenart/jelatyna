package jelatyna.repositories;

import org.springframework.data.repository.CrudRepository;

interface AgendaEntityRepository extends CrudRepository<AgendaEntity, String> {

}
