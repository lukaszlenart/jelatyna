package jelatyna.domain;

public interface WithFile {
    String getSubfolder();

    String getFileName();
}
