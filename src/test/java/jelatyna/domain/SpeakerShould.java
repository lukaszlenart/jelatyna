package jelatyna.domain;

import org.junit.Test;

import java.util.List;

import static jelatyna.domain.Speaker.*;
import static org.assertj.core.api.Assertions.*;

public class SpeakerShould {
    private Speaker speaker = new Speaker();

    @Test
    public void get70CharachersOfBioIfLonger() {
        String bio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." +
                " Nam imperdiet orci in nisi eleifend congue. Nulla ipsum libero, " +
                "malesuada id faucibus eget, mattis non ipsum. Nulla sed nisl ut " +
                "sem luctus varius. Nullam eleifend tortor eu lacus hendrerit quis" +
                " auctor justo porta. Curabitur at consectetur nisl. Sed nec lorem";
        speaker.bio(bio);

        String shortBio = speaker.getShortBio();

        assertThat(shortBio).hasSize(SHORT_BIO_LENGTH).isEqualTo(bio.substring(0, SHORT_BIO_LENGTH));
    }

    @Test
    public void getWholeBioIfShorterThenLimit() {
        String bio = "this is my bio";
        speaker.bio(bio);

        String shortBio = speaker.getShortBio();

        assertThat(shortBio).isEqualTo(bio);
    }

    @Test
    public void getEmptyShortBioIfBioIsEmpty() {

        String shortBio = speaker.getShortBio();

        assertThat(shortBio).isEmpty();
    }

    @Test
    public void getShortBioWithoutAnyTags() {
        speaker.bio("<p>full<br /> bio</p>");

        String shortBio = speaker.getShortBio();

        assertThat(shortBio).isEqualTo("full bio");
    }

    @Test
    public void get_accepted_presentations() throws Exception {
        Presentation acceptedPresentation = new Presentation().id(1).accepted(true);
        speaker.addPresentation(acceptedPresentation);
        speaker.addPresentation(new Presentation().id(2).accepted(false));

        List<Presentation> accepted = speaker.getAcceptedPresentations();

        assertThat(accepted).containsOnly(acceptedPresentation);
    }

    @Test
    public void shouldReturnFalseWhenAcceptedUserHasAcceptedPresentation() {
        Speaker speaker = getSpeakerWithPresentation(true, true);

        assertThat(speaker.isNecessaryToRefreshAcceptance()).isFalse();
    }

    @Test
    public void shouldReturnTrueWhenAcceptedUserHasNotAcceptedPresentation() {
        Speaker speaker = getSpeakerWithPresentation(true, false);

        assertThat(speaker.isNecessaryToRefreshAcceptance()).isTrue();
    }

    @Test
    public void shouldUpdateAcceptanceStatusWhenAcceptedSpeakerDoesNotHaveAnyAcceptedPresentation() {
        Speaker speaker = getSpeakerWithPresentation(false, true);

        speaker.updateSpeakerAcceptanceStatus();

        assertThat(speaker.isAccepted()).isFalse();
    }

    @Test
    public void shouldNotChangeAcceptanceStatusWhenNotAcceptedSpeakerHasAcceptedPresentation() {
        Speaker speaker = getSpeakerWithPresentation(false, true);

        speaker.updateSpeakerAcceptanceStatus();

        assertThat(speaker.isAccepted()).isFalse();
    }

    private Speaker getSpeakerWithPresentation(boolean speakerAccepted, boolean presentationAccepted) {
        Presentation acceptedPresentation = new Presentation().id(1).accepted(presentationAccepted);
        Speaker speaker = new Speaker();
        speaker.addPresentation(acceptedPresentation);
        speaker.accepted(speakerAccepted);
        return speaker;
    }

}
