package jelatyna.services.exceptions;

@SuppressWarnings("serial")
public class UserDoesNotExistException extends RuntimeException {
    public UserDoesNotExistException(String message) {
        super(message);
    }
}
