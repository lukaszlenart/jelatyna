package jelatyna.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

public class AgendaItem implements Serializable {
    private static final long serialVersionUID = -41732272905974108L;
    public String value = "";
    public Integer presentationId;

    public AgendaItem(String value) {
        this.value = value;
    }

    public AgendaItem(Integer presentationId) {
        this.presentationId = presentationId;
    }

    public AgendaItem(Integer presentationId, String value) {
        this.presentationId = presentationId;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public boolean hasPresentation() {
        return presentationId != null;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AgendaItem)) {
            return false;
        }
        AgendaItem other = (AgendaItem) o;
        return new EqualsBuilder()
                .append(presentationId, other.presentationId)
                .append(value, other.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(presentationId)
                .append(value)
                .toHashCode();
    }
}
