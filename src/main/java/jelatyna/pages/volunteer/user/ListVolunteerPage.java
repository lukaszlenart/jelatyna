package jelatyna.pages.volunteer.user;

import jelatyna.components.DeleteEntityLink;
import jelatyna.components.RedirectLink;
import jelatyna.domain.Volunteer;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.admin.user.ChangePasswordPage;
import jelatyna.services.VolunteerService;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ListVolunteerPage extends AdminBasePage {
    @SpringBean
    private VolunteerService service;

    public ListVolunteerPage() {
        add(new UserGrid(service.findAll()));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.ORGANIZERS;
    }

    @Override
    public String getPageTitle() {
        return AdminMenuItemEnum.ORGANIZERS.getLabel();
    }

    private final class UserGrid extends DataView<Volunteer> {
        private UserGrid(List<Volunteer> list) {
            super("rows", new ListDataProvider<Volunteer>(list));
        }

        @Override
        protected void populateItem(Item<Volunteer> item) {
            final Volunteer volunteer = item.getModelObject();
            item.add(label("firstName", volunteer.getFirstName()));
            item.add(label("lastName", volunteer.getLastName()));
            item.add(new RedirectLink("edit", AddVolunteerPage.class, volunteer));
            item.add(new RedirectLink("changePassword", ChangePasswordPage.class, volunteer));
            item.add(new DeleteEntityLink(volunteer, ListVolunteerPage.class) {
                @Override
                protected void deleteById(Integer id) {
                    service.deleteById(id);
                }
            });
        }
    }
}
