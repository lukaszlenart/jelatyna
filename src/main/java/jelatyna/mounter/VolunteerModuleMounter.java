package jelatyna.mounter;

import jelatyna.Confitura;
import jelatyna.pages.volunteer.VolunteerHomePage;
import jelatyna.pages.volunteer.login.VolunteerLoginPage;

public class VolunteerModuleMounter extends ModuleMounter {

    public VolunteerModuleMounter(Confitura confitura) {
        super("/volunteer", confitura);
    }

    @Override
    public void mount() {
        mountPage("", VolunteerHomePage.class);
        mountPage("/login", VolunteerLoginPage.class);
    }

}
