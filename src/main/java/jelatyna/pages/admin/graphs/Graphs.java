package jelatyna.pages.admin.graphs;

import jelatyna.domain.Participant;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.*;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;

public class Graphs {

    private final List<Participant> participants;

    public Graphs(List<Participant> participants) {
        this.participants = participants;
    }

    public Graph registrationGraph() {
        Map<String, Integer> map = participants
                .stream()
                .sorted(comparing(Participant::getRegistrationTime))
                .collect(groupingBy(p -> p.getRegistrationHour().toString(), reducing(0, e -> 1, Integer::sum)));
        return new Graph(newArrayList(map.keySet()), newArrayList(map.values()));
    }

}
