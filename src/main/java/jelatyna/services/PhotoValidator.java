package jelatyna.services;

import com.google.common.collect.Range;
import jelatyna.utils.ImageIO;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.List;

import static java.math.BigDecimal.*;

@SuppressWarnings("serial")
public class PhotoValidator implements IValidator<List<FileUpload>> {
    static final String WRONG_RATION = "photo.ratio.error";
    static final String WRONG_TYPE = "photo.type.error";
    static final BigDecimal TOLERANCE = new BigDecimal("0.10");
    private ImageIO imageIO = new ImageIO();

    @Override
    public void validate(IValidatable<List<FileUpload>> validatable) {
        if (validatable.getValue() == null) {
            return;
        }
        try {
            BufferedImage image = imageIO.read(validatable.getValue().get(0));
            if (createRange(image).contains(image.getWidth()) == false) {
                validatable.error(new ValidationError().addKey(WRONG_RATION));
            }
        } catch (Exception ex) {
            validatable.error(new ValidationError().addKey(WRONG_TYPE));
        }
    }

    private Range<Integer> createRange(BufferedImage image) {
        return Range.closed(multiplyBy(image, ONE.subtract(TOLERANCE).doubleValue()), multiplyBy(image, ONE.add(TOLERANCE).doubleValue()));
    }

    private int multiplyBy(BufferedImage image, double ratio) {
        return (int) Math.round(image.getHeight() * ratio);
    }
}
