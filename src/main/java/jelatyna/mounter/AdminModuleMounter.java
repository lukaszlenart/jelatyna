package jelatyna.mounter;

import jelatyna.Confitura;
import jelatyna.pages.admin.AdminHomePage;
import jelatyna.pages.admin.agenda.DefineAgendaPage;
import jelatyna.pages.admin.c4p.DefineRegistrationMailPage;
import jelatyna.pages.admin.c4p.DefineResetPassMailPage;
import jelatyna.pages.admin.c4p.presentation.ListPresentationPage;
import jelatyna.pages.admin.c4p.presentation.ViewPresentationPage;
import jelatyna.pages.admin.c4p.speaker.EditSpeakerPage;
import jelatyna.pages.admin.c4p.speaker.ListSpeakerPage;
import jelatyna.pages.admin.c4p.speaker.ViewSpeakerPage;
import jelatyna.pages.admin.files.FilesPage;
import jelatyna.pages.admin.login.LoginPage;
import jelatyna.pages.admin.lottery.LotteryPage;
import jelatyna.pages.admin.menu.DefineMainMenuPage;
import jelatyna.pages.admin.news.AddNewsPage;
import jelatyna.pages.admin.news.ListNewsPage;
import jelatyna.pages.admin.registration.DefineParticipantMailPage;
import jelatyna.pages.admin.registration.GroupMailPage;
import jelatyna.pages.admin.registration.ListParticipantsPage;
import jelatyna.pages.admin.registration.RegisterParticipantsPage;
import jelatyna.pages.admin.registration.RegistrationSettingsPage;
import jelatyna.pages.admin.registration.StatisticsPage;
import jelatyna.pages.admin.simple.AddSimpleContentPage;
import jelatyna.pages.admin.simple.ListSimpleContentPage;
import jelatyna.pages.admin.sponsor.AddSponsorPage;
import jelatyna.pages.admin.sponsor.ListSponsorPage;
import jelatyna.pages.admin.sponsor.SponsorTypesPage;
import jelatyna.pages.admin.user.AddAdminPage;
import jelatyna.pages.admin.user.ListAdminPage;
import jelatyna.pages.volunteer.user.AddVolunteerPage;
import jelatyna.pages.volunteer.user.ListVolunteerPage;

public class AdminModuleMounter extends ModuleMounter {

    public AdminModuleMounter(Confitura confitura) {
        super("/admin", confitura);
    }

    @Override
    public void mount() {
        mountPage("", AdminHomePage.class);
        mountPage("/", AdminHomePage.class);
        mountPage("/login", LoginPage.class);
        mountPage("/menu", DefineMainMenuPage.class);
        mountPage("/content", ListSimpleContentPage.class);
        mountPage("/content/${id}", AddSimpleContentPage.class);
        mountPage("/c4p/presentations", ListPresentationPage.class);
        mountPage("/c4p/presentations/${id}", ViewPresentationPage.class);
        mountPage("/c4p/speakers", ListSpeakerPage.class);
        mountPage("/c4p/speakers/${id}", ViewSpeakerPage.class);
        mountPage("/c4p/speakers/edit/${id}", EditSpeakerPage.class);
        mountPage("/c4p/mail", DefineRegistrationMailPage.class);
        mountPage("/c4p/resetpassmail", DefineResetPassMailPage.class);
        mountPage("/registration/settings", RegistrationSettingsPage.class);
        mountPage("/registration/participants", ListParticipantsPage.class);
        mountPage("/registration/mail", DefineParticipantMailPage.class);
        mountPage("/registration/groupMail", GroupMailPage.class);
        mountPage("/registration/stats", StatisticsPage.class);
        mountPage("/registration/participation", RegisterParticipantsPage.class);
        mountPage("/sponsors", ListSponsorPage.class);
        mountPage("/sponsors/${id}", AddSponsorPage.class);
        mountPage("/sponsors/types/${id}", SponsorTypesPage.class);
        mountPage("/news", ListNewsPage.class);
        mountPage("/news/${id}", AddNewsPage.class);
        mountPage("/upload", FilesPage.class);
        mountPage("/organizers", ListAdminPage.class);
        mountPage("/organizers/${id}", AddAdminPage.class);
        mountPage("/lottery", LotteryPage.class);
        mountPage("/agenda", DefineAgendaPage.class);

        mountPage("/volunteers", ListVolunteerPage.class);
        mountPage("/volunteers/${id}", AddVolunteerPage.class);
    }

}
