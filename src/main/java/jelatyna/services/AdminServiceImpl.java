package jelatyna.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jelatyna.domain.Admin;
import jelatyna.repositories.AdminRepository;
import jelatyna.repositories.UserRepository;

@Service
public class AdminServiceImpl extends UserServiceImpl<Admin> implements AdminService {

    @Autowired
    private AdminRepository repository;

    @Override
    public Admin loginWith(String email, String password) {
        if (noUsersDefinedYet()) {
            return new Admin(email);
        } else {
            return super.loginWith(email, password);
        }
    }

    @Override
    boolean passwordIsCorrect(Admin user, String password) {
        return true;
    }

    private boolean noUsersDefinedYet() {
        return getRepository().count() == 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <REPO extends UserRepository<Admin>> REPO getRepository() {
        return (REPO) repository;
    }
}
