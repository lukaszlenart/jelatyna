package jelatyna.repositories;

import jelatyna.domain.RegistrationConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegistrationConfigurationRepository extends JpaRepository<RegistrationConfiguration, Integer> {

}
