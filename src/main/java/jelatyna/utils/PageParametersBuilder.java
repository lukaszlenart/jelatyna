package jelatyna.utils;

import org.apache.wicket.request.mapper.parameter.PageParameters;

public class PageParametersBuilder {
    public static final String ID = "id";
    public static final String TITLE = "title";
    private PageParameters params = new PageParameters();

    public static PageParametersBuilder params() {
        return new PageParametersBuilder();
    }

    public PageParameters build() {
        return params;
    }

    public PageParametersBuilder id(Object value) {
        return put(ID, value);
    }

    public PageParametersBuilder title(Object value) {
        return put(TITLE, value);
    }

    public static PageParameters paramsFor(String key, Object value) {
        return new PageParametersBuilder().put(key, value).params;
    }

    private PageParametersBuilder put(String key, Object value) {
        if (value != null) {
            params.add(key, value);
        }
        return this;
    }

}
