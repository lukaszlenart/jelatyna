package jelatyna.pages.confitura.speaker;

import jelatyna.components.RedirectLink;
import jelatyna.components.StaticImage;
import jelatyna.domain.Speaker;
import jelatyna.services.FileService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class SpeakersWidget extends Panel {
    @SpringBean
    private SpeakerService service;
    @SpringBean
    private FileService fileService;

    public SpeakersWidget(String id) {
        super(id);
        Speaker speaker = service.randomSpeaker();
        if (speaker == null) {
            setVisible(false);
        } else {
            doShowSpeaker(speaker);
        }
    }

    private void doShowSpeaker(Speaker speaker) {
        add(new StaticImage("photo", fileService.getUrlTo(speaker)));
        add(richLabel("bio", speaker.getShortBio() + "(...)"));
        add(linkToSpeaker(speaker));
    }

    private Component linkToSpeaker(Speaker speaker) {
        RedirectLink link = new RedirectLink("link", ListSpeakerPage.class).withAnchor(speaker.getFullName());
        link.add(label("first_name", speaker.getFirstName()));
        link.add(label("last_name", speaker.getLastName()));
        return link;
    }

}
