package jelatyna.pages.confitura.registration;

import org.apache.wicket.markup.html.panel.FeedbackPanel;

import jelatyna.pages.confitura.BaseWebPage;

@SuppressWarnings("serial")
public class RegistrationInfoPage extends BaseWebPage {

    public RegistrationInfoPage(String info) {
        add(new FeedbackPanel("feedback"));
        info(info);
        setPageTitlePostfix("Rejestracja");
    }

}
