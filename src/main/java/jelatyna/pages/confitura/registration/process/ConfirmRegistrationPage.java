package jelatyna.pages.confitura.registration.process;

import org.apache.wicket.request.mapper.parameter.PageParameters;

import jelatyna.domain.RegistrationStatus;

@SuppressWarnings("serial")
public class ConfirmRegistrationPage extends ChangeRegistrationStatePage {

    public ConfirmRegistrationPage(PageParameters params) {
        super(params, RegistrationStatus.CONFIRMED);
        setPageTitlePostfix("Potwierdzenie rejestracji");
    }

    @Override
    String getMessage() {
        return "Dziękujemy za potwierdzenie rejestracji!";
    }

}
