package jelatyna.pages.admin.headerpanel;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import jelatyna.ConfituraSession;
import jelatyna.components.nogeneric.Link;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;

import java.io.Serializable;
import java.util.Collection;

@SuppressWarnings("serial")
public class AdminTopHeaderPanel extends Panel {

    private AdminTopHeaderPanel(final Builder builder) {
        super(builder.id);

        BookmarkablePageLink<Void> homePageLink = new BookmarkablePageLink<Void>("homePageLink", builder.homePage);
        homePageLink.add(new Label("label", builder.applicationName));
        add(homePageLink);

        add(new Link("logout") {
            @Override
            public void onClick() {
                ConfituraSession.get().invalidateNow();
                setResponsePage(builder.homePage);//LoginPage.class);
            }

        });

        RepeatingView repeatingView = new RepeatingView("menuItems");

        for (AdminMenuItemEnum item : builder.linksMap.keySet()) {
            boolean shouldBeActive = item.equals(builder.activeMenuItem);

            Collection<BookmarkablePageLink<?>> linksInThisMenuItem = builder.linksMap.get(item);

            if (linksInThisMenuItem.size() == 1) {
                BookmarkablePageLink<?> pageLink = Iterables.get(linksInThisMenuItem, 0);

                MenuLinkItem menuLinkItem = new MenuLinkItem(repeatingView.newChildId(), pageLink, shouldBeActive);
                repeatingView.add(menuLinkItem);
            } else {
                repeatingView.add(new MenuDropdownItem(repeatingView.newChildId(), item, linksInThisMenuItem,
                        shouldBeActive));
            }
        }

        add(repeatingView);
    }

    public static class Builder implements Serializable {

        private String id;
        private Class<? extends Page> homePage;
        private String applicationName;
        private AdminMenuItemEnum activeMenuItem;

        private Multimap<AdminMenuItemEnum, BookmarkablePageLink<?>> linksMap = LinkedHashMultimap.create();

        public Builder(String id, Class<? extends Page> homePage, String applicationName,
                       AdminMenuItemEnum activeMenuItem) {
            this.id = id;
            this.homePage = homePage;
            this.applicationName = applicationName;
            this.activeMenuItem = activeMenuItem;
        }

        public Builder withMenuItem(AdminMenuItemEnum menuItem, Class<? extends Page> pageToLink) {
            Preconditions.checkState(linksMap.containsKey(menuItem) == false, "Builder already contains " + menuItem +
                    ". Please use withMenuItemInDropdown if you need many links in one menu item");
            BookmarkablePageLink<Page> link = new BookmarkablePageLink<Page>("link", pageToLink);
            link.setBody(new Model<String>(menuItem.getLabel()));
            linksMap.put(menuItem, link);
            return this;
        }

        public Builder withMenuItemAsDropdown(AdminMenuItemEnum menuItem, Class<? extends Page> pageToLink, String label) {
            BookmarkablePageLink<Page> link = new BookmarkablePageLink<Page>("link", pageToLink);
            link.setBody(new Model<String>(label));
            linksMap.put(menuItem, link);
            return this;
        }

        public AdminTopHeaderPanel build() {
            return new AdminTopHeaderPanel(this);
        }
    }
}
