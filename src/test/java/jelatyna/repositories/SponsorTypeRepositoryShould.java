package jelatyna.repositories;

import jelatyna.WicketIntegrationTest;
import jelatyna.domain.SponsorType;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class SponsorTypeRepositoryShould extends WicketIntegrationTest {

    @Autowired
    private SponsorTypeRepository repository;

    @Test
    public void findAllTypesSortedByOrder() {
        repository.save(new SponsorType().position(2));
        repository.save(new SponsorType().position(0));
        repository.save(new SponsorType().position(1));
        repository.flush();

        List<SponsorType> sponsorTypes = repository.findAllSorted();

        assertThat(sponsorTypes).extracting("position").containsExactly(0, 1, 2);
    }
}
