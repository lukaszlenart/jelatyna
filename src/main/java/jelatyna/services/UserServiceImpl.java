package jelatyna.services;

import static jelatyna.utils.HtmlUtils.*;

import java.util.List;

import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.jasypt.util.password.PasswordEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import jelatyna.domain.User;
import jelatyna.repositories.UserRepository;
import jelatyna.services.exceptions.LoginException;

public abstract class UserServiceImpl<USER extends User<?>> implements UserService<USER> {
    static final String ACCESS_DENIED = "c4p.login.error";
    static final String MAIL_NOT_UNIQUE = "c4p.mail-not-unique.error";
    static final String WRONG_OLD_PASSWORD = "cp4.password.old.wrong.error";
    @Autowired
    private FileService fileService;
    private PasswordEncryptor encryptor = new StrongPasswordEncryptor();

    @Override
    public USER loginWith(String email, String password) {
        USER user = getRepository().findByMail(email);
        if (user != null && passwordIsCorrect(user, password)) {
            return user;
        } else {
            throw new LoginException(ACCESS_DENIED);
        }
    }

    @Override
    @Transactional
    public void save(USER user, FileUpload fileUpload) {
        if (fileUpload != null) {
            user.setFileName(escape(fileUpload.getClientFileName()));
        }
        if (user.isNew()) {
            saveNewUser(user);
        } else {
            doSave(user);
        }
        savePhoto(user, fileUpload);
    }

    @Override
    public USER findById(int id) {
        return getRepository().findOne(id);
    }

    @Override
    public List<USER> findAll() {
        return getRepository().findAll();
    }

    @Override
    public void deleteById(Integer id) {
        getRepository().delete(id);
    }

    @Override
    public void changePassword(USER user, String oldPassword, String newPassword) {
        if (passwordIsCorrect(user, oldPassword)) {
            resetPassword(user, newPassword);
        } else {
            throw new LoginException(WRONG_OLD_PASSWORD);
        }
    }


    @Override
    public void resetPassword(USER user, String newPassword) {
        encryptPassword(user, newPassword);
        getRepository().save(user);
    }

    private User<?> encryptPassword(USER user, String newPassword) {
        return user.password(encryptor.encryptPassword(newPassword));
    }

    private void doSaveNewUser(USER user) {
        encryptPassword(user, user.getPlainPassword());
        doSave(user);
    }

    private void doSave(USER user) {
        user.bio(clearHtml(user.getBio()));
        getRepository().save(user);

    }

    private boolean mailIsUnique(USER user) {
        return getRepository().findByMail(user.getMail()) == null;
    }

    private void savePhoto(USER user, FileUpload fileUpload) {
        if (fileUpload != null) {
            fileService.save(fileUpload, user);
        }
    }

    private String escape(String clientFileName) {
        if (clientFileName != null) {
            return clientFileName.replaceAll("\\s+", "-");
        }
        return null;
    }

    boolean passwordIsCorrect(USER user, String password) {
        return encryptor.checkPassword(password, user.getPassword());
    }

    void saveNewUser(USER user) {
        if (mailIsUnique(user)) {
            doSaveNewUser(user);
        } else {
            throw new LoginException(MAIL_NOT_UNIQUE);
        }
    }

    abstract protected <REPO extends UserRepository<USER>> REPO getRepository();

}