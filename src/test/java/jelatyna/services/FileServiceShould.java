package jelatyna.services;

import jelatyna.domain.WithFile;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static jelatyna.TestUtils.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceShould {

    private static final String MAIN_FOLDER = "/main";
    private static final String CONTEXT = "/context";
    private static final String FILE_NAME = "filaname";

    @Mock
    private FileUpload fileUpload;
    @Mock
    private FileSystem fileSystem;
    @InjectMocks
    private FileService service;

    @Before
    public void setMainFolder() {
        service.mainFolder = MAIN_FOLDER;
        service.context = CONTEXT;
    }

    @Test
    public void saveFileToGivenPath() {
        when(fileUpload.getClientFileName()).thenReturn(FILE_NAME);

        service.save(fileUpload, newArrayList("encryptor", "b", "c"));

        verifyFileSavedIn("/encryptor/b/c");
    }

    @Test
    public void doNothingOnSavingIfFileUploadIsNull() {

        service.save(null, newArrayList("encryptor", "b", "c"));

        verifyZeroInteractions(fileSystem);
    }

    @Test
    public void saveFileWithGivenName() {

        service.save(fileUpload, newArrayList("encryptor", "b"), FILE_NAME);

        verifyFileSavedIn("/encryptor/b");
    }

    @Test
    public void doNothingOnSavingWithNameIfFileUploadIsNull() {

        service.save(null, newArrayList("encryptor", "b", "c"), FILE_NAME);

        verifyZeroInteractions(fileSystem);
    }

    @Test
    public void saveEntityWithFile() {
        WithFile withFile = withFile("subfolder", FILE_NAME);

        service.save(fileUpload, withFile);

        verifyFileSavedIn("/subfolder");
    }

    @Test
    public void doNothingOnSavingEntityIfFileUploadIsNull() {

        service.save(null, withFile("subfolder", FILE_NAME));

        verifyZeroInteractions(fileSystem);
    }

    @Test
    public void getUrlToFileFromEntity() {
        WithFile withFile = withFile("subfolder", FILE_NAME);

        String url = service.getUrlTo(withFile);

        assertUrlHas(url, "subfolder", FILE_NAME);
    }

    @Test
    public void getEmptyUrlIfFileNameIsNull() {
        WithFile withFile = withFile("subfolder", null);

        String url = service.getUrlTo(withFile);

        assertThat(url).isEmpty();
    }

    @Test
    public void getUrlToFileInPath() {

        String url = service.getUrlTo(newArrayList("encryptor", "b", "c"), FILE_NAME);

        assertUrlHas(url, "encryptor", "b", "c", FILE_NAME);
    }

    @Test
    public void returnSortedFoldersThenFiles() {
        File[] fies = $(file("file_z"), folder("folder_z"), file("file_a"), folder("folder_a"));
        when(fileSystem.listFilesIn(anyString())).thenReturn(fies);

        List<File> listedFiles = service.listFilesIn(newArrayList("folder"));

        assertThat(listedFiles).extracting("directory").containsExactly(true, true, false, false);
        assertThat(listedFiles).extracting("name").containsExactly("folder_a", "folder_z", "file_a", "file_z");
    }

    @Test
    public void getPathToEntity() {
        WithFile withFile = withFile("subfolder", FILE_NAME);

        String path = service.getPathTo(withFile);

        assertThat(path).isEqualTo(MAIN_FOLDER + "/subfolder/" + FILE_NAME);
    }

    private WithFile withFile(String subfolder, String fileName) {
        WithFile withFile = mock(WithFile.class);
        when(withFile.getSubfolder()).thenReturn(subfolder);
        when(withFile.getFileName()).thenReturn(fileName);
        return withFile;
    }

    private void assertUrlHas(String url, String... elements) {
        assertThat(url).isEqualTo(createUrl(elements));
    }

    private String createUrl(String... elements) {
        String expected = CONTEXT;
        for (String element : elements) {
            expected += "/" + element;
        }
        return expected;
    }

    private void verifyFileSavedIn(String path) {
        verify(fileSystem).saveFile(fileUpload, MAIN_FOLDER + path, FILE_NAME);
    }
}
