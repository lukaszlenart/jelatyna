package jelatyna.pages.admin.registration;

import static java.lang.String.*;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;

import jelatyna.domain.Participant;
import jelatyna.utils.MailSender;

@Component
public class GroupMailSender {
    private Logger logger = LoggerFactory.getLogger(getClass());
    static final String BASE_URL = "http://confitura.pl/#/register";
    @Autowired
    private MailSender sender;
    private long delay = 30000;


    public GroupMailSender(int delay) {
        this.delay = delay;
    }

    public GroupMailSender() {
    }

    public void sendMessages(List<Participant> participants) {
        sender.setTemplateName("group_mail");
        doSendMessages(participants);
    }

    @Async
    private void doSendMessages(List<Participant> participants) {
        for (List<Participant> subList : Iterables.partition(participants, 50)) {
            for (Participant participant : subList) {
                logSending(participants, participant);
                sendMessage(participant);
            }
            doWait();
        }
    }

    private void sendMessage(Participant participant) {
        sender.set("firstName", participant.getFirstName());
        sender.set("lastName", participant.getLastName());
        sender.set("confirmLink", getLink(participant, "final"));
        sender.set("cancelLink", getLink(participant, "cancel"));
        sender.set("statusLink", getLink(participant, "status"));
        sender.sendMessage(participant);
    }


    private String getLink(Participant participant, String type) {
        return BASE_URL + "/" + type + "/" + participant.getToken();
    }


    private void logSending(List<Participant> participants, Participant participant) {
        logger.info(format("%s/%s Sending email to %s",
                participants.indexOf(participant) + 1, participants.size(), participant.getMail()));
    }

    private void doWait() {
        try {
            Thread.sleep(delay);
        } catch (Exception ex) {
        }
    }
}
