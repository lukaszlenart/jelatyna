package jelatyna.components.datetimepicker;

import org.apache.wicket.datetime.DateConverter;
import org.apache.wicket.datetime.markup.html.form.DateTextField;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.model.IModel;

import java.util.Date;

public class DateTimePicker extends DateTextField {
    public DateTimePicker(String id, IModel<Date> model, DateConverter converter) {
        super(id, model, converter);
    }

    public DateTimePicker(String id, DateConverter converter) {
        super(id, converter);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(CssHeaderItem.forReference(DateTimePickerCssResourceReference.get()));
        response.render(JavaScriptHeaderItem.forReference(DateTimePickerJavaScriptResourceReference.get()));

        response.render(OnDomReadyHeaderItem.forScript("$('#"+getMarkupId()+"').datetimepicker({format: 'd-m-Y H:i', mask: true});"));
    }
}
