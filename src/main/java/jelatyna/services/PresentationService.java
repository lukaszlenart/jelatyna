package jelatyna.services;

import jelatyna.domain.Presentation;
import org.apache.wicket.markup.html.form.upload.FileUpload;

import java.util.List;


public interface PresentationService {
    void save(Presentation presentation, FileUpload fileUpload);

    Presentation findBy(Integer id);

    List<Presentation> acceptedPresentations();
}
