package jelatyna.pages.admin.registration;

import edu.emory.mathcs.backport.java.util.Arrays;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.confitura.registration.form.ParticipantDetailsEditFormPanel;
import jelatyna.repositories.ParticipantRepository;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

public class EditParticipantPage extends AdminBasePage {

    @SpringBean
    private ParticipantRepository repository;

    public EditParticipantPage() {
        final Participant participant = new Participant();
        initPage(participant);
    }

    public EditParticipantPage(PageParameters parameters) {
        Participant participant = repository.findOne(parameters.get("id").toInt());
        initPage(participant);
    }

    private void initPage(final Participant participant) {
        add(new ConfituraFeedbackPanel("feedback"));
        Form editForm = new Form("form");
        add(editForm);

        editForm.add(new ParticipantDetailsEditFormPanel("detailsFormPanel", editForm, participant));
        editForm.add(required(withLabel("Status rejestracji",
                dropDown("registrationStatus", propertyModel(participant, "registrationType"), Arrays.asList(RegistrationStatus.values())))));

        Button submitButton = new Button("saveChanges") {
            @Override
            public void onSubmit() {
                if (emailsIsUnique(participant)) {
                    repository.saveAndFlush(participant);
                    getSession().info(participant.getFullName() + ": zmiany zostały zapisane.");
                    setResponsePage(ListParticipantsPage.class);
                } else {
                    error("Podany adres e-mail już istnieje w bazie");
                }
            }
        };
        editForm.add(submitButton);
        editForm.add(cancelLink(ListParticipantsPage.class));
    }


    private boolean emailsIsUnique(Participant editedParticipant) {
        Participant existingParticipant = repository.findByMail(editedParticipant.getMail());

        if (existingParticipant == null) {
            return true;
        }

        return existingParticipant.getId().equals(editedParticipant.getId());
    }


    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "Edytuj/Stwórz uczestnika";
    }
}
