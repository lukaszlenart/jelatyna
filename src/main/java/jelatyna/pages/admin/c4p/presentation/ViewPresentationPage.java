package jelatyna.pages.admin.c4p.presentation;

import jelatyna.components.user.speaker.ViewPresentationPanel;
import jelatyna.domain.Presentation;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.repositories.PresentationRepository;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class ViewPresentationPage extends AdminBasePage {
    @SpringBean
    private PresentationRepository repository;

    public ViewPresentationPage(PageParameters params) {
        final Presentation presentation = repository.findOne(params.get("id").toInteger());
        add(ViewPresentationPanel.createPanel(presentation));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return AdminMenuItemEnum.CALL_4_PAPERS.getLabel();
    }
}
