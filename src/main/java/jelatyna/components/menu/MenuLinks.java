package jelatyna.components.menu;

import jelatyna.domain.MenuItem;
import jelatyna.domain.MenuLinkItem;
import jelatyna.pages.confitura.ViewAdminPage;
import jelatyna.pages.confitura.ViewSimpleContentPage;
import jelatyna.pages.confitura.c4p.ViewSpeakerPage;
import jelatyna.pages.confitura.news.ListNewsPage;
import jelatyna.pages.confitura.registration.form.RegistrationPage;
import jelatyna.pages.confitura.speaker.ListPresentationPage;
import jelatyna.pages.confitura.speaker.ListSpeakerPage;
import jelatyna.pages.confitura.sponsor.ViewSponsorsPage;
import jelatyna.pages.confitura.volunteer.ListVolunteerPage;
import jelatyna.repositories.MenuLinkItemRepository;
import jelatyna.repositories.SimpleContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.google.common.collect.Iterables.*;
import static com.google.common.collect.Lists.*;

@Component
public class MenuLinks {
    @Autowired
    private SimpleContentRepository repository;
    @Autowired
    private MenuLinkItemRepository menuLinkItemRepository;

    private List<MenuLink> links = newArrayList();

    @PostConstruct
    public void initLinks() {
        links.clear();
        add(new EmptyMenuLink());
        add(new SingleMenuLink(menuLinkItemRepository, "Kapitula", ViewAdminPage.class));
        add(new SingleMenuLink(menuLinkItemRepository, "Aktualnosci", ListNewsPage.class));
        add(new SingleMenuLink(menuLinkItemRepository, "C4P", ViewSpeakerPage.class));
        add(new SingleMenuLink(menuLinkItemRepository, "Sponsorzy", ViewSponsorsPage.class));
        add(new SingleMenuLink(menuLinkItemRepository, "Rejestracja", RegistrationPage.class));
        add(new SingleMenuLink(menuLinkItemRepository, "Prelegenci", ListSpeakerPage.class));
        add(new SingleMenuLink(menuLinkItemRepository, "Prezentacje", ListPresentationPage.class));
        add(new SingleMenuLink(menuLinkItemRepository, "Wolontariusze", ListVolunteerPage.class));
        add(new DynamicMenuLink<>(menuLinkItemRepository, repository, ViewSimpleContentPage.class));
    }

    MenuLink getMenuLinkFor(MenuLinkItem menuLinkItem) {
        return links
                .stream()
                .filter(link -> link.containsLink(menuLinkItem.getTitle()))
                .findFirst()
                .get();
    }

    public void add(MenuLink menuLink) {
        links.add(menuLink);
    }

    public List<MenuLinkItem> getAllItems() {
        return links.stream()
                .map(MenuLink::getAllItems)
                .reduce((x, y) -> newArrayList(concat(x, y)))
                .get();

    }

    public MenuLink getMenuLinkFor(MenuItem menuItem) {
        if (menuItem != null && menuItem.getLinkItem() != null) {
            return getMenuLinkFor(menuItem.getLinkItem());
        }
        return null;
    }

}
