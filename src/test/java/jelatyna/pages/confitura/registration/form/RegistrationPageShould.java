package jelatyna.pages.confitura.registration.form;

import jelatyna.WicketTest;
import jelatyna.domain.Participant;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.confitura.registration.Registration;
import jelatyna.pages.confitura.registration.RegistrationInfoPage;
import jelatyna.services.FileService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;

import static com.google.common.collect.Lists.*;
import static jelatyna.pages.confitura.registration.form.RegistrationForm.*;
import static jelatyna.pages.confitura.registration.form.RegistrationPage.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationPageShould extends WicketTest {
    @Mock
    private Registration registration;
    @Mock
    private RegistrationOptions options;

    @Override
    public void setupServices() {
        optionsWith(position(), experiance(), shirtSize(), info());
        put(registration, options, mock(SpeakerService.class), mock(FileService.class));
    }

    @Test
    public void startPageForClosedRegistration() {
        // given
        String closeInfo = "Rejestracja zamknięta";
        when(registration.isActive()).thenReturn(false);
        when(registration.getCloseInfo()).thenReturn(closeInfo);

        // when
        tester.startPage(RegistrationPage.class);

        // then
        tester.assertLabel(CLOSE_INFO_ID, closeInfo);
        tester.assertInvisible("form");
    }

    @Test
    public void registerParticipant() {
        activeRegistration();
        tester.startPage(RegistrationPage.class);

        fillForm().submit();

        ArgumentCaptor<Participant> captor = ArgumentCaptor.forClass(Participant.class);
        verify(registration).register(captor.capture());
        Participant participant = captor.getValue();
        assertThat(participant.getFirstName()).isEqualTo("Jan");
        assertThat(participant.getLastName()).isEqualTo("Kowalski");
        assertThat(participant.getMail()).isEqualTo("Jan@Kowalski.pl");
        assertThat(participant.getCity()).isEqualTo("Warszawa");
        assertThat(participant.getSex()).isEqualTo("Mężczyzna");
        assertThat(participant.getSize()).isEqualTo(shirtSize());
        assertThat(participant.getInfo()).isEqualTo(info());
        assertThat(participant.getPosition()).isEqualTo(position());
        assertThat(participant.getExperience()).isEqualTo(experiance());
        tester.assertRenderedPage(RegistrationInfoPage.class);

    }

    @Test
    public void shouldNotAllowInvalidRepeatedEmail() {
        activeRegistration();
        tester.startPage(RegistrationPage.class);

        FormTester registrationForm = fillForm();
        registrationForm.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.MAIL_REPEATED_FIELD_ID, "Jan@Kowalskiii.pl");

        registrationForm.submit();

        tester.assertRenderedPage(RegistrationPage.class);
        tester.assertErrorMessages("'Jan@Kowalski.pl' z e-mail i 'Jan@Kowalskiii.pl' z Powtórz e-mail muszą być równe.");
    }


    @Test
    public void showErrorWhenExceptionOnRegistering() {
        String message = "error message";
        activeRegistration();
        doThrow(new RuntimeException(message)).when(registration).register(anyParticipant());
        tester.startPage(RegistrationPage.class);

        fillForm().submit();

        tester.assertRenderedPage(RegistrationPage.class);
        tester.assertErrorMessages(message);
    }

    @Test
    public void hidePresentationDescriptionsOnStart() {
        // given
        activeRegistration();
        createPresentation();

        // when
        tester.startPage(RegistrationPage.class);

        // then
        tester.assertInvisible(formId(PERSONAL_AGENDA_FIELD_ID, "0", DESCRIPTION_ROW_ID, DESCRIPTION_FIELD_ID));
    }

    @Test
    public void showPresentationDescriptionsOnSubjectClick() {
        // given
        activeRegistration();
        createPresentation();

        // when
        tester.startPage(RegistrationPage.class);
        tester.clickLink(formId(PERSONAL_AGENDA_FIELD_ID, "0", SPEAKER_TITLE_LINK_ID));

        // then
        tester.assertVisible(formId(PERSONAL_AGENDA_FIELD_ID, "0", DESCRIPTION_ROW_ID, DESCRIPTION_FIELD_ID));
    }

    private OngoingStubbing<Boolean> activeRegistration() {
        return when(registration.isActive()).thenReturn(true);
    }

    private void createPresentation() {
        Speaker speaker = new Speaker().firstName("Jan").lastName("Kowalski");
        Presentation presentation = new Presentation().title("title").description("description").owner(speaker);
        when(registration.allPresentations()).thenReturn(newArrayList(presentation));
    }

    private String formId(String... elements) {
        String id = "form";
        for (String element : elements) {
            id += ":" + element;
        }
        return id;
    }

    private FormTester fillForm() {
        FormTester formTester = tester.newFormTester("form");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.FIRST_NAME_FIELD_ID, "Jan");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.LAST_NAME_FIELD_ID, "Kowalski");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.MAIL_FIELD_ID, "Jan@Kowalski.pl");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.MAIL_REPEATED_FIELD_ID, "Jan@Kowalski.pl");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.CITY_FIELD_ID, "Warszawa");
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.SEX_FIELD_ID, 1);
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.INFO_FIELD_ID, 0);
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.POSITION_FIELD_ID, 0);
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.EXPERIENCE_FIELD_ID, 0);
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.SIZE_FIELD_ID, 0);
        return formTester;
    }

    private Participant anyParticipant() {
        return any(Participant.class);
    }

    private void optionsWith(String position, String experiance, String size, String info) {
        when(options.getPosition()).thenReturn(newArrayList(position));
        when(options.getExperiance()).thenReturn(newArrayList(experiance));
        when(options.getShirtSize()).thenReturn(newArrayList(size));
        when(options.getInfo()).thenReturn(newArrayList(info));
    }

    private String info() {
        return "WJUG";
    }

    private String shirtSize() {
        return "S";
    }

    private String experiance() {
        return "brak";
    }

    private String position() {
        return "student";
    }
}
