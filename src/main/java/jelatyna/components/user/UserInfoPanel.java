package jelatyna.components.user;

import jelatyna.components.StaticImage;
import jelatyna.components.nogeneric.Link;
import jelatyna.domain.User;
import jelatyna.services.FileService;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
public class UserInfoPanel<USER extends User<?>> extends Panel {
    @SpringBean
    private FileService fileService;

    public UserInfoPanel(String id, USER user) {
        super(id);
        add(new AttributeModifier("id", model(user.getFullName())));
        add(label("first_name", user.getFirstName()).setRenderBodyOnly(true));
        add(label("last_name", user.getLastName()));
        add(richLabel("bio", user.getBio()));
        Link link = linkToBigPhoto(user);
        link.add(new StaticImage("photo", fileService.getUrlTo(user)));
        add(link);
    }

    private Link linkToBigPhoto(USER user) {
        Link link = new Link("big_photo") {

            @Override
            public void onClick() {
            }
        };
        link.add(new AttributeModifier("href", fileService.getUrlTo(user)));
        return link;
    }

}
