package jelatyna.domain.dto.converters;


import jelatyna.domain.Vote;
import jelatyna.domain.dto.VoteDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VoteConverter {

    @Autowired
    private SpeakerConverter speakerConverter;

    @Autowired
    private PresentationConverter presentationConverter;

    public VoteDto map(Vote v) {
        VoteDto vote = new VoteDto();
        vote.setVote(v.getRate());
        vote.setPresentation(presentationConverter.map(v.getPresentation()));

        return vote;
    }

}
