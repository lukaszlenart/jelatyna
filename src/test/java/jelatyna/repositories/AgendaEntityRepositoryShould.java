package jelatyna.repositories;

import jelatyna.IntegrationTest;
import jelatyna.domain.Agenda;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class AgendaEntityRepositoryShould extends IntegrationTest {
    @Autowired
    private AgendaEntityRepository repository;

    @Test
    public void saveAndFindAgenda() {
        //given
        assertNull(repository.findOne(AgendaEntity.AGENDA_KEY));
        Agenda agenda = new Agenda();

        //when
        repository.save(new AgendaEntity(agenda));

        //then
        AgendaEntity foundAgendaEntry = repository.findOne(AgendaEntity.AGENDA_KEY);
        assertNotNull(foundAgendaEntry);
        Agenda foundAgenda = foundAgendaEntry.getAgenda();
        assertEquals(agenda, foundAgenda);
    }
}
