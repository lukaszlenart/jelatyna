package jelatyna.assertions;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.TagTester;
import org.apache.wicket.util.tester.WicketTester;

public class WicketAssertions {

    public static TagAssert assertThat(TagTester tester) {
        return new TagAssert(tester);
    }

    public static FormAssert assertThat(FormTester formTester) {
        return new FormAssert(formTester);
    }

    public static ComponentAssert assertThat(Component component) {
        return new ComponentAssert(component);
    }

    public static FormTester form(WicketTester tester, String path) {
        return tester.newFormTester(path);
    }

    public static TagTester tag(WicketTester tester, String wicketId) {
        return tester.getTagByWicketId(wicketId);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Component> T component(WicketTester tester, String path) {
        return ((T) tester.getComponentFromLastRenderedPage(path));
    }

    public static Label label(WicketTester tester, String path) {
        return component(tester, path);
    }
}
