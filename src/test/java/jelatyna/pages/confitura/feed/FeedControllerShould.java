package jelatyna.pages.confitura.feed;

import jelatyna.domain.News;
import jelatyna.services.NewsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FeedControllerShould {
    private Date FORMER_ENTRY_CREATION_DATE = new Date(1);
    private Date LATTER_ENTRY_CREATION_DATE = new Date(2);
    private List<News> newsList = newArrayList(news("title2", LATTER_ENTRY_CREATION_DATE),
            news("title1", FORMER_ENTRY_CREATION_DATE));
    @Mock
    private NewsService service;

    @InjectMocks
    private FeedController feedController;

    @Test
    public void returnViewWithNews() {
        // given
        given(service.fetchPublished()).willReturn(newsList);

        // when
        ModelAndView modelAndView = feedController.feed();

        // then
        assertThat(modelAndView.getModel()).contains(entry(FeedController.NEWS_VIEW_KEY, newsList));
    }

    @Test
    public void returnViewWithLastUpdateTime() {
        // given
        given(service.fetchPublished()).willReturn(newsList);

        // when
        ModelAndView modelAndView = feedController.feed();

        // then
        assertThat(modelAndView.getModel()).contains(entry(FeedController.LAST_UPDATE_VIEW_KEY, LATTER_ENTRY_CREATION_DATE));
    }

    @Test
    public void returnTheBeginningOfTimeAsLastUpdateInViewWhenListIsEmpty() {
        // given
        given(service.fetchPublished()).willReturn(new ArrayList<News>());

        // when
        ModelAndView modelAndView = feedController.feed();

        // then
        assertThat(modelAndView.getModel()).contains(entry(FeedController.LAST_UPDATE_VIEW_KEY, new Date(0)));
    }

    private News news(String title, Date date) {
        return new News().title(title).creationDate(date);
    }
}
