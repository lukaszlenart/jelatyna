package jelatyna.pages.confitura.volunteer;

import jelatyna.components.user.UserInfoPanel;
import jelatyna.domain.Volunteer;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.services.FileService;
import jelatyna.services.VolunteerService;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
public class ListVolunteerPage extends BaseWebPage {
    @SpringBean
    private VolunteerService service;
    @SpringBean
    private FileService fileService;

    public ListVolunteerPage() {
        List<Volunteer> volunteers = service.findAll();
        Collections.shuffle(volunteers);
        add(new VolunteerList("volunteers", volunteers));
        setPageTitlePostfix("Wolontariusze");
    }

    private final class VolunteerList extends ListView<Volunteer> {
        private VolunteerList(String id, List<Volunteer> list) {
            super(id, list);
        }

        @Override
        protected void populateItem(ListItem<Volunteer> item) {
            Volunteer volunteer = item.getModelObject();
            item.add(new UserInfoPanel<>("volunteer", volunteer));
            addImageForFacebook(fileService.getUrlTo(volunteer));
        }
    }
}
