package jelatyna.services;

import com.googlecode.catchexception.apis.CatchExceptionBdd;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.domain.Vote;
import jelatyna.pages.confitura.c4p.SpeakerMailSender;
import jelatyna.pages.confitura.c4p.login.RequestPasswordResetMailSender;
import jelatyna.repositories.PresentationRepository;
import jelatyna.repositories.SpeakerRepository;
import jelatyna.repositories.UserRepository;
import jelatyna.repositories.VoteRepository;
import jelatyna.services.exceptions.LoginException;
import jelatyna.services.exceptions.UserDoesNotExistException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static com.googlecode.catchexception.CatchException.*;
import static java.lang.Boolean.*;
import static jelatyna.services.SpeakerServiceImpl.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SpeakerServiceImplShould extends UserServiceImplShould<Speaker> {
    private static final Integer SPEAKER_ID = 1;
    @Mock
    private RandomGenerator randomGenerator;
    @Mock
    private SpeakerRepository repository;
    @Mock
    private VoteRepository voteRepository;
    @Mock
    private PresentationRepository presentationRepository;
    @Mock
    private SpeakerMailSender mailSender;
    @Mock
    private RequestPasswordResetMailSender passwordMailSender;
    @Mock
    private Speaker speaker;
    @InjectMocks
    private SpeakerServiceImpl service;

    @Test
    public void sendEMailAfterSavingNewSpeaker() {
        when(speaker.isNew()).thenReturn(true);

        service.save(speaker, fileUpload);

        InOrder inOrder = inOrder(mailSender, repository);
        inOrder.verify(repository).save(speaker);
        inOrder.verify(mailSender).sendMessage(speaker);
    }

    @Override
    @Test
    public void saveOnlyIfSavingExistingUser() {
        when(speaker.isNew()).thenReturn(false);

        service.save(speaker, fileUpload);

        verify(repository).save(speaker);
        verify(repository, never()).findByMail(anyString());
        verify(mailSender, never()).sendMessage(any(Speaker.class));
        verify(encryptor, never()).encryptPassword(anyString());
    }

    @Test
    public void askUserIfOldPasswordIsCorrect() {
        when(encryptor.checkPassword(anyString(), anyString())).thenReturn(true);
        String oldPassword = "oldPassword";

        service.changePassword(speaker, oldPassword, "");

        verify(repository).save(speaker);
    }

    @Test
    public void throwExceptionIfOldPasswordNotCorrectOnPasswordChange() {
        when(encryptor.checkPassword(anyString(), anyString())).thenReturn(false);

        CatchExceptionBdd.when(getService()).changePassword(speaker, "wrongPassword", "newPassword");

        CatchExceptionBdd.then(caughtException())
                .isInstanceOf(LoginException.class)
                .hasMessage(SpeakerServiceImpl.WRONG_OLD_PASSWORD);
        verify(repository, never()).save(speaker);
    }

    @Test
    public void deletePresentation() {

        service.delete(new Presentation().id(1));

        verify(presentationRepository).delete(1);
    }

    @Test
    public void findAllPresentations() {
        List<Presentation> presentations = newArrayList(new Presentation().title("title"));
        when(presentationRepository.findAll()).thenReturn(presentations);

        List<Presentation> fetchedPresentations = service.allPresentations();

        assertThat(fetchedPresentations).isSameAs(presentations);
    }

    @Test
    public void togglePresentationAcceptanceState() {
        Presentation presentation = mock(Presentation.class);

        service.toggleAcceptance(presentation);

        InOrder inOrder = inOrder(presentation, presentationRepository);
        inOrder.verify(presentation).toggleAccepted();
        inOrder.verify(presentationRepository).save(presentation);

    }

    @Test
    public void findSpeakerByToken() {
        String token = "123";
        when(repository.findByToken(anyString())).thenReturn(speaker);

        Speaker foundSpeaker = service.findByToken(token);

        assertThat(foundSpeaker).isSameAs(speaker);
        verify(repository).findByToken(token);
    }

    @Test
    public void clearTokenAndSetNewPasswordOnResettingPassword() {
        System.out.println(encryptor.encryptPassword("abc"));
        String newPassword = "new password";

        service.resetPasswordFor(speaker, newPassword);

        InOrder inOrder = inOrder(speaker, repository);
        inOrder.verify(speaker).token("");
        inOrder.verify(repository).save(speaker);
    }

    @Test
    public void thowExceptionIfWrongMailForResettingPassword() {
        when(repository.findByMail(anyString())).thenReturn(null);

        CatchExceptionBdd.when(service).requestPasswordResetBy("my@mail.com");

        CatchExceptionBdd.then(caughtException())
                .isInstanceOf(UserDoesNotExistException.class)
                .hasMessage(INVALID_MAIL);
    }

    @Test
    public void saveSpeakerWithTokenAndSendMailIfMailIsValidOnResettingMail() {
        String token = "123";
        when(repository.findByMail(anyString())).thenReturn(speaker);
        when(randomGenerator.getRandomId()).thenReturn(token);

        service.requestPasswordResetBy("my@mail.com");

        InOrder inOrder = inOrder(speaker, repository, passwordMailSender);
        inOrder.verify(speaker).token(token);
        inOrder.verify(repository).save(speaker);
        inOrder.verify(passwordMailSender).sendMessage(speaker);

    }

    @Test
    public void saveNewVote() {
        Vote vote = new Vote("1.1.1.1", new Presentation().id(1), 1);

        service.save(vote);

        verify(voteRepository).save(vote);
    }

    @Test
    public void updateVoteIfExistsForIpAndPresentation() {
        String ip = "1.1.1.1";
        Presentation presentation = new Presentation().id(1);
        Vote newVote = new Vote(ip, presentation, 1);
        Vote oldVote = new Vote(ip, presentation, 2).id(10);
        when(voteRepository.findByIpAndPresentation(anyString(), anyPresentation())).thenReturn(oldVote);

        service.save(newVote);

        verify(voteRepository).findByIpAndPresentation(ip, presentation);
        verify(voteRepository).save(new Vote(ip, presentation, 1).id(10));
    }

    @Test
    public void countNumberOfVotesForPresentation() {
        Presentation presentation = new Presentation();
        when(voteRepository.findByPresentation(anyPresentation())).thenReturn(newArrayList(new Vote(), new Vote()));

        int count = service.countVotesFor(presentation);

        assertThat(count).isEqualTo(2);
        verify(voteRepository).findByPresentation(presentation);
    }

    @Test
    public void calculateAverageRateForPresentationWithOneVote() {
        Presentation presentation = new Presentation();
        when(voteRepository.findByPresentation(anyPresentation()))
                .thenReturn(newArrayList(vote(presentation, 1)));

        double average = service.averageRateFor(presentation);

        assertThat(average).isEqualTo(1);
    }

    @Test
    public void calculateAverageRateForPresentationWithTwoVotes() {
        Presentation presentation = new Presentation();
        when(voteRepository.findByPresentation(anyPresentation()))
                .thenReturn(newArrayList(vote(presentation, 1), vote(presentation, 4)));

        double average = service.averageRateFor(presentation);

        assertThat(average).isEqualTo(2.5);
    }

    private Vote vote(Presentation presentation, int rate) {
        return new Vote("", presentation, rate);
    }

    @Test
    public void calculateAverageRateForPresentationWithNoVotes() {
        Presentation presentation = new Presentation();
        when(voteRepository.findByPresentation(anyPresentation())).thenReturn(new ArrayList<Vote>());

        double average = service.averageRateFor(presentation);

        assertThat(average).isEqualTo(0);
    }

    @Test
    public void roundAverageRateToTwoDecimalNumbers() {
        Presentation presentation = new Presentation();
        when(voteRepository.findByPresentation(anyPresentation())).thenReturn(
                newArrayList(vote(presentation, 1), vote(presentation, 3), vote(presentation, 3)));

        double average = service.averageRateFor(presentation);

        assertThat(average).isEqualTo(2.33);
    }

    @Test
    public void fetchShuffeledPresentations() {
        List<Presentation> presentations = newArrayList(new Presentation().id(1));
        List<Presentation> shuffledPresentations = newArrayList(new Presentation().id(2));
        when(presentationRepository.findAll()).thenReturn(presentations);
        when(randomGenerator.shuffle(presentations)).thenReturn(shuffledPresentations);

        List<Presentation> fetchedPresentations = service.allShuffledPresentations();

        assertThat(fetchedPresentations).isSameAs(shuffledPresentations);
    }

    @Test
    public void getRandomSpeaker() {
        List<Speaker> speakers = newArrayList(speaker(1), speaker(2));
        when(randomGenerator.getRandomIdx(anyListOf(Object.class))).thenReturn(1);
        when(repository.allAccepted()).thenReturn(speakers);

        Speaker randomSpeaker = service.randomSpeaker();

        assertThat(randomSpeaker).isSameAs(speakers.get(1));
    }

    @Test
    public void getEmptySpeakerAsRandomIfNoAccepted() {
        when(randomGenerator.getRandomIdx(anyListOf(Object.class))).thenReturn(1);
        when(repository.allAccepted()).thenReturn(new ArrayList<Speaker>());

        Speaker randomSpeaker = service.randomSpeaker();

        assertThat(randomSpeaker).isSameAs(Speaker.EMPTY);
    }

    @Test
    public void shouldRefreshSpeakerAcceptanceStatusOnRemovingAcceptanceFromPresentation() {
        Presentation presentation = acceptedPresentationWithAcceptedSpeaker();
        Speaker speaker = presentation.getOwner();
        when(repository.findOne(SPEAKER_ID)).thenReturn(speaker);

        service.toggleAcceptance(presentation);

        assertThat(speaker.isAccepted()).isFalse();
        verify(repository).save(speaker);
    }

    @Test
    public void shouldRefreshSpeakerAcceptanceStatusOnRemovingLastAcceptedPresentation() {
        Presentation presentation = acceptedPresentationWithAcceptedSpeaker();
        Speaker refreshedSpeaker = new Speaker(SPEAKER_ID).accepted(TRUE);
        when(repository.findOne(SPEAKER_ID)).thenReturn(refreshedSpeaker);

        service.delete(presentation);

        assertThat(refreshedSpeaker.isAccepted()).isFalse();
        verify(repository).save(refreshedSpeaker);
    }

    public void shouldNotChangeSpeakerAcceptanceWithAnyAcceptedPresentation() {
        Presentation presentation = acceptedPresentationWithAcceptedSpeaker();
        Speaker speaker = presentation.getOwner();
        speaker.addPresentation(new Presentation().accepted(TRUE));
        when(repository.findOne(SPEAKER_ID)).thenReturn(speaker);

        service.toggleAcceptance(presentation);

        assertThat(speaker.isAccepted()).isTrue();
        verify(repository, never()).save(speaker);
    }

    private Speaker speaker(int id) {
        return new Speaker(id);
    }

    private Presentation acceptedPresentationWithAcceptedSpeaker() {
        Speaker speaker = new Speaker().id(SPEAKER_ID).accepted(TRUE);
        Presentation presentation = new Presentation().accepted(TRUE).owner(speaker);
        speaker.addPresentation(presentation);
        return presentation;
    }

    private Presentation anyPresentation() {
        return any(Presentation.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <REPO extends UserRepository<Speaker>> REPO getRepository() {
        return (REPO) repository;
    }

    @Override
    protected Speaker createUser() {
        return new Speaker();
    }

    @Override
    protected UserService<Speaker> getService() {
        return service;
    }
}
