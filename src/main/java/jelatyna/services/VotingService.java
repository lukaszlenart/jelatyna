package jelatyna.services;


import jelatyna.domain.Vote;

import java.util.List;

public interface VotingService {
    List<Vote> prepareVotesForKey(String key);

    void save(Vote vote);

    List<Vote> getVotes(String key);

    Vote getVote(String key, Integer presentationId);
}
