package jelatyna.components.menu;

import jelatyna.domain.MenuLinkItem;
import org.apache.wicket.Page;

import java.util.List;

import static com.google.common.collect.Lists.*;

public class EmptyMenuLink implements MenuLink {

    @Override
    public List<MenuLinkItem> getAllItems() {
        return newArrayList(new MenuLinkItem(null, ""));
    }

    @Override
    public Class<? extends Page> getPageClass() {
        return null;
    }

    @Override
    public boolean containsLink(String link) {
        return "".equals(link);
    }

    @Override
    public boolean isLink() {
        return false;
    }

}
