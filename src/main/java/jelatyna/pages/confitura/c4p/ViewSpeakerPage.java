package jelatyna.pages.confitura.c4p;

import jelatyna.components.user.speaker.SpeakerPanel;
import jelatyna.domain.Speaker;
import jelatyna.services.SpeakerService;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class ViewSpeakerPage extends SpeakerBasePage {

    @SpringBean
    private SpeakerService service;

    public ViewSpeakerPage() {
        Speaker speaker = getSession().setUser(fetchSpeaker());
        add(new SpeakerPanel(speaker, service, true));
        setPageTitlePostfix("Prelegent " + speaker.getFullName());
    }

    private Speaker fetchSpeaker() {
        Speaker user = getSession().getUser();
        if (user.getId() != null) {
            return service.findById(user.getId());
        } else {
            return user;
        }
    }

}
