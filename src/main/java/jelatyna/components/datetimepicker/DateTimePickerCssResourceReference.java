package jelatyna.components.datetimepicker;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.request.resource.CssResourceReference;

import java.util.Arrays;

/**
 * SINGLETON WITH DEFINITION OF MAIN CSS
 */
public class DateTimePickerCssResourceReference extends CssResourceReference {

    public static final String MAIN_CSS_FILENAME = "datetimepicker.css";

    private static DateTimePickerCssResourceReference INSTANCE = null;

    private DateTimePickerCssResourceReference() {
        super(DateTimePickerCssResourceReference.class, MAIN_CSS_FILENAME);
    }

    public static DateTimePickerCssResourceReference get() {
        if (null == INSTANCE) {
            INSTANCE = new DateTimePickerCssResourceReference();
        }
        return INSTANCE;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public Iterable<? extends HeaderItem> getDependencies() {
        return Arrays.asList();
    }
}
