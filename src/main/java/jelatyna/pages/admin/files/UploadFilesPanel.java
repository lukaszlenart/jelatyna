package jelatyna.pages.admin.files;

import jelatyna.services.FileService;
import org.apache.wicket.extensions.ajax.markup.html.form.upload.UploadProgressBar;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.MultiFileUploadField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.lang.Bytes;

import java.util.List;

import static com.google.common.collect.Lists.*;

@SuppressWarnings("serial")
public class UploadFilesPanel extends Panel {
    @SpringBean
    private FileService service;

    public UploadFilesPanel(String id, FilesPanel filesPanel) {
        super(id);
        add(new FileUploadForm("form", filesPanel));
    }

    private class FileUploadForm extends Form<Void> {

        private List<FileUpload> images = newArrayList();
        private FilesPanel filesPanel;

        public FileUploadForm(String id, FilesPanel filesPanel) {
            super(id);
            this.filesPanel = filesPanel;
            add(new MultiFileUploadField("file_input", new PropertyModel<List<FileUpload>>(this, "images"), 10));
            add(new UploadProgressBar("upload_progress", this));
            setMultiPart(true);
            setMaxSize(Bytes.kilobytes(10000));
        }

        @Override
        protected void onSubmit() {
            for (FileUpload image : images) {
                service.save(image, filesPanel.getFolderPath());
            }
        }
    }

}
