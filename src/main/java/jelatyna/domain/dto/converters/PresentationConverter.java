package jelatyna.domain.dto.converters;

import static java.util.stream.Collectors.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.domain.Presentation;
import jelatyna.domain.dto.PresentationDto;
import jelatyna.domain.dto.UserDto;

@Component
public class PresentationConverter {

    @Autowired
    private UserConverter userConverter;

    public PresentationDto map(Presentation presentation) {
        PresentationDto presentationDto = new PresentationDto();
        presentationDto.setId(presentation.getId());
        presentationDto.setLongDesc(presentation.getDescription());
        presentationDto.setShortDesc(presentation.getShortDescription());
        List<UserDto> speakers = presentation.getSpeakers().stream().map(userConverter::map).collect(toList());
        presentationDto.setSpeakers(speakers);
        presentationDto.setTitle(presentation.getTitle());
        return presentationDto;
    }

    public List<PresentationDto> mapAll(List<Presentation> presentations) {
        return presentations
                .stream().map((p) -> {
                    return map(p);
                }).collect(toList());
    }

}
