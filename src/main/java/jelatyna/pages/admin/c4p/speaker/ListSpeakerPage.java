package jelatyna.pages.admin.c4p.speaker;

import static jelatyna.utils.Components.*;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.components.DeleteEntityLink;
import jelatyna.components.LabeledLink;
import jelatyna.components.RedirectLink;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.Speaker;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.repositories.SpeakerRepository;

@SuppressWarnings("serial")
public class ListSpeakerPage extends AdminBasePage {

    @SpringBean
    private SpeakerRepository repository;

    public ListSpeakerPage() {
        add(new RedirectLink("add", EditSpeakerPage.class));
        add(new SpeakerGrid(repository.findAll()));
        add(new ConfituraFeedbackPanel("feedback"));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return "Prelegenci";
    }

    private final class SpeakerGrid extends DataView<Speaker> {

        private SpeakerGrid(List<Speaker> list) {
            super("rows", new ListDataProvider<Speaker>(list));
        }

        @Override
        protected void populateItem(Item<Speaker> item) {
            final Speaker speaker = item.getModelObject();
            item.add(label("idx", item.getIndex() + 1));
            item.add(label("first_name", speaker.getFirstName()));
            item.add(label("last_name", speaker.getLastName()));
            item.add(label("mail", speaker.getMail()));
            item.add(label("presentations_count", getInfoAboutPresentation(speaker)));

            item.add(yesNoIcon("accepted", speaker.isAccepted()));
            item.add(new AcceptSpeakerLink("accept_speaker", speaker));
            item.add(new RedirectLink("edit", EditSpeakerPage.class, speaker));
            item.add(new RedirectLink("view", ViewSpeakerPage.class, speaker));
            item.add(new DeleteSpeakerLink(speaker));
        }

        private String getInfoAboutPresentation(Speaker speaker) {
            return getAllPresentationCount(speaker) + "/" + getAcceptedPresentationCount(speaker);
        }

        private long getAcceptedPresentationCount(Speaker speaker) {
            return speaker.getAcceptedPresentations().size();
        }

        private int getAllPresentationCount(Speaker speaker) {
            return speaker.getPresentations().size();
        }
    }

    private class DeleteSpeakerLink extends DeleteEntityLink {

        private final Speaker speaker;

        public DeleteSpeakerLink(Speaker speaker) {
            super(speaker, ListSpeakerPage.class);
            this.speaker = speaker;
        }

        @Override
        public void onClick() {
            if (!speaker.anyPresentationAccepted()) {
                super.onClick();
            } else {
                error("Nie możesz usunąć tego prelegenta, gdyż ma zaakceptowana prezentację.");
            }
        }

        @Override
        protected void deleteById(Integer id) {
            repository.delete(id);
        }
    }

    private final class AcceptSpeakerLink extends LabeledLink {

        private final Speaker speaker;

        public AcceptSpeakerLink(String id, Speaker speaker) {
            super(id, null);
            this.speaker = speaker;
        }

        @Override
        protected void onConfigure() {
            if (!speaker.anyPresentationAccepted()) {
                add(AttributeModifier.append("class", "disabled"));
                add(AttributeModifier.append("title", "Prelegent nie ma żadnej zaakceptowanej prezentacji"));
                setEnabled(Boolean.FALSE);
            } else {
                if (!speaker.isProfileFilled() && !speaker.isAccepted()) {
                    add(AttributeModifier.replace("onclick", "return confirm('" + new ResourceModel("speaker.confirm.acceptance.no.filled.profile").getObject() + "');"));
                }
            }
        }

        @Override
        public void onClick() {
            speaker.toggleAcceptance();
            repository.save(speaker);
        }

        @Override
        public String getLabel() {
            return (speaker.isAccepted()) ? "Odrzuć akceptację" : "Zaakceptuj";
        }
    }
}
