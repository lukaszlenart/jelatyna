package jelatyna.pages.admin.c4p.speaker;

import jelatyna.ConfituraSession;
import jelatyna.WicketTest;
import jelatyna.domain.Admin;
import jelatyna.domain.Speaker;
import jelatyna.services.SpeakerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static jelatyna.assertions.WicketAssertions.*;
import static jelatyna.utils.PageParametersBuilder.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EditSpeakerPageShould extends WicketTest {
    @Mock
    private SpeakerService service;

    @Override
    public void setupServices() {
        ConfituraSession.get().setUser(new Admin());
        put(service);
    }

    @Test
    public void editExistingSpeaker() {
        when(service.findById(1)).thenReturn(new Speaker().firstName("John"));

        tester.startPage(EditSpeakerPage.class, paramsFor("id", 1));

        assertThat(form(tester, "userForm:form"))
                .hasValue("firstName", "John");
    }

    @Test
    public void createNewSpeaker() {

        tester.startPage(EditSpeakerPage.class);

        assertThat(form(tester, "userForm:form"))
                .hasValue("firstName", "");
    }

}
