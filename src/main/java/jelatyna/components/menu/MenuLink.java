package jelatyna.components.menu;

import jelatyna.domain.MenuLinkItem;
import org.apache.wicket.Page;

import java.util.List;

public interface MenuLink {

    List<MenuLinkItem> getAllItems();

    Class<? extends Page> getPageClass();

    boolean containsLink(String link);

    boolean isLink();
}
