package jelatyna.repositories;


import jelatyna.domain.SimpleContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SimpleContentRepository extends JpaRepository<SimpleContent, Integer> {
    @Modifying
    @Query("DELETE FROM SimpleContent WHERE id = ?")
    public void deleteById(Integer id);

    public SimpleContent findByTitle(String title);

}
