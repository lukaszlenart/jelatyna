package jelatyna.components;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.model.Model;

@SuppressWarnings("serial")
public class StaticImage extends WebComponent {

    public StaticImage(String id, String path) {
        super(id);
        add(new AttributeModifier("src", Model.of(path)));
    }

    public StaticImage(String id, String path, String alt) {
        this(id, path);
        add(new AttributeModifier("alt", Model.of(alt)));
    }

    @Override
    protected void onComponentTag(ComponentTag tag) {
        super.onComponentTag(tag);
        checkComponentTag(tag, "img");
    }

}