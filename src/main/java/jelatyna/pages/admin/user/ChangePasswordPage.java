package jelatyna.pages.admin.user;

import jelatyna.components.user.ChangePasswordPanel;
import jelatyna.domain.Admin;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.AdminService;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class ChangePasswordPage extends AdminBasePage {

    @SpringBean
    private AdminService service;

    public ChangePasswordPage(PageParameters params) {
        add(new ChangePasswordPanel<Admin>(getAdmin(params), service, ListAdminPage.class));
    }

    private Admin getAdmin(PageParameters params) {
        return service.findById(params.get("id").toInt());
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.ORGANIZERS;
    }

    @Override
    public String getPageTitle() {
        return "Zmień Hasło";
    }

}
