package jelatyna.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.*;
import static com.google.common.collect.Sets.*;

@SuppressWarnings("serial")
public class TimeSlot implements Serializable {

    private final String time;
    private List<AgendaItem> items = newArrayList();

    public TimeSlot(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public List<AgendaItem> getItems() {
        Set<AgendaItem> valuesAsSet = newHashSet(items);
        if (valuesAsSet.size() == 1) {
            return newArrayList(valuesAsSet);
        } else {
            return items;
        }
    }

    public TimeSlot addItem(AgendaItem item) {
        items.add(item);
        return this;
    }

    public int getSpan() {
        if (getItems().size() == 1) {
            return items.size();
        } else {
            return 1;
        }
    }

}
