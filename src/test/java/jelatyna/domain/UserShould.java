package jelatyna.domain;


import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class UserShould {

    private User<?> user = new TestUser();
    private String plainPassword = "passek";


//    @Test
//    public void passworBedWrong() {
//        user.password(plainPassword);
//        user.encryptPassword();
//        user.password(plainPassword + plainPassword);
//        user.encryptPassword();
//
//        boolean result = user.isPasswordCorrect(plainPassword);
//
//        assertThat(result).isFalse();
//    }
//
//    @Test(expected = Exception.class)
//    public void throwExceptionOnComparingPlainPassword() {
//        user.password(plainPassword);
//        user.encryptPassword();
//        user.password(plainPassword);
//
//        user.isPasswordCorrect(plainPassword);
//
//    }

    @Test
    public void returnFullName() {
        user.firstName("Stefek").lastName("Burczymucha");

        String fullName = user.getFullName();

        assertThat(fullName).isEqualTo("Stefek Burczymucha");
    }

    @Test
    public void getPlainWebPageIfSet() {
        String url = "www.jaja.pl";
        user.webPage(url);

        String result = user.getWebPage();

        assertThat(result).isEqualTo(url);
    }

    @Test
    public void getSetTwitterUrl() {
        String twitterUrl = "www.twitter.com/jajko";
        user.twitter(twitterUrl);

        String url = user.getTwitter();

        assertThat(url).isEqualTo(twitterUrl);
    }

    @SuppressWarnings("serial")
    private class TestUser extends User<TestUser> {

        @Override
        public String getSubfolder() {
            return "subfolder";
        }

        @Override
        public String getFileName() {
            return "file";
        }

    }
}
