package jelatyna.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jelatyna.domain.Presentation;
import jelatyna.domain.Vote;
import jelatyna.repositories.PresentationRepository;
import jelatyna.repositories.VoteRepository;

@Service
public class VotingServiceImpl implements VotingService {

    @Autowired
    private SpeakerService service;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private PresentationRepository presentationRepository;



    @Override
    public List<Vote> prepareVotesForKey(String key) {
        List<Presentation> presentations = service.allShuffledPresentations();
        List<Vote> votes = new ArrayList<>();
        for (int i = 0; i < presentations.size(); i++) {
            Vote vote = new Vote(key, presentations.get(i), null, i);
            votes.add(vote);
            save(vote);
        }
        return votes;

    }

    @Override
    public void save(Vote vote) {
        service.save(vote);
    }

    @Override
    public List<Vote> getVotes(String key) {
        return voteRepository.findByIp(key);
    }

    @Override
    public Vote getVote(String key, Integer presentationId) {
        return voteRepository.findByIpAndPresentation(key, presentationRepository.findOne(presentationId));
    }



}
