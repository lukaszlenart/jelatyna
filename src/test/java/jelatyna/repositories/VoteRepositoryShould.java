package jelatyna.repositories;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jelatyna.IntegrationTest;
import jelatyna.domain.Presentation;
import jelatyna.domain.Vote;
public class VoteRepositoryShould extends IntegrationTest {
    @Autowired
    private VoteRepository repository;
    @Autowired
    private PresentationRepository presentationRepository;
    private String ip = "1.1.1.1";

    @Test
    public void fetchVoteByIpAndPresentation() {
        Presentation presentation1 = createPresentation();
        Presentation presentation2 = createPresentation();
        saveVote(presentation1, 1);
        saveVote(presentation2, 10);

        Vote vote = repository.findByIpAndPresentation(ip, presentation1);

        assertThat(vote.getRate()).isEqualTo(1);
    }

    @Test
    public void fetchVotesForPresentation() {
        Presentation presentation1 = createPresentation();
        Presentation presentation2 = createPresentation();
        Vote vote1 = saveVote(presentation1, 1);
        Vote vote2 = saveVote(presentation1, 2);
        saveVote(presentation2, 3);

        List<Vote> votes = repository.findByPresentation(presentation1);

        assertThat(votes).containsOnly(vote1, vote2);
    }

    @Test
    public void fetch_votes_with_not_null_rate() {
        Presentation presentation1 = createPresentation();
        Vote vote1 = saveVote(presentation1, 1);
        saveVote(presentation1, null);

        List<Vote> votes = repository.findByPresentation(presentation1);

        assertThat(votes).containsOnly(vote1);
    }

    private Vote saveVote(Presentation presentation, Integer rate) {
        return repository.save(new Vote(ip, presentation, rate));
    }

    private Presentation createPresentation() {
        return presentationRepository.save(new Presentation());
    }
}
